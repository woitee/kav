﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UTS.Collections {
    /// <summary>
    /// A very simple heap implementation.
    /// Should not be used for data heavy operations.
    /// Supports multiple keys for entries.
    /// </summary>
    /// <typeparam name="TKey">Key to arrange the heap by.</typeparam>
    /// <typeparam name="TValue">Value to be present in the heap.</typeparam>
    class SimpleHeap<TKey, TValue> {
        protected SortedDictionary<TKey, Queue<TValue>> sortedDictionary;
        public SimpleHeap() {
            sortedDictionary = new SortedDictionary<TKey, Queue<TValue>>();
        }

        /// <summary>
        /// Adds an element to the heap.
        /// </summary>
        /// <param name="key">Key of the element.</param>
        /// <param name="value">Value of the element.</param>
        public void Add(TKey key, TValue value) {
            if (!sortedDictionary.ContainsKey(key)) {
                sortedDictionary.Add(key, new Queue<TValue>());
            }
            sortedDictionary[key].Enqueue(value);
        }

        /// <summary>
        /// Extracts the element with the minimum key and removes it from the heap and returns it.
        /// </summary>
        /// <returns></returns>
        public KeyValuePair<TKey, TValue> ExtractMin() {
            var first = sortedDictionary.First();
            var queue = first.Value;
            if (queue.Count == 1) {
                sortedDictionary.Remove(first.Key);
            }
            return new KeyValuePair<TKey, TValue>(first.Key, queue.Dequeue());
        }

        /// <summary>
        /// True if the Heap is empty.
        /// </summary>
        public bool IsEmpty {
            get { return sortedDictionary.Count == 0; }
        }
    }
}
