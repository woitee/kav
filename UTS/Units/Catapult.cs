﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UTS.Buildings;
using UTS.Drawing;

namespace UTS.Units {
    class Catapult : Unit {
        public Catapult(PlayerColor color)
            : base( new SpriteOfEachColor(Textures.CatapultBlue, Textures.CatapultRed),
                    new SpriteOfEachColor(Textures.CatapultBlueSelected, Textures.CatapultRedSelected),
                    Textures.ScrollMidCatapultTexture,
                    color
            ) {

            AttackBonusDistribution = MissableAttackBonusDistribution;
        }

        public override int AttackDamageTo(Hexes.Hex other, ref Hexes.AttackBonus bonus) {
            int damage = base.AttackDamageTo(other, ref bonus);
            if (other as Building != null)
                damage += 5;
            return damage;
        }
    }
}
