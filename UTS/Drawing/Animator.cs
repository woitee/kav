﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UTS.Utils;

namespace UTS.Drawing {
    /// <summary>
    /// A class that holds sprites for drawing and can fluently add, remove or move them from on spot to another.
    /// </summary>
    public class Animator : IScrollingDrawer {
        /// <summary>
        /// Consists of all the details the Animator holds about a sprite.
        /// </summary>
        protected internal class SpriteInfo {
            /// <summary>
            /// Part of the path to travel, holds info about a location and the time it should reached.
            /// </summary>
            public class PathPart {
                public Point location;
                public long reachTime; //milliseconds

                public PathPart(Point location, long reachTime) {
                    this.location = location;
                    this.reachTime = reachTime;
                }
            }
            public ILocationDrawable texture;

            public SpriteInfo (ILocationDrawable texture) {
                this.texture = texture;
            }

            // Properties used only for moving
            public bool IsMoving = false;
            public Vector2 CurrentLocation;
            public Queue<PathPart> MovePath;
            public Action MoveFinishCallback;
        }
        
        protected internal Dictionary<Point, SpriteInfo> sprites = new Dictionary<Point,SpriteInfo>();
        protected Rectangle positionOnScreen;
        public Vector2 Offset { get; protected set; }
        protected Vector2 maxOffset;
        
        public Vector2 maxScrollingAlteration { get; set; }
        protected Vector2 offsetChange = Vector2.Zero;
        /// <summary>
        /// The scrolling speed in pixels per second.
        /// </summary>
        public int ScrollSpeed = Constants.ScrollSpeed;
        /// <summary>
        /// Actions called after a draw is redrawn.
        /// Generally used to add/remove objects to be drawn, which cannot be done during the draw loop.
        /// </summary>
        public Queue<Action> drawFinishCallbacks = new Queue<Action>();

        /// <summary>
        /// Constructor of an animator without scrolling capabilities.
        /// </summary>
        /// <param name="positionOnScreen">The position on screen relative to which sprites will be drawn.</param>
        public Animator(Rectangle positionOnScreen) : this(positionOnScreen, new Point(positionOnScreen.Width, positionOnScreen.Height)) {
        }

        /// <summary>
        /// Constructor of an animator with scrolling capabilites.
        /// </summary>
        /// <param name="positionOnScreen">The position on screen relative to which sprites will be drawn.</param>
        /// <param name="size">The total size of canvas sprites will be drawn to. Can be larger than position on screen,
        ///     extra size will be solved by scrolling.
        /// </param>
        public Animator(Rectangle positionOnScreen, Point size) {
            this.positionOnScreen = positionOnScreen;
            Point drawRectangleSize = new Point(positionOnScreen.Width, positionOnScreen.Height);
            maxOffset = size .Sub( drawRectangleSize).ToVector();
        }

        /// <summary>
        /// Adds a sprite to a given location.
        /// </summary>
        /// <param name="texture">The sprite to add.</param>
        /// <param name="x">X-location of the sprite.</param>
        /// <param name="y">Y-location of the sprite.</param>
        public void AddSprite(ILocationDrawable texture, int x, int y) {
            AddSprite(texture, new Point(x, y));
        }

        /// <summary>
        /// Adds a sprite to a given location.
        /// </summary>
        /// <param name="texture">The sprite to add.</param>
        /// <param name="location">Location of the sprite.</param>
        public void AddSprite(ILocationDrawable texture, Point location) {
            sprites[location] = new SpriteInfo(texture);
        }

        /// <summary>
        /// Moves a sprite from one place to another.
        /// </summary>
        /// <param name="from">Location where the move starts.</param>
        /// <param name="to">Location where the move ends.</param>
        /// <param name="time">Time that the move shall last, in seconds.</param>
        /// <param name="callback">An action that will be called when the move finishes.</param>
        public void MoveSprite(Point from, Point to, double time, Action callback = null) {
            MoveSprite(from, new List<Point>(new Point[] { to }), time, callback);
        }

        /// <summary>
        /// Moves a sprite from one place to another, along a path.
        /// </summary>
        /// <param name="from">Location where the move starts.</param>
        /// <param name="path">List of points the move should visit.</param>
        /// <param name="time">Time that the move shall last, in seconds.</param>
        /// <param name="callback">An action that will be called when the move finishes.</param>
        public void MoveSprite(Point from, List<Point> path, double time, Action callback = null) {
            SpriteInfo info = sprites[from];
            
            sprites[path[path.Count - 1]] = info;
            sprites.Remove(from);

            info.IsMoving = true;
            info.CurrentLocation = from.ToVector();
            info.MovePath = CreateMovePath(from, path, (long)(time * 1000));
            info.MoveFinishCallback = callback;
        }

        // Private methods, gets milliseconds since start of computer time
        private long GetMilliseconds() {
            return DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        }

        /// <summary>
        /// Creates a more detailed path information about a move.
        /// </summary>
        /// <param name="from">Move starting location.</param>
        /// <param name="path">The path of the move.</param>
        /// <param name="timeMillis">Time the move lasts.</param>
        /// <returns></returns>
        protected Queue<SpriteInfo.PathPart> CreateMovePath(Point from, List<Point> path, long timeMillis) {
            double totalDistance = 0;
            Point point = from;
            foreach (Point pathPoint in path) {
                totalDistance += point.DistanceFrom(pathPoint);
                point = pathPoint;
            }

            var ret = new Queue<SpriteInfo.PathPart>();
            long time = GetMilliseconds();
            ret.Enqueue(new SpriteInfo.PathPart(from, time));
            point = from;
            foreach (Point pathPoint in path) {
                double distance = point.DistanceFrom(pathPoint);
                double percentage = distance / totalDistance;

                long timeShare = (long)(timeMillis * percentage);
                time += timeShare;

                ret.Enqueue(new SpriteInfo.PathPart(pathPoint, time));

                point = pathPoint;
            }

            return ret;
        }

        /// <summary>
        /// Removes all sprites from the Animator.
        /// </summary>
        public void RemoveAllSprites() {
            sprites = new Dictionary<Point, SpriteInfo>();
        }

        /// <summary>
        /// Removes a sprite at a specific location.
        /// </summary>
        /// <param name="location">Location of the sprite.</param>
        public void RemoveSprite(Point location) {
            sprites.Remove(location);
        }

        /// <summary>
        /// Return true if a sprite is at a given location.
        /// </summary>
        /// <param name="location">Location of the query.</param>
        /// <returns></returns>
        public bool SpriteExists(Point location) {
            return sprites.ContainsKey(location);
        }

        /// <summary>
        /// Gets the sprite at a given location.
        /// </summary>
        /// <param name="location">Location of the sprite.</param>
        /// <returns></returns>
        public ILocationDrawable SpriteAt(Point location) {
            return sprites[location].texture;
        }

        public void Update(GameTime gameTime) {
            #region Update Moving Pictures
            List<Action> callbacksToDo = new List<Action>();

            foreach (SpriteInfo info in sprites.Values) {
                if (info.IsMoving) {
                    var movePath = info.MovePath;

                    long currentTime = GetMilliseconds();
                    var curTarget = movePath.ElementAt(1);

                    //cycle through already reached points
                    while (curTarget.reachTime <= currentTime) {
                        movePath.Dequeue();
                        if (movePath.Count < 2) {
                            info.IsMoving = false;
                            break;
                        }
                        curTarget = movePath.ElementAt(1);
                    }
                    if (!info.IsMoving) {
                        //already finished move
                        if (info.MoveFinishCallback != null) {
                            callbacksToDo.Add(info.MoveFinishCallback);
                        }
                        continue;
                    }

                    var lastTarget = movePath.Peek();

                    long total = curTarget.reachTime - lastTarget.reachTime;
                    long current = currentTime - lastTarget.reachTime;

                    float progress = (float)current / (float)total;

                    Vector2 vector = curTarget.location.Sub(lastTarget.location).ToVector();
                    vector *= progress;
                    info.CurrentLocation = lastTarget.location.ToVector() + vector;
                }
            }
            // Callbacks need to be called in the end, because they may change the enumeration
            foreach (Action callback in callbacksToDo) {
                callback();
            }
            #endregion
            #region Update Scrolling
            Offset += offsetChange * (float)gameTime.ElapsedGameTime.TotalSeconds;
            Offset = Vector2.Clamp(Offset, Vector2.Zero, maxOffset + maxScrollingAlteration);
            #endregion
        }

        public void StartScrolling(Direction direction) {
            switch (direction) {
                case Direction.Left:
                    offsetChange.X = -ScrollSpeed;
                    break;
                case Direction.Right:
                    offsetChange.X = ScrollSpeed;
                    break;
                case Direction.Up:
                    offsetChange.Y = -ScrollSpeed;
                    break;
                case Direction.Down:
                    offsetChange.Y = ScrollSpeed;
                    break;
                default:
                    break;
            }
        }
        public void StopScrolling(Orientation orientation) {
            switch (orientation) {
                case Orientation.Horizontal:
                    offsetChange.X = 0;
                    break;
                case Orientation.Vertical:
                    offsetChange.Y = 0;
                    break;
                default:
                    break;
            }
        }
        public void StopScrolling() {
            offsetChange = Vector2.Zero;
        }
        public void ScrollTo(Vector2 offset) {
            Offset = Vector2.Clamp(offset, Vector2.Zero, maxOffset + maxScrollingAlteration);
        }

        /// <summary>
        /// Adds an action to be called after next redraw finishes.
        /// </summary>
        /// <param name="action">The action to call after redrawing.</param>
        public void AddDrawFinishCallback(Action action) {
            drawFinishCallbacks.Enqueue(action);
        }

        /// <summary>
        /// Inner function that draws a sprite to a location, takes account of current scrolling, etc.
        /// </summary>
        /// <param name="sprite">The sprite to draw.</param>
        /// <param name="spriteBatch">SpriteBatch to draw on.</param>
        /// <param name="location">The location of the draw.</param>
        /// <param name="RequestedRemove">Returns true if a sprite should be removed after this redraw.</param>
        protected void DrawToMe(ILocationDrawable sprite, SpriteBatch spriteBatch, Vector2 location, out bool RequestedRemove) {
            // Pre-processing points
            location -= Offset;

            location.X += positionOnScreen.X;
            location.Y += positionOnScreen.Y;

            sprite.DrawTo(spriteBatch, location, out RequestedRemove);
        }
        public void Draw(SpriteBatch spriteBatch) {
            var toRemove = new List<Point>();

            // Draw stationary sprites first
            foreach (var keyVal in sprites) {
                Point location = keyVal.Key;
                SpriteInfo spriteInfo = keyVal.Value;

                if (!spriteInfo.IsMoving) {
                    bool shallRemove;
                    DrawToMe(spriteInfo.texture, spriteBatch, location.ToVector(), out shallRemove);
                    if (shallRemove)
                        toRemove.Add(location);
                }
            }

            // Draw moving sprites
            foreach (var keyVal in sprites) {
                SpriteInfo spriteInfo = keyVal.Value;

                if (spriteInfo.IsMoving) {
                    bool shallRemove;
                    DrawToMe(spriteInfo.texture, spriteBatch, spriteInfo.CurrentLocation, out shallRemove);
                    if (shallRemove)
                        toRemove.Add(keyVal.Key);
                }
            }

            // Remove scheduled sprites
            foreach (Point key in toRemove) {
                sprites.Remove(key);
            }

            // Process callbacks
            while (drawFinishCallbacks.Count > 0) {
                drawFinishCallbacks.Dequeue()();
            }
        }
    }
}
