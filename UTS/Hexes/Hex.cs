﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UTS.Collections;
using UTS.Drawing;
using UTS.Hexes.Animations;
using UTS.Utils;
using UTS.Units;
using UTS.Buildings;

namespace UTS.Hexes {
    /// <summary>
    /// A class describing a possible attack bonus, containing the bonus amount and a colored text to show.
    /// </summary>
    public struct AttackBonus {
        public int bonus;
        public string text;
        public Color textColor;

        public AttackBonus(int bonus, string text, Color textColor) {
            this.bonus = bonus;
            this.text = text;
            this.textColor = textColor;
        }

        /// <summary>
        /// Create a floating text of this bonus at a specified location.
        /// </summary>
        /// <param name="hexLocation">The location to show the floating text at.</param>
        public void CreateFloatingText(Point hexLocation) {
            Match.Current.hexAnimator.AddSprite(
                new FloatingTextAnimation(text, textColor, Direction.Up, 2),
                HexAnimator.GetPixelCoordinates(hexLocation).Add(new Point(Constants.HexWidth / 2, -12)),
                (int)Match.Layer.FloatingTexts
            );
        }
    }
    /// <summary>
    /// Contains all information about an attack that was made.
    /// </summary>
    public class AttackInfo {
        public Hex source;
        public Hex target;
        public int damageDealt;
        public AttackInfo(Hex source, Hex target, int damageDealt) {
            this.source = source;
            this.target = target;
            this.damageDealt = damageDealt;
        }
    }

    /// <summary>
    /// Common ancestor of units and buildings.
    /// Contains all info about them, how to draw them, position, variables, etc.
    /// </summary>
    public class Hex : ILocationDrawable{
        // ==================
        // Colors and sprites
        // ==================
        public PlayerColor color { get; protected set; }
        public int Width { get; protected set; }
        public int Height { get; protected set; }
        public bool isDying;
        public DateTime deathStart;
        protected SpriteOfEachColor sprites { get; set; }
        public ILocationDrawable Sprite {
            get { return sprites.Get(color); }
        }
        public Texture2D InnerScroll { get; protected set; }
        public bool ShowingOverlay;
        public bool ShowingCrosshair;

        // ======================
        // Combat and cost values
        // ======================
        public enum AttackType { Ranged, Melee, Boulder, None };
        public AttackType attackType { get; protected set; }
        public int AttackRange { get; protected set; }
        public int AttackDamage;
        public int Health { get; protected set; }
        public int MaxHealth { get; protected set; }
        public int Defense;
        public int FoodCost { get; protected set; }
        public int PeopleCost { get; protected set; }
        public int Upkeep { get; protected set; }
        public AttackInfo lastAttacked;
        public AttackInfo lastAttackedBy;

        // =========
        // Variables
        // =========
        public Point Location { get; protected set; } // hex location
        public Point PixelLocation {
            get {
                return HexAnimator.GetPixelCoordinates(Location);
            }
        }
        public bool IsInHand {
            get { return Location == new Point(-1, -1); }
        }
        protected bool ItsMyTurn { get { return Match.Current.CurrentPlayer.color == this.color; }}
        public bool IsExhausted;
        protected virtual bool CanShowInfo {
            get {
                return !isDying;
            }
        }
        protected internal bool ShowingHitAnimation;
        protected internal HexHitAnimation HitAnimation;
        public int HealthBeforeLastHit { get; protected set; }
        public List<Buff> Buffs = new List<Buff>(6);

        // ========
        // Layering
        // ========
        protected static Hex primaryHex = null; //hex that is primary
        protected Match.Layer currentLayer;
        protected Match.Layer defaultLayer;
        protected Match.Layer primaryLayer;

        // ============
        // Showing info
        // ============
        public enum InfoState { None, Expanding, Retracting };
        private InfoState infoState = InfoState.None;

        protected static KeyValuePair<double, AttackBonus>[] DefaultAttackBonusDistribution;
        protected static KeyValuePair<double, AttackBonus>[] MissableAttackBonusDistribution;
        protected KeyValuePair<double, AttackBonus>[] AttackBonusDistribution;
        static Hex() {
            DefaultAttackBonusDistribution = new KeyValuePair<double, AttackBonus>[] {
                new KeyValuePair<double, AttackBonus>(0.05, new AttackBonus(-2, "Barely scratched", GameColors.BadColor)),
                new KeyValuePair<double, AttackBonus>(0.25, new AttackBonus(-1, "Clumsy hit", GameColors.BelowAverageColor)),
                new KeyValuePair<double, AttackBonus>(0.75, new AttackBonus( 0, "Mediocre hit", GameColors.NeutralColor)),
                new KeyValuePair<double, AttackBonus>(0.95, new AttackBonus( 1, "Good hit", GameColors.GoodColor)),
                new KeyValuePair<double, AttackBonus>(1,    new AttackBonus( 2, "Great hit", GameColors.GreatColor))
            };
            MissableAttackBonusDistribution = new KeyValuePair<double, AttackBonus>[] {
                new KeyValuePair<double, AttackBonus>(0.2, new AttackBonus(-1000, "Missed", GameColors.ReallyBadColor)),
                new KeyValuePair<double, AttackBonus>(0.28, new AttackBonus(-2, "Barely scratched", GameColors.BadColor)),
                new KeyValuePair<double, AttackBonus>(0.4, new AttackBonus(-1, "Clumsy hit", GameColors.BelowAverageColor)),
                new KeyValuePair<double, AttackBonus>(0.75, new AttackBonus( 0, "Mediocre hit", GameColors.NeutralColor)),
                new KeyValuePair<double, AttackBonus>(0.88, new AttackBonus( 1, "Good hit", GameColors.GoodColor)),
                new KeyValuePair<double, AttackBonus>(1,    new AttackBonus( 2, "Great hit", GameColors.GreatColor))
            };
        }

        // ===========
        // Valid moves
        // ===========
        protected List<Point> validMoves;
        protected List<Hex> validTargets;

        /// <summary>
        /// A default Hex constructor.
        /// </summary>
        /// <param name="sprites">Sprite for each color of unit.</param>
        /// <param name="innerScroll">Texture of the scroll, that is shown below Hexes.</param>
        /// <param name="color">Color of the Hex.</param>
        public Hex(SpriteOfEachColor sprites, Texture2D innerScroll, PlayerColor color) {
            this.sprites = sprites;
            this.InnerScroll = innerScroll;
            this.color = color;
            Width = Constants.HexWidth;
            Height = Constants.HexHeight;

            AttackBonusDistribution = DefaultAttackBonusDistribution;
        }

        // ===============================================================
        // Calculating and showing of valid moves and targets of this hex.
        // ===============================================================
        protected internal virtual List<Point> GetValidMoves() {
            return new List<Point>();
        }
        protected internal virtual List<Hex> GetValidTargets() {
            Match match = Match.Current;

            List<Point> hexesInRange = match.Navigator.HexesInDistance(Location, AttackRange);
            
            List<Hex> ret = new List<Hex>();
            foreach (Point point in hexesInRange) {
                Unit unit = match.units[point];
                Building building = match.buildings[point];

                if (unit != null && CanAttack(unit, false))
                    ret.Add(unit);
                if (building != null && CanAttack(building, false))
                    ret.Add(building);
            }
            return ret;
        }
        protected void HideValidMoves() {
            Match match = Match.Current;
            if (validMoves == null)
                return;
            foreach (Point move in validMoves) {
                HideAsValidMove(move);
            }
            validMoves = null;
        }
        protected void ShowValidMoves() {
            if (validMoves == null)
                validMoves = GetValidMoves();
            foreach (Point move in validMoves) {
                DisplayAsValidMove(move);
            }
        }
        protected void HideValidTargets() {
            if (validTargets == null)
                return;
            foreach (Hex hex in validTargets) {
                hex.HideAsValidTarget();
            }
            validTargets = null;
        }
        protected void ShowValidTargets() {
            validTargets = GetValidTargets();
            foreach (Hex hex in validTargets) {
                hex.DisplayAsValidTarget();
            }
        }
        public static void DisplayAsValidMove(Point hex) {
            Match match = Match.Current;
            if (match.buildings[hex] == null) {
                match.hexAnimator.AddHex(Textures.HexOverlay, hex, (int)Match.Layer.PossMoveHexes);
            } else {
                match.buildings[hex].ShowingOverlay = true;
            }
        }
        public static void HideAsValidMove(Point hex) {
            Match match = Match.Current;
            if (match.buildings[hex] == null) {
                match.hexAnimator.RemoveHex(hex, (int)Match.Layer.PossMoveHexes);
            } else {
                match.buildings[hex].ShowingOverlay = false;
            }
        }
        public static bool IsDisplayedAsValidMove(Point hex) {
            Match match = Match.Current;
            if (match.buildings[hex] == null) {
                return match.hexAnimator.HexExists(hex, (int)Match.Layer.PossMoveHexes);
            } else {
                return match.buildings[hex].ShowingOverlay;
            }
        }
        public void DisplayAsValidTarget() {
            ShowingCrosshair = true;
        }
        public void HideAsValidTarget() {
            ShowingCrosshair = false;
        }
        public bool IsDisplayedAsValidTarget() {
            return ShowingCrosshair;
        }
        protected void ShowValidMovesAndTargets() {
            ShowValidMoves();
            ShowValidTargets();
        }
        protected void HideValidMovesAndTargets() {
            HideValidMoves();
            HideValidTargets();
        }

        public virtual bool CanBeSelectedBy(Player player) {
            return !IsExhausted && color == player.color;
        }
        
        /// <summary>
        /// A function that is called when Hex is created on map.
        /// </summary>
        /// <param name="hexLoc">Location, where the hex was created.</param>
        public virtual void CreatedAt(Point hexLoc) {
            this.Location = hexLoc;

            Player myPlayer = Match.Current.GetPlayer(color);
            myPlayer.upkeep += Upkeep;
        }
        /// <summary>
        /// Called whenever a turn starts. (This unit's and opponents also)
        /// </summary>
        /// <param name="eventQueue">EventQueue for enqueueing focusable actions.</param>
        public virtual void StartTurn(EventQueue eventQueue) {
            IsExhausted = false;
        }
        public virtual bool CanAttack(Hex other, bool checkRange) {
            if (IsExhausted || this.color == other.color || other.isDying)
                return false;

            if (checkRange && !Match.Current.Navigator.HexesInDistance(Location, AttackRange).Contains(other.Location)) {
                return false;
            }

            return true;
        }
        /// <summary>
        /// Calculates attack damage to another unit.
        /// </summary>
        /// <param name="other">The unit to be attacked.</param>
        /// <param name="bonus">The bonus that was chosen for this attack.</param>
        /// <returns></returns>
        public virtual int AttackDamageTo(Hex other, ref AttackBonus bonus) {
            return (AttackDamage + bonus.bonus - other.DefenseFrom(this, ref bonus));
        }
        public virtual int DefenseFrom(Hex other, ref AttackBonus bonus) {
            return Defense;
        }
        public virtual void Attack(Hex other) {
            if (other.isDying)
                return;

            // Pick attack bonus
            double roll = MainGame.random.NextDouble();
            AttackBonus bonus = new AttackBonus();
            foreach (var keyVal in AttackBonusDistribution) {
                if (roll < keyVal.Key) {
                    bonus = keyVal.Value;
                    break;
                }
            }

            // Apply damage
            int thisAttackDamage = AttackDamageTo(other, ref bonus).LimitToRange(0, int.MaxValue);

            other.HealthBeforeLastHit = other.Health;
            other.Health -= thisAttackDamage;

            // Save info about attacks
            AttackInfo thisAttack = new AttackInfo(this, other, thisAttackDamage);
            lastAttacked = thisAttack;
            other.lastAttackedBy = thisAttack;

            // Trigger event
            Match.Current.HexAttacked(new Events.HexAttackedEventArgs(this, other, thisAttackDamage));
            if (other.Health <= 0) {
                other.Die();
            }

            Unit thisUnit = this as Unit;
            // Unselect unit, if it was selected
            if (thisUnit != null && thisUnit.isSelected) {
                HideValidMovesAndTargets();
                Match.Current.UnselectUnit();
                IsExhausted = true;
            }
            other.ShowingHitAnimation = true;
            other.HitAnimation = new HexHitAnimation(this, other, thisAttackDamage, bonus);
        }
        public virtual void Die() {
            UnsetMePrimary();
            Match match = Match.Current;

            //Animate death
            isDying = true;
            deathStart = DateTime.Now;

            Player myPlayer = Match.Current.GetPlayer(color);
            myPlayer.upkeep -= Upkeep;

            if (this is Unit) {
                Unit unit = (Unit) this;
                match.units[Location] = null;
                if (unit.shallBeDrawnByBuilding) {
                    match.buildings[Location].DiedHere = unit;
                }
            } else if (this is Building)
                match.buildings[Location] = null;
        }
        /// <summary>
        /// Die without showing the death animation and fading out.
        /// </summary>
        public void DieFast() {
            Die();
            deathStart = DateTime.Now - TimeSpan.FromHours(1);
        }
        /// <summary>
        /// Called when a unit is removed from board.
        /// </summary>
        protected virtual void BoardRemove() {
        }

        public virtual void ChangeColor(PlayerColor to) {
            color = to;
        }

        public virtual void Update(GameTime gameTime) {
            Match match = Match.Current;
            if (match.HoveredHex == this) {
                if (infoState != InfoState.Expanding)
                    ShowInfo();
            } else {
                if (infoState != InfoState.None)
                    HideInfo();
            }
        }
        private static void DrawScaled(SpriteBatch spriteBatch, Texture2D texture, Vector2 location, Color color, double scale) {
            Point newOrigin = location.ToPoint().Add(new Point(79, 107).Scale(1 - scale));
            Point size = Constants.InnerHexSize.Scale(scale);
            Rectangle destination = new Rectangle(newOrigin.X, newOrigin.Y, size.X, size.Y);

            spriteBatch.Draw(texture, destination, color);
        }
        public virtual void DrawTo(SpriteBatch spriteBatch, Vector2 location, out bool requestRemove) {
            DefaultDrawTo(spriteBatch, location, out requestRemove, 1);
        }
        protected void DefaultDrawTo(SpriteBatch spriteBatch, Vector2 location, out bool requestRemove, double scale) {
            Match match = Match.Current;
            double fadeoutStart = 1;
            double fadeoutEnd = 2;

            // Dying
            Color tint = Color.White;
            if (isDying) {
                DateTime now = DateTime.Now;
                double deadSeconds = (now - deathStart).TotalSeconds;
                if (deadSeconds > fadeoutEnd) {
                    requestRemove = true;
                    match.hexAnimator.GetAnimator((int) currentLayer).AddDrawFinishCallback(() => {
                        BoardRemove();
                    });
                    return;
                }
                if (deadSeconds > fadeoutStart) {
                    double fadeoutPercentage = (deadSeconds - fadeoutStart) / (fadeoutEnd - fadeoutStart);
                    tint *= 1 - (float)fadeoutPercentage;
                }
            }

            requestRemove = false;
            
            // Draw base texture
            Texture2D texture;
            Unit thisUnit = this as Unit;
            if (thisUnit != null && thisUnit.isSelected) {
                texture = ((Texture2DILocationDrawable)thisUnit.SelectedSprite).Texture;
            } else {
                texture = ((Texture2DILocationDrawable)Sprite).Texture;
            }
            DrawScaled(spriteBatch, texture, location, tint, scale);
            

            // Draw exhaustion
            if (IsExhausted) {
                DrawScaled(spriteBatch, Textures.ZzzTexture, location, tint, scale);
            }
            //Maybe draw some overlays
            DrawOverlays(spriteBatch, location, tint, scale);

            // Draw hit
            if (ShowingHitAnimation) {
                bool animationFinished = false;
                HitAnimation.DrawTo(spriteBatch, location, out animationFinished);
                if (animationFinished) {
                    HitAnimation = null;
                    ShowingHitAnimation = false;
                }
            }
        }
        /// <summary>
        /// Draw additional overlays, such as HexOverlay or Crosshair.
        /// Can be overloaded to add additional overlays, this function is called at a proper time to do so.
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch to draw to.</param>
        /// <param name="location">Location of the draw.</param>
        /// <param name="tint">Color tint of the drawn overlays.</param>
        /// <param name="scale">Scale of the overlay to be drawn.</param>
        protected virtual void DrawOverlays(SpriteBatch spriteBatch, Vector2 location, Color tint, double scale) {
            // Maybe draw some overlays
            if (ShowingOverlay) {
                DrawScaled(spriteBatch, Textures.HexOverlayTexture, location, tint, scale);
            }
            if (ShowingCrosshair) {
                if (ShowingOverlay)
                    scale *= 0.5;
                DrawScaled(spriteBatch, Textures.CrosshairTexture, location, tint, scale);
            }
        }
        /// <summary>
        /// Creates a floating text above a hex.
        /// </summary>
        /// <param name="hexLoc">Location of the hex that should get a floating text.</param>
        /// <param name="text">Text of the floating text.</param>
        /// <param name="textColor">Color of the floating text.</param>
        public static void CreateFlowingText(Point hexLoc, string text, Color textColor) {
            Match.Current.hexAnimator.AddSprite(
                new FloatingTextAnimation(text, textColor, Direction.Up, 2),
                HexAnimator.GetPixelCoordinates(hexLoc).Add(new Point(Constants.HexWidth / 2, -12)),
                (int)Match.Layer.FloatingTexts
            );
        }
        /// <summary>
        /// Create a floating text above this Hex.
        /// </summary>
        /// <param name="text">Text of the floating text.</param>
        /// <param name="textColor">Color of the text.</param>
        public void CreateFlowingText(string text, Color textColor) {
            CreateFlowingText(Location, text, textColor);
        }

        // Layering
        public virtual void SwitchToLayer(Match.Layer layer) {
            if (currentLayer != layer) {
                Match.Current.hexAnimator.ChangeHexLayer(Location, (int)currentLayer, (int)layer);
                currentLayer = layer;
            }
        }
        /// <summary>
        /// There is always at maximum one hex primary and drawn on top.
        /// This sets this hex as it.
        /// </summary>
        protected void SetAsPrimary() {
            if (primaryHex != null) {
                if (primaryHex.infoState != InfoState.None)
                    primaryHex.ImmediatelyHideInfo();
                primaryHex.SwitchToLayer(primaryHex.defaultLayer);
            }
            this.SwitchToLayer(primaryLayer);
            primaryHex = this;
        }
        /// <summary>
        /// There is always at maximum one hex primary and drawn on top.
        /// This unsets this hex from that position.
        /// </summary>
        protected void UnsetMePrimary() {
            if (primaryHex != this)
                return;

            primaryHex = null;
            if (infoState != InfoState.None)
                ImmediatelyHideInfo();
            SwitchToLayer(defaultLayer);
        }

        /// <summary>
        /// Starts showing the sliding info about this hex.
        /// </summary>
        public void ShowInfo() {
            if (!CanShowInfo)
                return;

            Match match = Match.Current;
            if (infoState == InfoState.Retracting) {
                HexDetails myDetails = (HexDetails)(match.hexAnimator.HexAt(Location, (int)Match.Layer.UnitDetails));
                infoState = InfoState.Expanding;
                myDetails.StartExpandingAgain();
                return;
            }

            SetAsPrimary();
            infoState = InfoState.Expanding;

            Point myPixelLoc = HexAnimator.GetPixelCoordinates(Location);
            match.hexAnimator.AddHex(new HexDetails(this, () => { infoState = InfoState.None; }), Location, (int)Match.Layer.UnitDetails);
        }

        /// <summary>
        /// Immediately hides the sliding info of this hex.
        /// </summary>
        protected void ImmediatelyHideInfo() {
            if (infoState == InfoState.None)
                return;

            infoState = InfoState.None;
            Match match = Match.Current;

            match.hexAnimator.RemoveHex(Location, (int)Match.Layer.UnitDetails);

            SwitchToLayer(defaultLayer);
        }
        /// <summary>
        /// Hides all sliding infos of all hexes.
        /// </summary>
        public static void HideAllInfos() {
            if (primaryHex != null) {
                primaryHex.UnsetMePrimary();
            }
        }
        /// <summary>
        /// Starts hiding the info about this hex.
        /// </summary>
        public void HideInfo() {
            if (infoState != InfoState.Expanding)
                return;
            infoState = InfoState.Retracting;
            Match match = Match.Current;

            HexDetails myDetails = (HexDetails)(match.hexAnimator.HexAt(Location, (int)Match.Layer.UnitDetails));
            myDetails.StartRetracting();
        }
    }
}
