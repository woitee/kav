﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using UTS.Drawing;
using UTS.Events;

namespace UTS.Units {
    class Marshal : Unit {
        protected UnitCreatedEventHandler unitCreatedEventHandler;
        protected UnitMovedEventHandler unitMovedEventHandler;

        public Marshal(PlayerColor color)
            : base( new SpriteOfEachColor(Textures.MarshalBlue, Textures.MarshalRed),
                    new SpriteOfEachColor(Textures.MarshalBlueSelected, Textures.MarshalRedSelected),
                    Textures.ScrollMidMarshalTexture,
                    color
            ) {

            unitCreatedEventHandler = (args) => { OnUnitCreated(args); };
            unitMovedEventHandler = (args) => { OnUnitMoved(args); };
        }

        private bool IsBoostable(Unit unit) {
            return unit != null && unit != this && unit.color == this.color;
        }
        private void BoostAllUnitsFrom(Point location) {
            Match match = Match.Current;
            var neighbors = match.Navigator.GetNeighbors(location);
            foreach (var neighborUnit in from neighbor in neighbors
                                         let unit = match.units[neighbor]
                                         where IsBoostable(unit)
                                         select unit) {
                // All move neighboring units
                neighborUnit.AttackDamage += 2;
            }
        }
        private void UnboostAllUnitsFrom(Point location) {
            Match match = Match.Current;
            var neighbors = match.Navigator.GetNeighbors(location);
            foreach (var neighborUnit in from neighbor in neighbors
                                         let unit = match.units[neighbor]
                                         where IsBoostable(unit)
                                         select unit) {
                // All move neighboring units
                neighborUnit.AttackDamage -= 2;
            }
        }

        public override void CreatedAt(Microsoft.Xna.Framework.Point hex) {
            base.CreatedAt(hex);
            BoostAllUnitsFrom(Location);

            Match.Current.UnitCreated += unitCreatedEventHandler;
            Match.Current.UnitMoved += unitMovedEventHandler;
        }

        public override void Die() {
            base.Die();
            UnboostAllUnitsFrom(Location);

            Match.Current.UnitCreated -= unitCreatedEventHandler;
            Match.Current.UnitMoved -= unitMovedEventHandler;
        }

        private void OnUnitCreated(UnitCreatedEventArgs args) {
            Match match = Match.Current;
            Unit unit = args.unit;

            if (!IsBoostable(unit))
                return;

            var neighbors = match.Navigator.GetNeighbors(Location);
            if (neighbors.Contains(unit.Location)) {
                unit.AttackDamage += 2;
            }
        }

        private void OnUnitMoved(UnitMovedEventArgs args) {
            Match match = Match.Current;
            Unit unit = args.unit;

            if (!IsBoostable(unit))
                return;

            if (unit == this) {
                // It was this marshal, that has moved
                UnboostAllUnitsFrom(args.fromHex);
                BoostAllUnitsFrom(args.toHex);
                return;
            }

            var neighbors = match.Navigator.GetNeighbors(Location);
            if (neighbors.Contains(args.fromHex)) {
                unit.AttackDamage -= 2;
            }
            if (neighbors.Contains(args.toHex)) {
                unit.AttackDamage += 2;
            }
        }
    }
}
