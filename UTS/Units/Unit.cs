﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UTS.Collections;
using UTS.Drawing;
using UTS.Events;
using UTS.Utils;
using UTS.Hexes.Animations;
using UTS.Hexes;
using UTS.Buildings;

namespace UTS.Units {
    /// <summary>
    /// Default unit types, needed mainly for special unit interaction.
    /// </summary>
    public enum UnitType {
        Infantry, Cavalry, Siege
    }
    
    /// <summary>
    /// A common ancestor of all units.
    /// </summary>
    public abstract class Unit : Hex, ILocationDrawable{
        // Constants

        public UnitType unitType { get; protected set; }
        public int MovementRange;
        protected double scaleInc = 0.03;
        protected double hexMoveSeconds = 0.3;

        // Variables
        
        protected Point LastMoveStartLocation;
        protected bool _IsSelectable = false;
        public bool IsSelectable {
            get {
                return _IsSelectable && !IsMidMove;
            }
            protected set {
                _IsSelectable = value;
            }
        } // hex location
        public bool isSelected { get; protected set; }
        public SpriteOfEachColor SelectedSprites { get; protected set; }
        public ILocationDrawable SelectedSprite {
            get { return SelectedSprites.Get(color); }
        }
        /// <summary>
        /// Returns whether the unit is in the middle of an unconfirmed move.
        /// </summary>
        public bool IsMidMove { get; protected set; }
        /// <summary>
        /// Returns whether the unit is visually travelling.
        /// </summary>
        public bool IsTravelling { get; protected set; }
        /// <summary>
        /// Current scale of the unit displayed. Note: units scale towards their bottom-right corner.
        /// </summary>
        public double curScale { get; protected set; }
        protected override bool CanShowInfo {
            get { return base.CanShowInfo && !IsTravelling && curScale > 0.99; }
        }
        protected internal double targetScale = 1.0;
        public double? overrideTargetScale = null;

        /// <summary>
        /// Creates a default unit.
        /// </summary>
        /// <param name="sprites">Sprite for each color of the unit.</param>
        /// <param name="selectedSprite">Sprite for each color of the unit when selected.</param>
        /// <param name="innerScroll">Texture of the scroll drawn beneath units.</param>
        /// <param name="color">Color of this unit.</param>
        public Unit(SpriteOfEachColor sprites, SpriteOfEachColor selectedSprite, Texture2D innerScroll, PlayerColor color)
                : base(sprites, innerScroll, color) {
            this.IsSelectable = true;
            this.Location = new Point(-1, -1);
            this.curScale = 1.0;
            SetBasicAttributes();

            SelectedSprites = selectedSprite;
            primaryLayer = Match.Layer.PrimaryHex;
            defaultLayer = Match.Layer.Units;
            currentLayer = defaultLayer;
        }

        /// <summary>
        /// Sets basic attributes of the Unit.
        /// </summary>
        protected void SetBasicAttributes() {
            UnitBasicValue values = BasicAttributes.units[this.GetType()];
            Health = values.health;
            AttackDamage = values.attackDamage;
            AttackRange = values.attackRange;
            attackType = values.attackType;
            Defense = values.defense;
            MovementRange = values.movementRange;
            FoodCost = values.foodCost;
            PeopleCost = values.peopleCost;
            Upkeep = values.upkeep;
            unitType = values.unitType;
        }

        /// <summary>
        /// Returns all valid moves of the unit.
        /// </summary>
        /// <returns></returns>
        protected internal override List<Point> GetValidMoves() {
            if (IsExhausted) {
                return new List<Point>();
            }
            return Match.Current.Navigator.AvailableMoves(Location, MovementRange);
        }

        /// <summary>
        /// Returns whether a specific player can select a unit.
        /// </summary>
        /// <param name="player">The player that potentially selects this unit.</param>
        /// <returns></returns>
        public override bool CanBeSelectedBy(Player player) {
            return base.CanBeSelectedBy(player) && IsSelectable;
        }
        /// <summary>
        /// Returns whether the unit can move to a specific hex location.
        /// </summary>
        /// <param name="hex">Destination of the move.</param>
        /// <returns></returns>
        public bool CanMoveTo(Point hex) {
            if (IsMidMove)
                return false;
            if (validMoves == null)
                validMoves = GetValidMoves();
            return validMoves.Contains(hex);
        }

        /// <summary>
        /// Selects the unit.
        /// </summary>
        public virtual void Selected() {
            isSelected = true;
            ShowValidMovesAndTargets();
        }
        /// <summary>
        /// Unselects the unit.
        /// </summary>
        public virtual void Unselected() {
            Match match = Match.Current;

            isSelected = false;
            HideValidMovesAndTargets();
            if (IsMidMove) {
                RevertMove();
            }
        }

        /// <summary>
        /// Creates the unit on the map.
        /// </summary>
        /// <param name="hex">Place, where the unit was created.</param>
        public override void CreatedAt(Point hex) {
            base.CreatedAt(hex);
            IsExhausted = true;
            Match.Current.UnitCreated(new UnitCreatedEventArgs(this));
        }
        /// <summary>
        /// Internally starts turn of the unit.
        /// </summary>
        /// <param name="eventQueue">EventQueue for focusable events to happen.</param>
        public override void StartTurn(EventQueue eventQueue) {
            base.StartTurn(eventQueue);
            IsSelectable = true;
        }
        /// <summary>
        /// Starts the move of the unit.
        /// </summary>
        /// <param name="to">The location the unit is moving to.</param>
        public void StartMove(Point to) {
            Match match = Match.Current;
            Navigator navigator = match.Navigator;

            HideValidMovesAndTargets();
            ImmediatelyHideInfo();
            SetAsPrimary();
            
            Action callback= () => { Moved(to); };

            var path = navigator.GetPathTo(this, to);
            match.hexAnimator.MoveHex(Location, path, (int) currentLayer, path.Count * hexMoveSeconds, callback);

            LastMoveStartLocation = Location;
            match.units[Location] = null;
            this.Location = to;
            match.units[Location] = this;
            this.IsMidMove = true;
            this.IsSelectable = false;
            this.IsTravelling = true;

            ShowValidTargets();
        }
        /// <summary>
        /// The unit has moved to a location.
        /// </summary>
        /// <param name="to">The location the unit has moved to.</param>
        protected void Moved(Point to) {
            IsSelectable = true;
            IsTravelling = false;
        }
        /// <summary>
        /// Confirmns and finishes the move of a unit.
        /// </summary>
        public virtual void MoveFinished() {
            Match match = Match.Current;

            IsMidMove = false;
            IsExhausted = true;
            match.UnselectUnit();
            match.UnitMoved(new UnitMovedEventArgs(this, LastMoveStartLocation, Location));
        }
        /// <summary>
        /// Reverts a unit's uncomfirmed move.
        /// </summary>
        protected void RevertMove() {
            IsMidMove = false;
            IsExhausted = false;
            IsSelectable = true;
            IsTravelling = false;
            ImmediatelyHideInfo();

            Match match = Match.Current;
            match.units[Location] = null;
            match.hexAnimator.RemoveHex(Location, (int)currentLayer);
            this.Location = LastMoveStartLocation;
            match.units[Location] = this;
            match.hexAnimator.AddHex(this, Location, (int)currentLayer);
        }
        
        /// <summary>
        /// Attacks another unit.
        /// </summary>
        /// <param name="other">The attacked unit.</param>
        public override void Attack(Hex other) {
            MoveFinished();
            base.Attack(other);
        }

        public override void Update(GameTime gameTime) {
            base.Update(gameTime);
            if (shallBeDrawnByBuilding && Match.Current.HoveredHex != this) {
                targetScale = 0.5;
            } else {
                targetScale = 1;
            }
        }

        // Units on buildings are drawn by the building, to solve the problem of finite layers
        // This block of code deals with the ambiguity
        # region Drawing By Buildings
        bool buildingScheduledRemove = false;
        public bool lastShallBeDrawnByBuilding = false;
        public Building lastDrawnBy = null;
        public bool shallBeDrawnByBuilding {
            get { return Match.Current.buildings[this.Location] != null && !IsTravelling; }
        }
        public override void DrawTo(SpriteBatch spriteBatch, Vector2 location, out bool requestRemove) {
            Match match = Match.Current;

            if (!lastShallBeDrawnByBuilding) {
                if (shallBeDrawnByBuilding) {
                    StartedBeingDrawnByBuilding();
                }
            } else if (!shallBeDrawnByBuilding) {
                NoLongerDrawnByBuilding();
            }
            lastShallBeDrawnByBuilding = shallBeDrawnByBuilding;

            if (shallBeDrawnByBuilding) {
                requestRemove = buildingScheduledRemove;
                return;
            }
            InnerDrawTo(spriteBatch, location, out requestRemove);
        }
        public void DrawCalledByBuilding(SpriteBatch spriteBatch, Vector2 location, out bool requestRemove) {
            requestRemove = false;
            lastDrawnBy = Match.Current.buildings[Location];
            InnerDrawTo(spriteBatch, location, out buildingScheduledRemove);
        }

        protected void StartedBeingDrawnByBuilding() {
            UpdateBuildingLayers();
        }
        protected void NoLongerDrawnByBuilding() {
            UpdateBuildingLayers(true);
        }
        protected void UpdateBuildingLayers(bool hasLeft = false) {
            Building building = lastDrawnBy != null ? lastDrawnBy : Match.Current.buildings[this.Location];
            if (
                    (currentLayer == primaryLayer && !hasLeft) ||
                    primaryHex == building
                ) {
                building.SwitchToLayer(Match.Layer.SecondaryHex);
            } else {
                building.SwitchToLayer(Match.Layer.Buildings);
            }
        }
        public override void SwitchToLayer(Match.Layer layer) {
            base.SwitchToLayer(layer);
            if (shallBeDrawnByBuilding) {
                UpdateBuildingLayers();
            }
        }
        #endregion
        // This code now deals with drawing only
        protected void InnerDrawTo(SpriteBatch spriteBatch, Vector2 location, out bool requestRemove) {
            Match match = Match.Current;

            // Process scale
            double curTargetScale = overrideTargetScale != null ? (double)overrideTargetScale : targetScale;
            if (curScale < curTargetScale) {
                curScale += scaleInc;
                curScale = curScale > curTargetScale ? curTargetScale : curScale;
            } else if (curScale > curTargetScale) {
                curScale -= scaleInc;
                curScale = curScale < curTargetScale ? curTargetScale : curScale;
            }

            DefaultDrawTo(spriteBatch, location, out requestRemove, curScale);
        }
    }
}
