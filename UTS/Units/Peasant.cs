﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using UTS.Drawing;
using UTS.Events;
using UTS.Maps;
using UTS.Buildings;

namespace UTS.Units {
    class Peasant : Unit {
        protected bool isHarvesting = false;
        protected BuildingCreatedEventHandler buildingCreatedEventHandler;

        public Peasant(PlayerColor color)
            : base( new SpriteOfEachColor(Textures.PeasantBlue, Textures.PeasantRed),
                    new SpriteOfEachColor(Textures.PeasantBlueSelected, Textures.PeasantRedSelected),
                    Textures.ScrollMidPeasantTexture,
                    color
            ) {

                buildingCreatedEventHandler = (args) => {
                    UpdateHarvesting();
                };
        }

        public override void CreatedAt(Point hex) {
            base.CreatedAt(hex);
            Match match = Match.Current;
            match.BuildingCreated += buildingCreatedEventHandler;
            UpdateHarvesting();
        }

        public override void MoveFinished() {
            base.MoveFinished();
            Match match = Match.Current;
            UpdateHarvesting();
        }

        public override void Die() {
            base.Die();
            Match.Current.BuildingCreated -= buildingCreatedEventHandler;
            StopHarvesting();
        }

        protected bool IsHarvestable(Point hexLoc) {
            Match match = Match.Current;
            Map map = match.Map;

            bool neighboringFriendlyBuilding = false;
            foreach (Point neighbor in match.Navigator.GetNeighbors(hexLoc)) {
                Building building = match.buildings[neighbor];
                if (building != null && building.color == this.color) {
                    neighboringFriendlyBuilding = true;
                    break;
                }
            }

            return neighboringFriendlyBuilding &&
                   map.FieldAt(hexLoc) == MapField.Grass &&
                   match.buildings[hexLoc] == null;
        }

        protected void StartHarvesting() {
            if (isHarvesting)
                return;

            isHarvesting = true;
            Match.Current.GetPlayer(color).foodIncome += 1;
        }

        protected void StopHarvesting() {
            if (!isHarvesting)
                return;

            isHarvesting = false;
            Match.Current.GetPlayer(color).foodIncome -= 1;
        }

        protected void UpdateHarvesting() {
            if (IsHarvestable(Location))
                StartHarvesting();
            else
                StopHarvesting();
        }
    }
}
