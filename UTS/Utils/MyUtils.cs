﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

using UTS.Drawing;

namespace UTS.Utils {
    static class MyUtils {
        #region Vector2 extensions

        public static float DistanceFrom(this Vector2 vector, Vector2 other) {
            return Vector2.Distance(vector, other);
        }

        public static Point ToPoint(this Vector2 vector) {
            return new Point((int)vector.X, (int)vector.Y);
        }

        #endregion

        #region Rectangle extensions

        public static Point Origin(this Rectangle rect) {
            return new Point(rect.X, rect.Y);
        }

        public static Point BottomRight(this Rectangle rect) {
            return new Point(rect.X + rect.Width, rect.Y + rect.Height);
        }

        public static Rectangle FromBorders(int LeftX, int TopY, int RightX, int BottomY) {
            return new Rectangle(LeftX, TopY, RightX - LeftX, BottomY - TopY);
        }

        public static Rectangle FromBorders(float LeftX, float TopY, float RightX, float BottomY) {
            return FromBorders((int)LeftX, (int)TopY, (int)RightX, (int)BottomY);
        }

        public static Point Size(this Rectangle rect) {
            return new Point(rect.Width, rect.Height);
        }

        #endregion

        #region Point extensions

        public static Point Add(this Point p, Point other) {
            p.X += other.X; p.Y += other.Y;
            return p;
        }

        public static Point Add(this Point p, int x, int y) {
            p.X += x; p.Y += y;
            return p;
        }

        public static Point Sub(this Point p, Point other) {
            p.X -= other.X; p.Y -= other.Y;
            return p;
        }

        public static Point Sub(this Point p, int x, int y) {
            p.X -= x; p.Y -= y;
            return p;
        }

        public static Point Scale(this Point p, double mult) {
            p.X = (int)(p.X * mult);
            p.Y = (int)(p.Y * mult);
            return p;
        }

        public static double DistanceFrom(this Point p, Point other) {
            return Math.Sqrt((p.X - other.X) * (p.X - other.X) + (p.Y - other.Y) * (p.Y - other.Y));
        }

        public static Point Clamp(this Point p, Point low, Point high) {
            p.X = p.X.LimitToRange(low.X, high.X);
            p.Y = p.Y.LimitToRange(low.Y, high.Y);
            return p;
        }

        public static bool IsIn(this Point p, Rectangle rect) {
            return p.X >= rect.X && p.Y >= rect.Y && p.X <= rect.X + rect.Width && p.Y <= rect.Y + rect.Height;
        }

        public static Vector2 ToVector(this Point p) {
            return new Vector2(p.X, p.Y);
        }

        #endregion

        #region ILocationDrawable extensions

        public static Point Size(this ILocationDrawable texture) {
            return new Point(texture.Width, texture.Height);
        }

        #endregion

        #region Variable bounding

        public static T LimitToRange<T>(this T x, T low, T high) where T : IComparable<T> {
            if (x.CompareTo(low) <= 0) {
                return low;
            }
            if (x.CompareTo(high) >= 0) {
                return high;
            }
            return x;
        }

        public static bool IsLimitedToRange<T>(this T x, T low, T high) where T : IComparable<T> {
            if (x.CompareTo(low) < 0) {
                return false;
            }
            if (x.CompareTo(high) > 0) {
                return false;
            }
            return true;
        }

        #endregion

        #region 2DArray Utils

        public static T[][] CreateArray<T>(int dimOne, int dimTwo) {
            T[][] ret = new T[dimOne][];
            for (int i = 0; i < dimOne; ++i) {
                ret[i] = new T[dimTwo];
            }
            return ret;
        }

        public static T At<T>(this T[][] array, Point point) {
            return array[point.X][point.Y];
        }

        public static void Set<T>(this T[][] array, Point point, T value) {
            array[point.X][point.Y] = value;
        }

        #endregion

        #region MouseState extensions

        public static Point Location(this MouseState mouseState) {
            return new Point(mouseState.X, mouseState.Y);
        }

        #endregion

        #region List extensions

        public static void Shuffle<T>(this List<T> list) {
            Random rng = MainGame.random;
            // linear time shuffling
            int n = list.Count;
            while (n > 1) {
                int k = rng.Next(n);
                n--;
                T swap = list[k];
                list[k] = list[n];
                list[n] = swap;
            }
        }
        public static T RandomElem<T>(this List<T> list) {
            return list[MainGame.random.Next(list.Count)];
        }

        #endregion

        #region TcpClient extensions
        // Function from stackoverflow question #6993295
        public static bool IsConnected(this TcpClient tcpClient) {
            try {
                if (tcpClient != null && tcpClient.Client != null && tcpClient.Client.Connected) {
                    /* pear to the documentation on Poll:
                    * When passing SelectMode.SelectRead as a parameter to the Poll method it will return 
                    * -either- true if Socket.Listen(Int32) has been called and a connection is pending;
                    * -or- true if data is available for reading; 
                    * -or- true if the connection has been closed, reset, or terminated; 
                    * otherwise, returns false
                    */
                    if (tcpClient.Client.Poll(0, SelectMode.SelectRead)) {
                        byte[] buff = new byte[1];
                        if (tcpClient.Client.Receive(buff, SocketFlags.Peek) == 0) {
                            // Client disconnected
                            return false;
                        }
                    }

                    return true;
                } else {
                    return false;
                }
            } catch {
                return false;
            }
        }

        #endregion
    }
}
