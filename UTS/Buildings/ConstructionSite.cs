﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using UTS.Collections;
using UTS.Hexes;
using UTS.Units;
using UTS.Drawing;

namespace UTS.Buildings {
    class ConstructionSite : Building {
        protected Building building;

        public ConstructionSite(PlayerColor color, Building building)
            : base(
                new SpriteOfEachColor(Textures.SiteBlue, Textures.SiteRed),
                Textures.ScrollMidSiteTexture,
                color
            ) {
                this.building = building;
        }

        public override void StartTurn(EventQueue eventQueue) {
            base.StartTurn(eventQueue);
            if (!ItsMyTurn)
                return;

            eventQueue.EnqueueFocusOn(this);
            eventQueue.Enqueue(() => {
                Match match = Match.Current;
                this.DieFast();
                building.ChangeColor(this.color);
                match.AddBuilding(building, Location);
            });
            eventQueue.EnqueueUnfocus(this);
        }
    }
}
