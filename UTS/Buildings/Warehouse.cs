﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using UTS.Drawing;

namespace UTS.Buildings {
    class Warehouse : Building {
        public Warehouse(PlayerColor color)
            : base(
                new SpriteOfEachColor(Textures.WarehouseBlue, Textures.WarehouseRed),
                Textures.ScrollMidWarehouseTexture,
                color
            ) {
        }

        public override void CreatedAt(Point hex) {
            base.CreatedAt(hex);
            Player myPlayer = Match.Current.GetPlayer(color);
            myPlayer.foodIncome += 1;
            myPlayer.foodCapacity += 4;
        }

        public override void Die() {
            base.Die();
            Player myPlayer = Match.Current.GetPlayer(color);
            myPlayer.foodIncome -= 1;
            myPlayer.foodCapacity -= 4;
        }
    }
}
