﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UTS.Cards {
    class FootmanCard : UnitCard {
        public FootmanCard()
            : base(
                Textures.CardFootman,
                new Units.Footman(PlayerColor.Blue)
                ) { }

    }
}
