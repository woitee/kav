﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UTS.Cards {
    class PikemanCard : UnitCard {
        public PikemanCard()
            : base(
                Textures.CardPikeman,
                new Units.Pikeman(PlayerColor.Blue)
                ) { }

    }
}
