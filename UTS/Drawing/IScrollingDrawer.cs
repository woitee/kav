﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace UTS.Drawing {
    public interface IScrollingDrawer : IDrawer {
        /// <summary>
        /// Starts scrolling in a specified direction.
        /// </summary>
        /// <param name="direction">Direction of scrolling.</param>
        void StartScrolling(Direction direction);
        /// <summary>
        /// Stops scrolling along either the x, or y axis.
        /// </summary>
        /// <param name="orientation">Orientation in which we don't want to scroll anymore.</param>
        void StopScrolling(Orientation orientation);
        /// <summary>
        /// Stops scrolling completely.
        /// </summary>
        void StopScrolling();
        /// <summary>
        /// Immediately scrolls to an offset.
        /// </summary>
        /// <param name="offset"></param>
        void ScrollTo(Vector2 offset);
        /// <summary>
        /// Current scrolling offset.
        /// </summary>
        Vector2 Offset { get; }
        /// <summary>
        /// Adjusts the default max scrolling (bottom-right corner) by a given amount.
        /// The default max scrolling is determined by the size argument in constructor.
        /// </summary>
        Vector2 maxScrollingAlteration { get; set; }
    }
}
