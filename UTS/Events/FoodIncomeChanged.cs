﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UTS.Events {
    public delegate void FoodIncomeChangedEventHandler(FoodIncomeChangedEventArgs args);

    public class FoodIncomeChangedEventArgs {
        public Player player;
        public int difference;
        public FoodIncomeChangedEventArgs(Player player, int difference) {
            this.player = player;
            this.difference = difference;
        }
    }
}
