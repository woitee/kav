﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace UTS {
    /// <summary>
    /// A static class that contains the most used constants of the game.
    /// </summary>
    public static class Constants {
        // HexWidth is exactly between inner hex width and outer hex width
        public static readonly int HexWidth = 111, HexHeight = 112;
        public static readonly Point HexSize = new Point(HexWidth, HexHeight);
        public static readonly int InnerHexWidth = 106, InnerHexHeight = 106;
        public static readonly Point InnerHexSize = new Point(InnerHexWidth, InnerHexHeight);
        //public static readonly int HexToInfoHiddenOffset = 23, HextToInfoShownOffset = 21;
        //public static readonly float HexInfoShowSpeed = 1f; //pixels per tick

        public static readonly int CardWidth = 150, CardHeight = 210;

        // Scrolling
        // How thick the borders for scrolling will really be.
        public static readonly int ScrollBorderWidth = 64;
        public static readonly int ScrollSpeed = 500;

        // Port
        public static int Port = 41847;
    }
}
