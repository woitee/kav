﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using UTS.Drawing;
using UTS.Utils;

namespace UTS.Units {
    class Horseman : Unit {
        public Horseman(PlayerColor color)
            : base( new SpriteOfEachColor(Textures.HorsemanBlue, Textures.HorsemanRed),
                    new SpriteOfEachColor(Textures.HorsemanBlueSelected, Textures.HorsemanRedSelected),
                    Textures.ScrollMidHorsemanTexture,
                    color
            ) {
        }

        protected Point startTurnLocation = new Point(-1, -1);

        public override void StartTurn(Collections.EventQueue eventQueue) {
            base.StartTurn(eventQueue);
            startTurnLocation = Location;
        }

        protected internal override List<Point> GetValidMoves() {
            Match match = Match.Current;
            foreach (Point p in match.Navigator.GetNeighbors(Location)) {
                Unit unit = match.units[p];
                if (unit != null && unit.color != color) {
                    return new List<Point>();
                }
            }

            return base.GetValidMoves();
        }

        public override int AttackDamageTo(Hexes.Hex other, ref Hexes.AttackBonus bonus) {
            if (startTurnLocation != Location &&
                other is Unit &&
                !(other is Pikeman)) {
                    return base.AttackDamageTo(other, ref bonus) + 3;
            }
            return base.AttackDamageTo(other, ref bonus);
        }
    }
}