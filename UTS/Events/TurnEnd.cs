﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using UTS.Units;

namespace UTS.Events {
    public delegate void TurnEndEventHandler(TurnEndEventArgs args);

    public class TurnEndEventArgs {
        public Player player;
        public TurnEndEventArgs(Player player) {
            this.player = player;
        }
    }
}
