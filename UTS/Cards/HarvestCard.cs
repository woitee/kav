﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using UTS.Hexes;

namespace UTS.Cards {
    class HarvestCard : EventCard {
        public HarvestCard() : base(Textures.CardHarvest) { }

        public override bool TryUseOnHex(Point location) {
            Match match = Match.Current;
            Player curPlayer = match.CurrentPlayer;

            if (TryPayCost(location)) {
                curPlayer.GainFood(curPlayer.foodIncome);
                return true;
            }

            return false;
        }
    }
}
