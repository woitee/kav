﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UTS.Drawing;
using UTS.Buildings;
using UTS.Cards;
using UTS.Collections;
using UTS.Controllers;
using UTS.Events;
using UTS.Hexes;
using UTS.Maps;
using UTS.Utils;
using UTS.Units;
using UTS.GUI;

namespace UTS {
    /// <summary>
    /// Main class of the "actual" game, the match between players. It contains info about the match, as well as drawing information,
    /// drawing methods, methods to do actions in the match and holds events that can be both raised and listened for by anything
    /// (usually units, buildings, cards, and the like).
    /// </summary>
    public class Match : IDrawer {
        protected MainGame game;
        /// <summary>
        /// Class representing the working graphic layout of the match.
        /// Contains pixel coordinates of where stuff is located in the match
        /// </summary>
        public class Layout {
            /// <summary>
            /// Mainly showing the resolution of the game, origin is at (0, 0).
            /// </summary>
            public Rectangle totalRectangle;
            /// <summary>
            /// Where the hexes are drawn and can be clicked.
            /// </summary>
            public Rectangle hexesRectangle;
            /// <summary>
            /// Where the cards are drawn and can be clicked.
            /// </summary>
            public Rectangle cardsRectangle;
            /// <summary>
            /// Where the next turn button is located and can be clicked.
            /// </summary>
            public Rectangle nextTurnButton;

            public Rectangle discardConfirmButton;
            public Rectangle discardCancelButton;

            /// <summary>
            /// Constructor of default layout.
            /// </summary>
            public Layout() {
                totalRectangle = new Rectangle(0, 0, 1366, 768);
                hexesRectangle = new Rectangle(0, 80, 1366, 688);
                cardsRectangle = new Rectangle(12, 526, 1342, 242);

                nextTurnButton = new Rectangle(611, 73, 171, 42);
                discardConfirmButton = new Rectangle(493, 116, 225, 131);
                discardCancelButton = new Rectangle(776, 116, 225, 131);
            }
            /// <summary>
            /// Adds Textures of the Layout to an Animator.
            /// </summary>
            /// <param name="layoutAnimator">The Animator to add layout to.</param>
            /// <param name="guiOverlay">An extra GUIOverlay to be added.</param>
            public void AddLayoutTo(MultiLayerAnimator layoutAnimator, GUIOverlay guiOverlay) {
                layoutAnimator.AddSprite(guiOverlay, Point.Zero, (int) Match.LayoutLayer.Gui);
                layoutAnimator.AddSprite(Textures.BtnEndTurnOn, nextTurnButton.Origin(), (int)Match.LayoutLayer.Gui);
            }
        }
        public enum Mode { Network, Hotseat };
        public Mode mode { get; protected set; }
        // SecondaryHex is Occupied by building when a unit on top of it is primary
        /// <summary>
        /// Graphic layers of the game, in bottom-top order.
        /// </summary>
        public enum Layer { Map, Buildings, PossMoveHexes, Units, UnitDetails, SecondaryHex, PrimaryHex, ZzzEffect, PossTargetHexes, FloatingTexts, DiscardOverlay };
        public enum LayoutLayer { Gui, Overlay };
        /// <summary>
        /// Current graphical Layout of the match / game.
        /// </summary>
        public Layout layout;
        
        /// <summary>
        /// A multi-layer animator that draws every Hex in the hex-area.
        /// </summary>
        public MultiLayerHexAnimator hexAnimator;
        /// <summary>
        /// An animator that draws the GUI.
        /// </summary>
        public MultiLayerAnimator layoutAnimator;
        /// <summary>
        /// An animator that draws the cards in a player's hand.
        /// </summary>
        public CardAnimator cardAnimator;
        /// <summary>
        /// List of all the IDrawers that should draw in the Draw loop.
        /// </summary>
        protected List<IDrawer> drawers;
        /// <summary>
        /// List of all the drawers that should scroll when scrolling.
        /// </summary>
        protected List<IScrollingDrawer> scrollers;
        /// <summary>
        /// Where the screen has scrolled to with it's top-left corner.
        /// </summary>
        public Vector2 ScrollOffset {
            get {
                return scrollers[0].Offset;
            }
        }

        //"Inner" game stuff
        /// <summary>
        /// A collection of all units currently in game.
        /// </summary>
        public Array2DNonNullEnumerating<Unit> units { get; protected set; }
        /// <summary>
        /// A collection of all buildings currently in game.
        /// </summary>
        public Array2DNonNullEnumerating<Building> buildings { get; protected set; }
        /// <summary>
        /// Enumeration of all the Units AND Buildings in game.
        /// </summary>
        public IEnumerable<Hex> hexes { get { return ((IEnumerable<Hex>)units.Values).Concat((IEnumerable<Hex>)buildings.Values); } }
        public Map Map { get; protected set; }
        public Navigator Navigator { get; protected set; }
        /// <summary>
        /// Index of the current player in the Players array.
        /// </summary>
        public int CurrentPlayerIndex { get; protected set; }
        public Player[] Players { get; protected set; }
        public Player GetPlayer(PlayerColor color) {
            foreach (Player player in Players) {
                if (player.color == color) {
                    return player;
                }
            }
            if (color == PlayerColor.Neutral) {
                return NeutralPlayer;
            }
            return null;
        }
        public Player CurrentPlayer {
            get { return Players[CurrentPlayerIndex]; }
        }
        // Neutral player doesn't play and is here to avoid null references on his units... he doesn't show up in the Players enumeration
        public Player NeutralPlayer = new Player(PlayerColor.Neutral, null);
        public GUIOverlay guiOverlay;

        // Events
        // Instantiated with a blank method, to prevent null reference exceptions
        // Not using the event keyword, because other classes may raise the events
        // Also only passing eventArgs, sender object is not important (it would be Match.Current)
        
        public UnitCreatedEventHandler UnitCreated;
        public BuildingCreatedEventHandler BuildingCreated;
        public UnitMovedEventHandler UnitMoved;
        public HexAttackedEventHandler HexAttacked;
        public TurnEndEventHandler TurnEnd;
        public FoodIncomeChangedEventHandler FoodIncomeChanged;
        public PeopleIncomeChangedEventHandler PeopleIncomeChanged;
        /// <summary>
        /// Actions that will be called and the end of the next Update loop.
        /// </summary>
        public Queue<Action> updateCallbacks = new Queue<Action>();
        /// <summary>
        /// Removes all event listeners.
        /// </summary>
        public void ResetEvents() {
            UnitCreated = (args) => { };
            BuildingCreated = (args) => { };
            UnitMoved = (args) => { };
            HexAttacked = (args) => { };
            TurnEnd = (args) => { };
            FoodIncomeChanged = (args) => { };
            PeopleIncomeChanged = (args) => { };
        }

        // units, buildings, etc. have always access to Match
        /// <summary>
        /// Last match that was created.
        /// </summary>
        public static Match Current;

        /// <summary>
        /// Creates a new match with the used parameters, initializes it, and starts it.
        /// </summary>
        /// <param name="game">Game this Match belongs to.</param>
        /// <param name="map">Map used in this Match.</param>
        /// <param name="mode">Mode of this Match.</param>
        /// <param name="players">Players in this Match.</param>
        public Match(MainGame game, Map map, Mode mode, Player[] players) {
            Current = this;
            this.game = game;
            this.Map = map;
            this.mode = mode;
            this.Players = players;
            for (int i = 0; i < players.Length; ++i)
                players[i].index = i;
            ResetEvents();

            layout = new Layout();
            hexAnimator = new MultiLayerHexAnimator(layout.hexesRectangle, map.Sprite.Size());
            foreach (int layer in Enum.GetValues(typeof(Layer))) {
                hexAnimator.AddLayer(layer);
            }
            cardAnimator = new CardAnimator(layout.totalRectangle, layout.cardsRectangle);
            layoutAnimator = new MultiLayerAnimator(layout.totalRectangle);
            foreach (int layer in Enum.GetValues(typeof(LayoutLayer))) {
                layoutAnimator.AddLayer(layer);
            }
            drawers = new List<IDrawer>(new IDrawer[] {
                hexAnimator, layoutAnimator, cardAnimator
            });
            scrollers = new List<IScrollingDrawer>(new IScrollingDrawer[] {
                hexAnimator
            });
            foreach (var scroller in scrollers) {
                scroller.maxScrollingAlteration = new Vector2(0, Constants.CardHeight);
            }
            guiOverlay = new GUIOverlay();
            layout.AddLayoutTo(layoutAnimator, guiOverlay);

            units = new Array2DNonNullEnumerating<Unit>(map.Size);
            buildings = new Array2DNonNullEnumerating<Building>(map.Size);

            Navigator = new Navigator();

            StartMatch();
        }

        protected void StartMatch() {
            foreach (Player player in Players) {
                player.deck.Shuffle();
            }
            
            hexAnimator.AddSprite(this.Map.Sprite, new Point(0, 0), (int) Layer.Map);

            AddCastles();
            AddVillages();

            //TODO delete this
            /*AddUnit(new Knight(PlayerColor.Blue), new Point(12, 1));
            AddBuilding(new Field(PlayerColor.Red), new Point(13, 2));
            AddUnit(new Footman(PlayerColor.Blue), new Point(12, 2));*/

            StartTurn();
        }
        /// <summary>
        /// Adds the starting Castle for each player.
        /// </summary>
        protected void AddCastles() {
            var castles = Navigator.GetHexesOfType(MapField.Start);
            AddBuilding(new Castle(Players[0].color), castles[0]);
            AddBuilding(new Castle(Players[1].color), castles[1]);
        }
        /// <summary>
        /// Adds Village to every map point, where a Village is supposed to be.
        /// </summary>
        protected void AddVillages() {
            var villagePoints = Navigator.GetHexesOfType(MapField.Village);
            foreach (Point villagePoint in villagePoints) {
                AddBuilding(new Village(PlayerColor.Neutral), villagePoint);
            }
        }

        /// <summary>
        /// Starts scrolling in the given Direction.
        /// </summary>
        /// <param name="direction">The Direction to scroll.</param>
        public void StartScrolling(Direction direction) {
            foreach (var scroller in scrollers) {
                scroller.StartScrolling(direction);
            }
        }
        /// <summary>
        /// Stops scrolling in the given axis.
        /// </summary>
        /// <param name="orientation">The Orientation to stop scrolling in.</param>
        public void StopScrolling(Orientation orientation) {
            foreach (var scroller in scrollers) {
                scroller.StopScrolling(orientation);
            }
        }
        /// <summary>
        /// Stops scrolling completely.
        /// </summary>
        public void StopScrolling() {
            foreach (var scroller in scrollers) {
                scroller.StopScrolling();
            }
        }
        /// <summary>
        /// Immediately scrolls to a given location.
        /// </summary>
        /// <param name="offset">The offset to scroll to.</param>
        public void ScrollTo(Vector2 offset) {
            foreach (var scroller in scrollers) {
                scroller.ScrollTo(offset);
            }
        }

        public void Update(GameTime gameTime) {
            // Update graphics
            foreach (var drawer in drawers) {
                drawer.Update(gameTime);
            }
            // Update potential Player input
            CurrentPlayer.Controller.Update(gameTime);
            // Update all Units and Buildings
            foreach (var hex in hexes) {
                hex.Update(gameTime);
            }
            // Process Turn-Start Events
            if (TurnStarting) {
                ProcessEventQueue();
            }
            // Call every callback registered to happen after Update
            while (updateCallbacks.Count > 0) {
                updateCallbacks.Dequeue()();
            }
        }

        public void Draw(SpriteBatch spriteBatch) {
            foreach (var drawer in drawers) {
                drawer.Draw(spriteBatch);
            }
        }

        /// <summary>
        /// Adds a Unit to the board.
        /// </summary>
        /// <param name="unit">The Unit to add.</param>
        /// <param name="hex">The place to add the Unit.</param>
        public void AddUnit(Unit unit, Point hex) {
            Logger.Info("Adding unit " + unit + " to hex " + hex);

            units[hex] = unit;
            hexAnimator.AddHex(unit, hex, (int) Layer.Units);
            unit.CreatedAt(hex);
        }
        /// <summary>
        /// Adds a Building to the board.
        /// </summary>
        /// <param name="building">The Building to add.</param>
        /// <param name="hex">The place to add the Unit.</param>
        public void AddBuilding(Building building, Point hex) {
            Logger.Info("Adding building " + building + " to hex " + hex);
            
            buildings[hex] = building;
            hexAnimator.AddHex(building, hex, (int) Layer.Buildings);
            building.CreatedAt(hex);
        }
        /// <summary>
        /// Adds a Card to the display.
        /// </summary>
        /// <param name="card">The Card to add.</param>
        public void AddCard(Card card) {
            cardAnimator.Add(card);
        }
        /// <summary>
        /// Adds multiple Cards to display.
        /// </summary>
        /// <param name="cards">The Cards to add.</param>
        public void AddCards(IEnumerable<Card> cards) {
            foreach (Card card in cards) {
                AddCard(card);
            }
        }
        /// <summary>
        /// Removes a Card from display.
        /// </summary>
        /// <param name="index">Zero-based index of the card to remove.</param>
        public void RemoveCard(int index) {
            cardAnimator.RemoveCard(index);
            CurrentPlayer.hand.RemoveAt(index);
        }

        #region Operating With Hexes
        public bool IsUnitSelected {
            get; protected set;
        }
        /// <summary>
        /// The Unit that is selected, or last selected unit if IsUnitSelected == null.
        /// </summary>
        public Unit SelectedUnit { get; protected set; }
        /// <summary>
        /// The Hex that is currently hovered on.
        /// </summary>
        public Hex HoveredHex;
        /// <summary>
        /// Player action that unselects the selected Unit.
        /// </summary>
        public void UnselectUnit() {
            if (!IsUnitSelected)
                return;
            IsUnitSelected = false;
            SelectedUnit.Unselected();
        }
        /// <summary>
        /// Player action that selects a Unit.
        /// </summary>
        /// <param name="unit">The Unit to select.</param>
        public void SelectUnit(Unit unit) {
            if (IsUnitSelected) {
                UnselectUnit();
            }
            IsUnitSelected = true;
            SelectedUnit = unit;
            SelectedUnit.Selected();
        }
        /// <summary>
        /// Player action that moves the selected Unit to a location. A move animation will be triggered.
        /// </summary>
        /// <param name="to">The location to move the Unit to.</param>
        public void MoveSelectedUnit(Point to) {
            Unit unit = (Unit) SelectedUnit;

            unit.StartMove(to);
        }
        /// <summary>
        /// Confirms and finishes the move of the selected Unit.
        /// </summary>
        public void FinishMoveSelected() {
            ((Unit)SelectedUnit).MoveFinished();
        }
        /// <summary>
        /// Attacks another Hex with the selected Unit.
        /// </summary>
        /// <param name="attacked">Hex to be attacked.</param>
        public void AttackSelected(Hex attacked) {
            SelectedUnit.Attack(attacked);
            IsUnitSelected = false;
        }
        #endregion

        #region Operating With Cards
        public bool IsCardSelected {
            get;
            protected set;
        }
        int selectedCardIndex;
        /// <summary>
        /// Player action to selects a card.
        /// </summary>
        /// <param name="index">Zero-based index of the selected card.</param>
        public void SelectCard(int index) {
            Hex.HideAllInfos();
            if (IsUnitSelected) {
                UnselectUnit();
            }
            if (IsCardSelected) {
                UnselectCard();
            }
            IsCardSelected = true;
            selectedCardIndex = index;
            Logger.Info("Selecting card " + SelectedCard + " at index " + selectedCardIndex);
            SelectedCard.Selected();
        }
        /// <summary>
        /// Returns the selected Card. May return any card if IsCardSelected == false.
        /// </summary>
        public Card SelectedCard {
            get { return cardAnimator.CardAt(selectedCardIndex); }
        }
        /// <summary>
        /// Unselects the selected Card.
        /// </summary>
        public void UnselectCard() {
            if (!IsCardSelected)
                return;
            Logger.Info("Unselecting card");
            SelectedCard.Unselected();
            IsCardSelected = false;
        }
        /// <summary>
        /// Tries to use the selected Card on a Hex position. Returns true on success.
        /// </summary>
        /// <param name="hex">The location to try to use the card on.</param>
        /// <returns>True when the use was successful, false otherwise.</returns>
        public bool TryUseSelectedCardOnHex(Point hex) {
            Logger.Info("Trying to use card " + SelectedCard + " on hex " + hex);
            Card card = SelectedCard;

            UnselectCard();
            if (card.TryUseOnHex(hex)) {
                IsCardSelected = false;
                RemoveCard(selectedCardIndex);
                return true;
            }
            return false;
            
        }
        /// <summary>
        /// Discards a card from hand.
        /// </summary>
        /// <param name="index">The index of the card to discard, or -1, if the card should be random.</param>
        public void DiscardCard(int index = -1) {
            if (index == -1) {
                index = MainGame.random.Next(CurrentPlayer.hand.Count);
            }
            CurrentPlayer.hand.RemoveAt(index);
            cardAnimator.RemoveCard(index);
        }
        /// <summary>
        /// Tries to draw a Card for the current Player.
        /// Card draw animation will be used.
        /// </summary>
        public void DrawCard() {
            if (CurrentPlayer.deck.Count == 0) {
                return;
            }
            if (CurrentPlayer.hand.Count >= 9) {
                return;
            }
            Card card = CurrentPlayer.deck.DrawCard();
            CurrentPlayer.hand.Add(card);
            cardAnimator.DrawCard(card);
        }
        #endregion

        /// <summary>
        /// Swaps what is owned by one player with what is owned by the other player.
        /// </summary>
        /// <param name="one">Color of the first Player to swap stuff.</param>
        /// <param name="two">Color of the second Player to swap stuff.</param>
        public void SwapColors(PlayerColor one, PlayerColor two) {
            // buildings
            foreach (Hex hex in hexes) {
                if (hex.color == one) {
                    hex.ChangeColor(two);
                } else if (hex.color == two) {
                    hex.ChangeColor(one);
                }
            }

            // players
            foreach (Player player in Players) {
                if (player.color == one) {
                    player.color = two;
                } else if (player.color == two) {
                    player.color = one;
                }
            }
        }

        /// <summary>
        /// Whether a turn is starting, which means that player control is disabled, and that only start-turn actions are processed.
        /// </summary>
        protected bool TurnStarting = false;
        /// <summary>
        /// EventQueue containing remaining actions that should be processed this start of turn.
        /// </summary>
        protected EventQueue eventQueue = new EventQueue();
        /// <summary>
        /// Starts a new turn. Draws cards and then starts processing everything that should be done at the start of a turn 
        /// (various special abilities of units and building and the like).
        /// </summary>
        protected void StartTurn() {
            Logger.Info("Turn starts");
            if (CurrentPlayer.color == PlayerColor.Blue)
                cardAnimator.Enabled = true;
            else
                cardAnimator.Enabled = false;

            AddCards(CurrentPlayer.hand);
            CurrentPlayer.StartTurn(eventQueue);
            eventQueue.Enqueue(() => { DrawCard(); });
            eventQueue.EnqueueWait(0.75);
            eventQueue.Enqueue(() => { DrawCard(); });
            eventQueue.EnqueueWait(0.75);
            foreach (Hex hex in hexes) {
                hex.StartTurn(eventQueue);
            }
            TurnStarting = true;
            eventQueue.Enqueue(() => {
                ScrollTo(CurrentPlayer.endTurnOffset);
                CurrentPlayer.Controller.Enable();
            });
            ProcessEventQueue();
            Logger.Info("Turn started");
        }
        /// <summary>
        /// Processes as much of the EventQueue as is currently possible.
        /// Sets TurnStarting to false 
        /// </summary>
        protected void ProcessEventQueue() {
            if (eventQueue.Process())
                TurnStarting = false;
        }

        /// <summary>
        /// Ends the current turn, with associated actions - mainly switching players and starting a new turn.
        /// </summary>
        public void EndTurn() {
            Logger.Info("Turn ends");
            if (IsCardSelected)
                UnselectCard();
            if (IsUnitSelected)
                UnselectUnit();
            TurnEnd(new TurnEndEventArgs(CurrentPlayer));

            CurrentPlayer.endTurnOffset = hexAnimator.Offset;

            if (mode == Mode.Hotseat) {
                SwapColors(PlayerColor.Blue, PlayerColor.Red);
            }

            CurrentPlayer.Controller.Disable();
            if (++CurrentPlayerIndex >= Players.Length) {
                CurrentPlayerIndex = 0;
            }
            cardAnimator.RemoveAllCards();

            StartTurn();
        }

        /// <summary>
        /// Goes to main menu.
        /// </summary>
        /// <param name="message">An optional message to show in the menu after returning (i.e. an error message).</param>
        public void GoToMenu(string message = "") {            
            ResetEvents();
            game.GoToMenu(message);
        }
    }
}
