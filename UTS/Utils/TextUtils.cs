﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UTS.Utils {
    public static class TextUtils {
        public static void DrawCenteredTextWithOutline(SpriteBatch spriteBatch, Vector2 location, string text, Color textColor, Color outlineColor, SpriteFont font) {
            Vector2 textSize = font.MeasureString(text);
            location -= textSize / 2;

            DrawTextWithOutline(spriteBatch, location, text, textColor, outlineColor, font);
        }
        public static void DrawCenteredTextWithOutline(SpriteBatch spriteBatch, Vector2 location, string text, Color textColor, Color outlineColor) {
            DrawCenteredTextWithOutline(spriteBatch, location, text, textColor, outlineColor, Fonts.Arial14);
        }
        public static void DrawCenteredTextWithOutline(SpriteBatch spriteBatch, Vector2 location, string text, Color textColor) {
            DrawCenteredTextWithOutline(spriteBatch, location, text, textColor, Color.Black);
        }
        public static void DrawCenteredTextWithOutline(SpriteBatch spriteBatch, Vector2 location, string text) {
            DrawCenteredTextWithOutline(spriteBatch, location, text, Color.White);
        }

        public static void DrawTextWithOutline(SpriteBatch spriteBatch, Vector2 location, string text, Color textColor, Color outlineColor, SpriteFont font) {
            spriteBatch.DrawString(font, text, location + new Vector2(1, 1), outlineColor);
            spriteBatch.DrawString(font, text, location + new Vector2(-1, 1), outlineColor);
            spriteBatch.DrawString(font, text, location + new Vector2(1, -1), outlineColor);
            spriteBatch.DrawString(font, text, location + new Vector2(-1, -1), outlineColor);

            spriteBatch.DrawString(font, text, location, textColor);
        }
        public static void DrawTextWithOutline(SpriteBatch spriteBatch, Vector2 location, string text, Color textColor, Color outlineColor) {
            DrawTextWithOutline(spriteBatch, location, text, textColor, outlineColor, Fonts.Arial14);
        }
        public static void DrawTextWithOutline(SpriteBatch spriteBatch, Vector2 location, string text, Color textColor) {
            DrawTextWithOutline(spriteBatch, location, text, textColor, Color.Black);
        }
        public static void DrawTextWithOutline(SpriteBatch spriteBatch, Vector2 location, string text) {
            DrawTextWithOutline(spriteBatch, location, text, Color.White);
        }
    }
}
