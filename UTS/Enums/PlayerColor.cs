﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UTS {
    /// <summary>
    /// All player colors that are in the game.
    /// </summary>
    public enum PlayerColor {
        Blue, Red, Neutral
    }
}
