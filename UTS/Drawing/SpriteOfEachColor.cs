﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Graphics;

using UTS;

namespace UTS.Drawing {
    /// <summary>
    /// Contains a sprite of each of the possible player colors.
    /// </summary>
    public class SpriteOfEachColor {
        List<ILocationDrawable> textures = new List<ILocationDrawable>();
        /// <summary>
        /// Creates SpriteOfEachColor with neutral color omitted (neutral color is not required for most of hexes).
        /// If the class is created this way, it will throw exceptions if neutral texture is required.
        /// </summary>
        /// <param name="blue"></param>
        /// <param name="red"></param>
        public SpriteOfEachColor(ILocationDrawable blue, ILocationDrawable red) {
            textures.Add(blue);
            textures.Add(red);
        }

        /// <summary>
        /// Creates a full SpriteOfEachColor, with textures even for neutral color.
        /// </summary>
        /// <param name="blue"></param>
        /// <param name="red"></param>
        /// <param name="neutral"></param>
        public SpriteOfEachColor(ILocationDrawable blue, ILocationDrawable red, ILocationDrawable neutral) {
            textures.Add(blue);
            textures.Add(red);
            textures.Add(neutral);
        }

        /// <summary>
        /// Gets the proper texture for a color.
        /// </summary>
        /// <param name="color">Color of the texture to get.</param>
        /// <returns></returns>
        public ILocationDrawable Get(PlayerColor color) {
            return textures[(int)color];
        }
    }
}
