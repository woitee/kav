﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UTS.Utils;

namespace UTS.Drawing {
    /// <summary>
    /// A drawing animator, supporting multiple (finite) layers of drawing.
    /// </summary>
    public class MultiLayerAnimator : IScrollingDrawer {
        protected SortedDictionary<int, Animator> layers = new SortedDictionary<int, Animator>();

        Rectangle positionOnScreen;
        Point size;
        
        /// <summary>
        /// Creates a new MultiLayerAnimator, without scrolling capabilities.
        /// </summary>
        /// <param name="positionOnScreen">Location on screen.</param>
        public MultiLayerAnimator(Rectangle positionOnScreen) : this(positionOnScreen, positionOnScreen.Size()) {
        }

        /// <summary>
        /// Creates a new MultiLayerAnimator, with scrolling capabilities.
        /// </summary>
        /// <param name="positionOnScreen">Location on screen.</param>
        /// <param name="size">Size of the Canvas. Can be larger than position on screen,
        ///     extranneous space can be scrolled to.</param>
        public MultiLayerAnimator(Rectangle positionOnScreen, Point size) {
            this.positionOnScreen = positionOnScreen;
            this.size = size;
        }


        /// <summary>
        /// Adds a new layer.
        /// </summary>
        /// <param name="layer">The layer depth. Lower number means higher layer.</param>
        public void AddLayer(int layer) {
            layers.Add(layer, new Animator(positionOnScreen, size));
        }

        /// <summary>
        /// Add multiple layers at once.
        /// </summary>
        /// <param name="layers">Depths of the added layers. Lower number means higher layer.</param>
        public void AddLayers(params int[] layers) {
            foreach (int layer in layers) {
                AddLayer(layer);
            }
        }

        /// <summary>
        /// Gets an animator for the given layer only.
        /// </summary>
        /// <param name="layer">Layer of the animator.</param>
        /// <returns></returns>
        public Animator GetAnimator(int layer) {
            return layers[layer];
        }

        /// <summary>
        /// Adds a sprite to the animator.
        /// </summary>
        /// <param name="sprite">Sprite to add.</param>
        /// <param name="location">Coordinates of the sprite.</param>
        /// <param name="layer">Layer to add the sprite to. The layer must be added to the animator before adding to it.</param>
        public void AddSprite(ILocationDrawable sprite, Point location, int layer) {
            Logger.Fine("Adding sprite {0} to {1} at layer {2}, Overwrite:{3}",
                         sprite, location, layer, layers[layer].SpriteExists(location));
            layers[layer].AddSprite(sprite, location);
        }

        /// <summary>
        /// Moves an existing sprite in the animator.
        /// </summary>
        /// <param name="location">Starting location of the sprite.</param>
        /// <param name="to">Destination of the move.</param>
        /// <param name="layer">Layer at which the sprite resides.</param>
        /// <param name="time">How long the move should take, in seconds.</param>
        /// <param name="callback">Action to be called after the move completes.</param>
        public void MoveSprite(Point location, Point to, int layer, double time, Action callback = null) {
            layers[layer].MoveSprite(location, to, time, callback);
        }

        /// <summary>
        /// Moves an existing sprite in the animator along a given path.
        /// </summary>
        /// <param name="location">Starting location of the sprite.</param>
        /// <param name="path">Path the sprite will travell through.</param>
        /// <param name="layer">Layer at which the sprite resides.</param>
        /// <param name="time">How long the move should take, in seconds.</param>
        /// <param name="callback">Action to be called after the move completes.
        public void MoveSprite(Point from, List<Point> path, int layer, double time, Action callback = null) {
            Logger.Fine("Moving sprite {0} from {1} to {2} at layer {3}",
                         layers[layer].SpriteAt(from),
                         from,
                         path.Last(),
                         layer);

            layers[layer].MoveSprite(from, path, time, callback);
        }

        /// <summary>
        /// Returns whether a sprite exists at a given layer.
        /// </summary>
        /// <param name="location">Location where the sprite should be.</param>
        /// <param name="layer">Layer of the potential sprite.</param>
        /// <returns></returns>
        public bool SpriteExists(Point location, int layer) {
            return layers[layer].SpriteExists(location);
        }

        /// <summary>
        /// Returns a sprite at a given location.
        /// </summary>
        /// <param name="location">Location of the sprite.</param>
        /// <param name="layer">Layer of the sprite.</param>
        /// <returns></returns>
        public ILocationDrawable SpriteAt(Point location, int layer) {
            return layers[layer].SpriteAt(location);
        }

        /// <summary>
        /// Removes a sprite from the animator.
        /// </summary>
        /// <param name="location">Location of the sprite.</param>
        /// <param name="layer">Layer of the sprite.</param>
        public void RemoveSprite(Point location, int layer) {
            Logger.Fine("Removing sprite {0} from location {1} at layer {2}",
                         layers[layer].SpriteAt(location),
                         location,
                         layer);
            layers[layer].RemoveSprite(location);
        }

        /// <summary>
        /// Moves a sprite from one layer to another.
        /// </summary>
        /// <param name="spriteLocation">Location of the sprite.</param>
        /// <param name="layerFrom">Layer the sprite resides at.</param>
        /// <param name="layerTo">Layer to move the sprite to.</param>
        public void ChangeSpriteLayer(Point spriteLocation, int layerFrom, int layerTo) {
            layers[layerTo].sprites[spriteLocation] = layers[layerFrom].sprites[spriteLocation];
            layers[layerFrom].RemoveSprite(spriteLocation);
        }

        public void Update(GameTime gameTime) {
            foreach (Animator anim in layers.Values) {
                anim.Update(gameTime);
            }
        }

        public void Draw(SpriteBatch spriteBatch) {
            foreach (Animator anim in layers.Values) {
                anim.Draw(spriteBatch);
            }
        }

        public void StartScrolling(Direction direction) {
            foreach (Animator anim in layers.Values) {
                anim.StartScrolling(direction);
            }
        }

        public void StopScrolling(Orientation orientation) {
            foreach (Animator anim in layers.Values) {
                anim.StopScrolling(orientation);
            }
        }

        public void StopScrolling() {
            foreach (Animator anim in layers.Values) {
                anim.StopScrolling();
            }
        }
        public void ScrollTo(Vector2 offset) {
            foreach (Animator anim in layers.Values) {
                anim.ScrollTo(offset);
            }
        }

        public Vector2 maxScrollingAlteration {
            get {
                if (layers.Count > 0)
                    return layers[0].maxScrollingAlteration;
                return Vector2.Zero;
            }
            set {
                foreach (Animator anim in layers.Values) {
                    anim.maxScrollingAlteration = value;
                }
            }
        }

        public Vector2 Offset {
            get { return layers.First().Value.Offset; }
        }
    }
}
