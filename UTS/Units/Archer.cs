﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UTS.Drawing;

namespace UTS.Units {
    class Archer : Unit {
        public Archer(PlayerColor color)
            : base( new SpriteOfEachColor(Textures.ArcherBlue, Textures.ArcherRed),
                    new SpriteOfEachColor(Textures.ArcherBlueSelected, Textures.ArcherRedSelected),
                    Textures.ScrollMidArcherTexture,
                    color
            ) {
        }

        public override int AttackDamageTo(Hexes.Hex other, ref Hexes.AttackBonus bonus) {
            Unit unit = other as Unit;
            if (unit != null && unit.unitType == UnitType.Infantry) {
                return base.AttackDamageTo(other, ref bonus) + 2;
            }
            return base.AttackDamageTo(other, ref bonus);
        }
    }
}
