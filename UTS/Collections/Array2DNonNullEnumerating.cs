﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace UTS.Collections {
    /// <summary>
    /// An 2D (nested) array of elements, that can be acessed as such.
    /// Additionaly, this can also enumerate all non null elements.
    /// </summary>
    /// <typeparam name="T">Type of the elements stored. Must be a class.</typeparam>
    public class Array2DNonNullEnumerating<T> : IEnumerable<KeyValuePair<Point, T>> where T : class {
        /// <summary>
        /// A class to extend the illusion of a 2D nested array.
        /// Provides methods to access array elements by the [][] notation from the original array.
        /// The first [] returns this class and the [] returns an element of the array.
        /// Similiarly used to set elements of the array.
        /// </summary>
        public class RowGetter {
            Array2DNonNullEnumerating<T> parent;
            int x;
            public RowGetter(Array2DNonNullEnumerating<T> parent, int x) {
                this.parent = parent;
                this.x = x;
            }

            public T this[int y] {
                get {
                    return parent.array[x][y];
                }
                set {
                    parent.array[x][y] = value;
                    if (value != null) {
                        parent.nonNulls[new Point(x, y)] = value;
                    } else {
                        parent.nonNulls.Remove(new Point(x, y));
                    }
                }
            }
        }

        /// <summary>
        /// Array where all the elements are stored.
        /// </summary>
        public T[][] array { get; protected set; }
        /// <summary>
        /// All the non null elements are stored in this dictionary.
        /// </summary>
        protected Dictionary<Point, T> nonNulls;

        /// <summary>
        /// Creates an array of the specified dimensions.
        /// </summary>
        /// <param name="size">Size of the array.</param>
        public Array2DNonNullEnumerating(Point size) : this(size.X, size.Y) { }

        /// <summary>
        /// Creates an array of the specified dimensions.
        /// </summary>
        /// <param name="width">Width of the array. (first coordinate)</param>
        /// <param name="height">Height of the array. (second coordinate)</param>
        public Array2DNonNullEnumerating(int width, int height) {
            array = new T[width][];
            for (int i = 0; i < width; ++i) {
                array[i] = new T[height];
            }
            nonNulls = new Dictionary<Point, T>();
        }

        /// <summary>
        /// Use another pair of square brackets to acess a specific element of the array.
        /// </summary>
        /// <param name="x">First coordinate of the array.</param>
        /// <returns></returns>
        public RowGetter this[int x] {
            get {
                return new RowGetter(this, x);
            }
        }

        /// <summary>
        /// Return an element of the array at a specified location.
        /// </summary>
        /// <param name="p">Location of the element.</param>
        /// <returns></returns>
        public T this[Point p] {
            get {
                return array[p.X][p.Y];
            }
            set {
                array[p.X][p.Y] = value;
                if (value != null) {
                    nonNulls[p] = value;
                } else {
                    nonNulls.Remove(p);
                }
                
            }
        }

        /// <summary>
        /// Enumerates all non null elements of this array as pairs of <Point, Value>.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<KeyValuePair<Point, T>> GetEnumerator() {
            return nonNulls.GetEnumerator();
        }

        /// <summary>
        /// Enumerates all non null values of the array.
        /// </summary>
        public IEnumerable<T> Values {
            get {
                foreach (var keyVal in nonNulls) {
                    yield return keyVal.Value;
                }
            }
        }

        /// <summary>
        /// Enumerates all non null elements of this array as pairs of <Point, Value>.
        /// </summary>
        /// <returns></returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
            return this.GetEnumerator();
        }
    }
}
