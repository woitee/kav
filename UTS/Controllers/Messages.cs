﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

using Microsoft.Xna.Framework;

using UTS.Hexes;
using UTS.Units;

namespace UTS.Controllers.Messages {
    /// <summary>
    /// Message describes a message that is sent over connection when the user is playing a network game.
    /// </summary>

    interface Message {
        string msgString { get; }
    }

    public class InvalidMessageException : Exception {
        public InvalidMessageException() {
        }
        public InvalidMessageException(string s)
            : base(s) {
        }
        public InvalidMessageException(string s, Exception innerException)
            : base(s, innerException) {
        }
    }
    internal static class MessageHelpers {
        public static string NetFormat(this Point point) {
            return point.X + " " + point.Y;
        }
        public static string NetFormat(this Vector2 vector) {
            return vector.X.ToString(CultureInfo.InvariantCulture) + " " +
                   vector.Y.ToString(CultureInfo.InvariantCulture);
        }

        public static bool TryParsePoint(string partX, string partY, out Point point) {
            int X, Y;
            if (!int.TryParse(partX, out X) || !int.TryParse(partY, out Y)) {
                point = Point.Zero;
                return false;
            }
            point = new Point(X, Y);
            return true;
        }
        public static bool TryParseVector2(string partX, string partY, out Vector2 vector) {
            float X, Y;
            if (!float.TryParse(partX, NumberStyles.Any, CultureInfo.InvariantCulture, out X) ||
                !float.TryParse(partY, NumberStyles.Any, CultureInfo.InvariantCulture, out Y)) {
                vector = Vector2.Zero;
                return false;
            }
            vector = new Vector2(X, Y);
            return true;
        }
    }
    static class MessageStatic {
        public static Message Parse(string s) {
            Message msg;
            if (SetRandomSeedMessage.TryParse(s, out msg) ||
                TryUseSelectedCardOnHexMessage.TryParse(s, out msg) ||
                AttackSelectedMessage.TryParse(s, out msg) ||
                MoveSelectedUnitMessage.TryParse(s, out msg) ||
                FinishMoveSelectedMessage.TryParse(s, out msg) ||
                UnselectHexMessage.TryParse(s, out msg) ||
                SelectUnitMessage.TryParse(s, out msg) ||
                UnselectCardMessage.TryParse(s, out msg) ||
                SelectCardMessage.TryParse(s, out msg) ||
                EndTurnMessage.TryParse(s, out msg) ||
                ScrollToMessage.TryParse(s, out msg))
            {
                return msg;
            }

            throw new InvalidMessageException();
        }
    }

    // Core messages
    class SetRandomSeedMessage : Message {
        public string msgString {
            get {
                return "RANDOMSEED " + seed;
            }
        }
        public int seed;

        public SetRandomSeedMessage(int seed) {
            this.seed = seed;
        }

        public static bool TryParse(string s, out Message msg) {
            string[] parts = s.Split(new char[] {' '});
            if (parts.Length != 2 || parts[0] != "RANDOMSEED") {
                msg = null;
                return false;
            }
            int seed;
            if (!int.TryParse(parts[1], out seed)) {
                msg = null;
                return false;
            }
            msg = new SetRandomSeedMessage(seed);
            return true;
        }
    }

    // User action messages
    class TryUseSelectedCardOnHexMessage : Message {
        public string msgString {
            get {
                return "USECARDONHEX " + hexLoc.NetFormat();
            }
        }
        public Point hexLoc;

        public TryUseSelectedCardOnHexMessage(Point hexLoc) {
            this.hexLoc = hexLoc;
        }

        public static bool TryParse(string s, out Message msg) {
            string[] parts = s.Split(new char[] { ' ' });
            if (parts.Length != 3 || parts[0] != "USECARDONHEX") {
                msg = null;
                return false;
            }
            Point hexLoc;
            if (!MessageHelpers.TryParsePoint(parts[1], parts[2], out hexLoc)) {
                msg = null;
                return false;
            }
            msg = new TryUseSelectedCardOnHexMessage(hexLoc);
            return true;
        }
    }
    class AttackSelectedMessage : Message {
        public string msgString {
            get {
                return "ATTACKSELECTED " + hex.Location.NetFormat() + " " + (hex is Unit);
            }
        }
        public Hex hex;

        public AttackSelectedMessage(Hex hex) {
            this.hex = hex;
        }

        public static bool TryParse(string s, out Message msg) {
            string[] parts = s.Split(new char[] { ' ' });
            Point hexLoc;
            bool isUnit;
            if (parts.Length != 4 || parts[0] != "ATTACKSELECTED" ||
                !MessageHelpers.TryParsePoint(parts[1], parts[2], out hexLoc) ||
                !bool.TryParse(parts[3], out isUnit)) {
                msg = null;
                return false;
            }

            Match match = Match.Current;
            Hex hex;
            if (isUnit) {
                hex = match.units[hexLoc];
            } else {
                hex = match.buildings[hexLoc];
            }
            msg = new AttackSelectedMessage(hex);
            return true;
        }
    }
    class MoveSelectedUnitMessage : Message {
        public string msgString {
            get {
                return "MOVESELECTED " + hexLoc.NetFormat();
            }
        }

        public Point hexLoc;
        public MoveSelectedUnitMessage(Point hexLoc) {
            this.hexLoc = hexLoc;
        }

        public static bool TryParse(string s, out Message msg) {
            string[] parts = s.Split(new char[] { ' ' });
            Point hexLoc;
            if (parts.Length != 3 || parts[0] != "MOVESELECTED" ||
                !MessageHelpers.TryParsePoint(parts[1], parts[2], out hexLoc)) {
                msg = null;
                return false;
            }
            msg = new MoveSelectedUnitMessage(hexLoc);
            return true;
        }
    }
    class FinishMoveSelectedMessage : Message {
        public string msgString {
            get {
                return "FINISHMOVESELECTED";
            }
        }

        public FinishMoveSelectedMessage() {
        }

        public static bool TryParse(string s, out Message msg) {
            if (s != "FINISHMOVESELECTED") {
                msg = null;
                return false;
            }
            msg = new FinishMoveSelectedMessage();
            return true;
        }
    }
    class UnselectHexMessage : Message {
        public string msgString {
            get {
                return "UNSELECTHEX";
            }
        }

        public UnselectHexMessage() {
        }

        public static bool TryParse(string s, out Message msg) {
            if (s != "UNSELECTHEX") {
                msg = null;
                return false;
            }
            msg = new UnselectHexMessage();
            return true;
        }
    }
    class SelectUnitMessage : Message {
        public string msgString {
            get {
                return "SELECTUNIT " + unit.Location.NetFormat();
            }
        }

        public Unit unit;
        public SelectUnitMessage(Unit unit) {
            this.unit = unit;
        }

        public static bool TryParse(string s, out Message msg) {
            string[] parts = s.Split(new char[] { ' ' });
            Point hexLoc;
            if (parts.Length != 3 || parts[0] != "SELECTUNIT" ||
                !MessageHelpers.TryParsePoint(parts[1], parts[2], out hexLoc)) {
                msg = null;
                return false;
            }
            msg = new SelectUnitMessage(Match.Current.units[hexLoc]);
            return true;
        }
    }
    class UnselectCardMessage : Message {
        public string msgString {
            get {
                return "UNSELECTCARD";
            }
        }

        public UnselectCardMessage() {
        }

        public static bool TryParse(string s, out Message msg) {
            if (s != "UNSELECTCARD") {
                msg = null;
                return false;
            }
            msg = new UnselectCardMessage();
            return true;
        }
    }
    class SelectCardMessage : Message {
        public string msgString {
            get {
                return "SELECTCARD " + cardIndex;
            }
        }

        public int cardIndex;
        public SelectCardMessage(int cardIndex) {
            this.cardIndex = cardIndex;
        }

        public static bool TryParse(string s, out Message msg) {
            string[] parts = s.Split(new char[] { ' ' });
            if (parts.Length != 2 || parts[0] != "SELECTCARD") {
                msg = null;
                return false;
            }
            int arg;
            if (!int.TryParse(parts[1], out arg)) {
                msg = null;
                return false;
            }
            msg = new SelectCardMessage(arg);
            return true;
        }
    }
    class EndTurnMessage : Message {
        public string msgString {
            get {
                return "ENDTURN";
            }
        }

        public EndTurnMessage() {
        }

        public static bool TryParse(string s, out Message msg) {
            if (s != "ENDTURN") {
                msg = null;
                return false;
            }
            msg = new EndTurnMessage();
            return true;
        }
    }

    class ScrollToMessage : Message {
        public string msgString {
            get {
                return "SCROLLTO " + offset.NetFormat();
            }
        }

        public Vector2 offset;
        public ScrollToMessage(Vector2 offset) {
            this.offset = offset;
        }

        public static bool TryParse(string s, out Message msg) {
            string[] parts = s.Split(new char[] { ' ' });
            Vector2 offset;
            if (parts.Length != 3 || parts[0] != "SCROLLTO" ||
                !MessageHelpers.TryParseVector2(parts[1], parts[2], out offset)) {
                msg = null;
                return false;
            }
            msg = new ScrollToMessage(offset);
            return true;
        }
    }
}
