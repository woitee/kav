﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UTS.Buildings;

namespace UTS.Cards {
    class FieldCard : BuildingCard {
        public FieldCard()
            : base(
                Textures.CardField,
                new Field(PlayerColor.Blue)
            ) { }
    }
}
