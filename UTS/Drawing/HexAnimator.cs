﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UTS.Utils;

namespace UTS.Drawing {
    /// <summary>
    /// A class that works like an Animator, holds all sprites and moves them, but takes all locations in hex coordinates.
    /// </summary>
    public class HexAnimator : IScrollingDrawer {
        Animator animator;
        
        public Vector2 Offset { get { return animator.Offset; } }

        /// <summary>
        /// Initializes a new HexAnimator without scrolling capabilities.
        /// </summary>
        /// <param name="positionOnScreen">Actual location on screen.</param>
        public HexAnimator(Rectangle positionOnScreen) {
            animator = new Animator(positionOnScreen);
        }

        /// <summary>
        /// Constructor of an animator with scrolling capabilites.
        /// </summary>
        /// <param name="positionOnScreen">The position on screen relative to which sprites will be drawn.</param>
        /// <param name="size">The total size of canvas sprites will be drawn to. Can be larger than position on screen,
        ///     extra size will be solved by scrolling.
        /// </param>
        public HexAnimator(Rectangle positionOnScreen, Point size) {
            animator = new Animator(positionOnScreen, size);
        }

        /// <summary>
        /// Adds a new hex to draw.
        /// </summary>
        /// <param name="hexTexture">A hex sprite to draw.</param>
        /// <param name="x">X-hex-coordinate of the sprite.</param>
        /// <param name="y">Y-hex-coordinate of the sprite.</param>
        public void AddHex(ILocationDrawable hexTexture, int x, int y) {
            animator.AddSprite(hexTexture, GetPixelCoordinates(x, y));
        }
        /// <summary>
        /// Adds a new hex to draw.
        /// </summary>
        /// <param name="hexTexture">A hex sprite to draw.</param>
        /// <param name="location">Hex-coordinates of the sprite.</param>
        public void AddHex(ILocationDrawable hexTexture, Point location) {
            AddHex(hexTexture, location.X, location.Y);
        }

        /// <summary>
        /// Moves a sprite from one place to another.
        /// </summary>
        /// <param name="from">Hex-location where the move starts.</param>
        /// <param name="to">Hex-location where the move ends.</param>
        /// <param name="time">Time that the move shall last, in seconds.</param>
        /// <param name="callback">An action that will be called when the move finishes.</param>
        public void MoveHex(Point from, Point to, double time, Action callback = null) {
            animator.MoveSprite(GetPixelCoordinates(from), GetPixelCoordinates(to), time, callback);
        }

        /// <summary>
        /// Moves a sprite from one place to another, along a path.
        /// </summary>
        /// <param name="from">Location where the move starts.</param>
        /// <param name="path">List of points the move should visit.</param>
        /// <param name="time">Time that the move shall last, in seconds.</param>
        /// <param name="callback">An action that will be called when the move finishes.</param>
        public void MoveHex(Point from, List<Point> path, double time, Action callback = null) {
            List<Point> translatedToPixels = new List<Point>();
            foreach (Point pathVector2 in path) {
                translatedToPixels.Add(GetPixelCoordinates(pathVector2));
            }
            animator.MoveSprite(GetPixelCoordinates(from), translatedToPixels, time, callback);
        }

        /// <summary>
        /// Returns whether a sprite exists at the given coordinates.
        /// </summary>
        /// <param name="location">Hex-coordinates of the query.</param>
        /// <returns></returns>
        public bool HexExists(Point location) {
            return animator.SpriteExists(GetPixelCoordinates(location));
        }

        /// <summary>
        /// Returns the sprite at a given location.
        /// </summary>
        /// <param name="location">Hex-coordinates of the sprite.</param>
        /// <returns></returns>
        public ILocationDrawable HexAt(Point location) {
            return animator.SpriteAt(GetPixelCoordinates(location));
        }

        /// <summary>
        /// Returns the sprite at a given location.
        /// </summary>
        /// <param name="x">X-hex-coordinate of the sprite.</param>
        /// <param name="y">Y-hex-coordinate of the sprite.</param>
        /// <returns></returns>
        public ILocationDrawable HexAt(int x, int y) {
            return animator.SpriteAt(GetPixelCoordinates(x, y));
        }

        /// <summary>
        /// Removes a sprite at the given location.
        /// </summary>
        /// <param name="x">X-hex-coordinate of the sprite.</param>
        /// <param name="y">Y-hex-coordinate of the sprite.</param>
        public void RemoveHex(int x, int y) {
            animator.RemoveSprite(GetPixelCoordinates(x, y));
        }

        /// <summary>
        /// Removes a sprite at the given location.
        /// </summary>
        /// <param name="point">Hex-coordinate of the sprite.</param>
        public void RemoveHex(Point point) {
            RemoveHex(point.X, point.Y);
        }

        /// <summary>
        /// Translates hex coordinates to actual pixel coordinates on screen.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public static Point GetPixelCoordinates(int x, int y) {
            Point ret = Point.Zero;
            if (x % 2 == 1) {
                ret.X = (3 * Constants.HexWidth) / 4;
                ret.Y = Constants.HexHeight / 2;
                x -= 1;
            }
            ret.X += (3 * x * Constants.HexWidth) / 4;
            ret.Y += y * Constants.HexHeight;

            // Account for border width
            ret = ret.Add(new Point(4, 6));
            return ret;
        }

        /// <summary>
        /// Translates hex coordinates to actual pixel coordinates on screen.
        /// </summary>
        /// <param name="hex"></param>
        /// <returns></returns>
        public static Point GetPixelCoordinates(Point hex) {
            return GetPixelCoordinates(hex.X, hex.Y);
        }

        /// <summary>
        /// Translates hex coordinates to actual pixel coordinates on screen. Variant operating with integers only.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="resultX"></param>
        /// <param name="resultY"></param>
        public static void GetPixelCoordinates(int x, int y, out int resultX, out int resultY) {
            Point point = GetPixelCoordinates(x, y);
            resultX = point.X;
            resultY = point.Y;
        }

        public int ScrollSpeed {
            get {
                return animator.ScrollSpeed;
            }
            set {
                animator.ScrollSpeed = value;
            }
        }

        public void StartScrolling(Direction direction) {
            animator.StartScrolling(direction);
        }

        public void StopScrolling(Orientation orientation) {
            animator.StopScrolling(orientation);
        }

        public void StopScrolling() {
            animator.StopScrolling();
        }
        public void ScrollTo(Vector2 offset) {
            animator.ScrollTo(offset);
        }
        public Vector2 maxScrollingAlteration {
            get {
                return animator.maxScrollingAlteration;
            }
            set {
                animator.maxScrollingAlteration = value;
            }
        }

        public void Update(GameTime gameTime) {
            animator.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch) {
            animator.Draw(spriteBatch);
        }
    }
}
