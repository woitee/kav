﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UTS.Units;

namespace UTS.Events {
    public delegate void UnitCreatedEventHandler(UnitCreatedEventArgs unit);

    public class UnitCreatedEventArgs {
        public Unit unit;
        public UnitCreatedEventArgs(Unit unit) {
            this.unit = unit;
        }
    }
}
