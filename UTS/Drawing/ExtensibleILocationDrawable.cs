﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UTS.Drawing {
    public abstract class ExtensibleILocationDrawable : ILocationDrawable {
        public List<Tuple<Vector2, ILocationDrawable>> overlays = new List<Tuple<Vector2, ILocationDrawable>>();
        
        public abstract int Width { get; }
        public abstract int Height { get; }

        public void AddOverlay(Vector2 location, ILocationDrawable overlay) {
            overlays.Add(Tuple.Create(location, overlay));
        }
        public void RemoveAllOverlays() {
            overlays.Clear();
        }

        public virtual void DrawTo(SpriteBatch spriteBatch, Vector2 location, out bool shallRemove) {
            //doesn't matter
            shallRemove = false;
            bool unused;

            foreach (var tuple in overlays) {
                Vector2 loc = tuple.Item1 + location;
                tuple.Item2.DrawTo(spriteBatch, loc, out unused);
            }
        }
    }
}
