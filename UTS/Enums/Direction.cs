﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UTS {
    /// <summary>
    /// One of the four possible directions.
    /// </summary>
    public enum Direction {
        Left, Right, Up, Down
    }

    /// <summary>
    /// Horizontal or vertical orientation.
    /// </summary>
    public enum Orientation {
        Horizontal, Vertical
    }
}
