﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UTS.Collections;
using UTS.Hexes;
using UTS.Units;
using UTS.Drawing;
using UTS.Events;

namespace UTS.Buildings {
    class Village : Building {
        HexAttackedEventHandler hexAttacked;
        bool[] wasAttacked = new bool[2];
        static readonly Vector2 flagOffset = new Vector2(59, 0);

        public Village(PlayerColor color)
            : base(
                new SpriteOfEachColor(Textures.VillageBlue, Textures.VillageRed, Textures.VillageYellow),
                InnerScrollOf(color),
                color
            )
        {

            hexAttacked = (args) => {
                if (args.attacked == this)
                    Attacked(args.attacker.color);
            };

            if (this.color != PlayerColor.Neutral) {
                // Conquered villages do not attack
                this.AttackDamage = 0;
                this.AttackRange = 0;
                this.attackType = AttackType.None;
            }
        }   

        protected static Texture2D InnerScrollOf(PlayerColor color) {
            if (color == PlayerColor.Blue) {
                return Textures.ScrollMidVillageOwnedTexture;
            }
            return Textures.ScrollMidVillageEnemyTexture;
        }

        public override void ChangeColor(PlayerColor to) {
            base.ChangeColor(to);
            this.InnerScroll = InnerScrollOf(to);
        }

        public override void StartTurn(EventQueue eventQueue) {
            base.StartTurn(eventQueue);
            Match match = Match.Current;

            if (this.color == PlayerColor.Neutral &&
                    wasAttacked[(int)match.CurrentPlayerIndex]) {
                CommonAbilities.AttackRandomEnemyUnit(this, eventQueue);
            }
        }

        public override bool CanAttack(Hex other, bool checkRange) {
            Match match = Match.Current; 
            if (!base.CanAttack(other, checkRange)) {
                return false;
            }

            if (!wasAttacked[match.CurrentPlayerIndex]) {
                return false;
            }

            return other.color == Match.Current.CurrentPlayer.color;
        }

        public override void CreatedAt(Point hex) {
            base.CreatedAt(hex);
            Match match = Match.Current;
            Player myPlayer = match.GetPlayer(color);
            match.HexAttacked += hexAttacked;

            myPlayer.foodIncome += 5;
            myPlayer.foodCapacity += 2;
            myPlayer.peopleIncome += 2;
            myPlayer.peopleCapacity += 2;
        }

        public override void Die() {
            base.Die();
            Match match = Match.Current;
            Player myPlayer = match.GetPlayer(color);
            match.HexAttacked -= hexAttacked;

            myPlayer.foodIncome -= 5;
            myPlayer.foodCapacity -= 2;
            myPlayer.peopleIncome -= 2;
            myPlayer.peopleCapacity -= 2;
        }

        // Event listener
        public void Attacked(PlayerColor byColor) {
            Match match = Match.Current;
            wasAttacked[match.GetPlayer(byColor).index] = true;
        }

        protected override void BoardRemove() {
            base.BoardRemove();

            if (lastAttackedBy != null) {
                Match match = Match.Current;
                match.AddBuilding(new Village(lastAttackedBy.source.color), Location);
            }
        }

        public override void DrawTo(SpriteBatch spriteBatch, Vector2 location, out bool requestRemove) {
            base.DrawTo(spriteBatch, location, out requestRemove);
            if (color == PlayerColor.Neutral) {
                bool unused;
                if (wasAttacked[0] || wasAttacked[1])
                    Textures.FlagRed.DrawTo(spriteBatch, location + flagOffset, out unused);
                else
                    Textures.FlagWhite.DrawTo(spriteBatch, location + flagOffset, out unused);
            }
        }
    }
}
