﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

using UTS.Maps;
using UTS.Drawing;
using UTS;

namespace OpacityMapReader {

    /// <summary>
    /// Creates a map class out of opacity map describing the various map fields.
    /// </summary>
    class Program {
        static Dictionary<Color, MapField> colorMapping = new Dictionary<Color, MapField>();

        static Program() {
            colorMapping.Add(Color.FromArgb(255, 255, 255), MapField.Grass);
            colorMapping.Add(Color.FromArgb(50, 150, 0), MapField.Forest);
            colorMapping.Add(Color.FromArgb(75, 75, 75), MapField.Mountains);
            colorMapping.Add(Color.FromArgb(0, 0, 255), MapField.Water);
            colorMapping.Add(Color.FromArgb(255, 0, 0), MapField.Start);
            colorMapping.Add(Color.FromArgb(255, 255, 0), MapField.Village);
            colorMapping.Add(Color.FromArgb(0, 255, 255), MapField.Swamp);
            colorMapping.Add(Color.FromArgb(0, 255, 255, 255), MapField.Nothing);
        }

        static Point GetPixelCoordinates(Point point) {
            int retX, retY;
            HexAnimator.GetPixelCoordinates(point.X, point.Y, out retX, out retY);
            retX += Constants.HexWidth / 2;
            retY += Constants.HexHeight / 2;
            return new Point(retX, retY);
        }

        static void Main(string[] args) {
            String src = "F:\\work\\c#\\KaV-XNA\\untracked\\maps\\Lake_Opacity.png";
            Bitmap bmp = new Bitmap(src);
            var resultDictionary = new Dictionary<Point, MapField>();

            Point hexCoords = new Point(0, 0);
            Point pixelPoint = GetPixelCoordinates(hexCoords);
            while (pixelPoint.X < bmp.Width) {
                while (pixelPoint.Y < bmp.Height) {
                    Color color = bmp.GetPixel(pixelPoint.X, pixelPoint.Y);
                    //Console.WriteLine("Found {0} at {1} ({2})", colorMapping[color], hexCoords, pixelPoint);
                    resultDictionary.Add(hexCoords, colorMapping[color]);
                    ++hexCoords.Y;
                    pixelPoint = GetPixelCoordinates(hexCoords);
                }
                ++hexCoords.X;
                hexCoords.Y = 0;
                pixelPoint = GetPixelCoordinates(hexCoords);
            }

            // write resultDictionary to file
            // First, determine the size of the array
            Size size = new Size(0, 0);
            foreach (var keyVal in resultDictionary) {
                if (keyVal.Key.X > size.Width) {
                    size.Width = keyVal.Key.X;
                }
                if (keyVal.Key.Y > size.Height) {
                    size.Height = keyVal.Key.Y;
                }
            }
            // size is actually the maximum value, so due to zero-indexing, needs to be increased by 1
            size.Width += 1; size.Height += 1;

            // Secondly, convert the dictionary to array
            var result = new MapField[size.Width][];
            for (int i = 0; i < size.Width; ++i) {
                result[i] = new MapField[size.Height];
            }
            foreach (var keyVal in resultDictionary) {
                result[keyVal.Key.X][keyVal.Key.Y] = keyVal.Value;
            }

            // Thirdly, write result as a definition
            TextWriter sw = new StreamWriter(src + ".out.txt");
            sw.WriteLine("MapField[][] mapGrid = new MapField[][] {");
            for (int i = 0; i < size.Height; ++i) {
                sw.Write("\tnew MapField[] { ");
                for (int j = 0; j < size.Width; ++j) {
                    sw.Write("\t MapField.{0}", result[j][i]);
                    if (j < size.Width - 1) {
                        sw.Write(",");
                    }
                    sw.Write(" ");
                }
                sw.Write("}");
                if (i < size.Height - 1) {
                    sw.Write(",");
                }
                sw.WriteLine();
            }
            sw.WriteLine("};");
            sw.Close();
            /*int[][] arr = new int[][] {
                                new int[] { 1 },
                                new int[] { 2, 3 }
            };*/
        }
    }
}
