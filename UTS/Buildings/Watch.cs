﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using UTS.Collections;
using UTS.Drawing;
using UTS.Hexes;

namespace UTS.Buildings {
    class Watch : Building {
        public Watch(PlayerColor color)
            : base(
                new SpriteOfEachColor(Textures.WatchBlue, Textures.WatchRed),
                Textures.ScrollMidWatchTexture,
                color
            ) {
        }

        public override void StartTurn(EventQueue eventQueue) {
            base.StartTurn(eventQueue);
            if (ItsMyTurn) {
                CommonAbilities.AttackRandomEnemyUnit(this, eventQueue);
            }
        }
    }
}
