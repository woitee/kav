﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using UTS.Collections;
using UTS.Hexes;
using UTS.Buildings;
using UTS.Units;
using UTS.Utils;

namespace UTS.Hexes{
    /// <summary>
    /// Class containing some of the common abilities of Hexes.
    /// </summary>
    static class CommonAbilities {

        #region Focusing and Scrolling to hex
        /// <summary>
        /// Creates an event that scrolls the view to an offset so a location is visible as much centered as possible.
        /// Then enqueues it in the EventQueue.
        /// </summary>
        /// <param name="eventQueue">EventQueue to enqueue event to.</param>
        /// <param name="location">Pixel location to be viewed.</param>
        public static void EnqueueCenterScroll(this EventQueue eventQueue, Point location) {
            eventQueue.PushOrEnqueueCenterScroll(false, location);
        }
        /// <summary>
        /// Creates an event that scrolls the view to an offset so a location is visible as much centered as possible.
        /// Then pushes it to front of the EventQueue.
        /// </summary>
        /// <param name="eventQueue">EventQueue to push event to.</param>
        /// <param name="location">Pixel location to be viewed.</param>
        public static void PushCenterScroll(this EventQueue eventQueue, Point location) {
            eventQueue.PushOrEnqueueCenterScroll(true, location);
        }

        /// <summary>
        /// Creates an event that focuses on a hex. Then enqueues it into the EventQueue.
        /// All focused hexes should be unfocused by an apropriate unfocus method.
        /// </summary>
        /// <param name="eventQueue">The EventQueue to enqueue the event to.</param>
        /// <param name="hex">The hex to focus on.</param>
        public static void EnqueueFocusOn(this EventQueue eventQueue, Hex hex) {
            eventQueue.EnqueueCenterScroll(hex.PixelLocation.Add(Constants.HexSize.Scale(0.5)));
            eventQueue.Enqueue(() => {
                hex.ShowingOverlay = true;
            });
            eventQueue.EnqueueWait(0.5);
        }
        /// <summary>
        /// Creates an event that unfocuses a hex. Then enqueues it into the EventQueue.
        /// </summary>
        /// <param name="eventQueue">The EventQueue to enqueue the event to.</param>
        /// <param name="hex">The hex to unfocus.</param>
        public static void EnqueueUnfocus(this EventQueue eventQueue, Hex hex) {
            eventQueue.Enqueue(() => {
                hex.ShowingOverlay = false;
            });
            eventQueue.EnqueueWait(0.1);
        }
        /// <summary>
        /// Creates an event that focuses on two hexes at the same time. Then enqueues it into the EventQueue.
        /// All focused hexes should be unfocused by an apropriate unfocus method.
        /// </summary>
        /// <param name="eventQueue">The EventQueue to enqueue the event to.</param>
        /// <param name="one">First of the hexes to focus on.</param>
        /// <param name="two">Second of the hexes to focus on.</param>
        public static void EnqueueFocusOn(this EventQueue eventQueue, Hex one, Hex two) {
            // Scroll to average of hexes
            Point scroll = one.PixelLocation.Add(two.PixelLocation).Scale(0.5);
            eventQueue.EnqueueCenterScroll(scroll.Add(Constants.HexSize.Scale(0.5)));

            eventQueue.Enqueue(() => {
                one.ShowingOverlay = true;
                two.ShowingOverlay = true;
            });
            eventQueue.EnqueueWait(0.5);
        }
        /// <summary>
        /// Creates an event that unfocuses two hexes at the same time. Then enqueues it into the EventQueue.
        /// </summary>
        /// <param name="eventQueue">The EventQueue to enqueue the event to.</param>
        /// <param name="one">First of the hexes to unfocus.</param>
        /// <param name="two">Second of the hexes to unfocus.</param>
        public static void EnqueueUnfocus(this EventQueue eventQueue, Hex one, Hex two) {
            eventQueue.Enqueue(() => {
                one.ShowingOverlay = false;
                two.ShowingOverlay = false;
            });
            eventQueue.EnqueueWait(0.1);
        }

        /// <summary>
        /// Creates an event that focuses on a hex. Then pushes it into the EventQueue.
        /// All focused hexes should be unfocused by an apropriate unfocus method.
        /// </summary>
        /// <param name="eventQueue">The EventQueue to push the event to.</param>
        /// <param name="hex">The hex to focus on.</param>
        public static void PushFocusOn(this EventQueue eventQueue, Hex hex) {
            // Doing stuff in reverse, so it is then processed in order
            eventQueue.PushWait(0.5);
            eventQueue.Push(() => {
                hex.ShowingOverlay = true;
            });
            eventQueue.PushCenterScroll(hex.PixelLocation.Add(Constants.HexSize.Scale(0.5)));
        }
        /// <summary>
        /// Creates an event that unfocuses a hex. Then pushes it into the EventQueue.
        /// </summary>
        /// <param name="eventQueue">The EventQueue to push the event to.</param>
        /// <param name="hex">The hex to unfocus.</param>
        public static void PushUnfocus(this EventQueue eventQueue, Hex hex) {
            // Doing stuff in reverse, so it is then processed in order
            eventQueue.PushWait(0.1);
            eventQueue.Push(() => {
                hex.ShowingOverlay = false;
            });
        }
        /// <summary>
        /// Creates an event that focuses on two hexes at the same time. Then pushes it into the EventQueue.
        /// All focused hexes should be unfocused by an apropriate unfocus method.
        /// </summary>
        /// <param name="eventQueue">The EventQueue to push the event to.</param>
        /// <param name="one">First of the hexes to focus on.</param>
        /// <param name="two">Second of the hexes to focus on.</param>
        public static void PushFocusOn(this EventQueue eventQueue, Hex one, Hex two) {
            // Doing stuff in reverse, so it is then processed in order
            eventQueue.PushWait(0.5);
            eventQueue.Push(() => {
                one.ShowingOverlay = true;
                two.ShowingOverlay = true;
            });
            // Scroll to average of hexes
            Point scroll = one.PixelLocation.Add(two.PixelLocation).Scale(0.5);
            eventQueue.PushCenterScroll(scroll.Add(Constants.HexSize.Scale(0.5)));
        }
        /// <summary>
        /// Creates an event that unfocuses two hexes at the same time. Then pushes it into the EventQueue.
        /// </summary>
        /// <param name="eventQueue">The EventQueue to push the event to.</param>
        /// <param name="one">First of the hexes to unfocus.</param>
        /// <param name="two">Second of the hexes to unfocus.</param>
        public static void PushUnfocus(this EventQueue eventQueue, Hex one, Hex two) {
            // Doing stuff in reverse, so it is then processed in order
            eventQueue.PushWait(0.1);
            eventQueue.Push(() => {
                one.ShowingOverlay = false;
                two.ShowingOverlay = false;
            });
        }

        private static void PushOrEnqueue(this EventQueue eventQueue, bool push, Action action) {
            if (push) {
                eventQueue.Push(action);
            } else {
                eventQueue.Enqueue(action);
            }
        }
        private static void PushOrEnqueueCenterScroll(this EventQueue eventQueue, bool push, Point location) {
            Point viewSize = Match.Current.layout.hexesRectangle.Size();
            Point targetOffset = location.Sub(viewSize.Scale(0.5));
            eventQueue.PushOrEnqueue(push, () => {
                Match.Current.ScrollTo(targetOffset.ToVector());
            });
        }
        #endregion

        /// <summary>
        /// An ability that creates an action of a hex attacking a random unit in range, if any.
        /// If such a unit exists, the action is enqueued into the EventQueue.
        /// </summary>
        /// <param name="source">Source of the potential attack.</param>
        /// <param name="eventQueue">EventQueue to push stuff to.</param>
        public static void AttackRandomEnemyUnit(Hex source, EventQueue eventQueue) {
            eventQueue.Enqueue(() => {
                List<Hex> validTargets = source.GetValidTargets().Where(
                    // Only attack units
                    (hex) => { return hex is Unit; }
                ).ToList();

                if (validTargets.Count < 1)
                    return;
                int ix = MainGame.random.Next(validTargets.Count);

                Hex target = validTargets[ix];

                // Do hit animation
                // Doing stuff in reverse, so it is then processed in order
                eventQueue.PushUnfocus(source, target);
                eventQueue.PushWait(0.5);
                eventQueue.Push(() => {
                    source.Attack(target);
                });
                eventQueue.PushFocusOn(source, target);
            });
        }
    }
}
