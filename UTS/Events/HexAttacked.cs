﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using UTS.Hexes;
using UTS.Units;

namespace UTS.Events {
    public delegate void HexAttackedEventHandler(HexAttackedEventArgs args);

    public class HexAttackedEventArgs {
        public Hex attacker;
        public Hex attacked;
        public int damage;
        public HexAttackedEventArgs(Hex attacker, Hex attacked, int damage) {
            this.attacker = attacker;
            this.attacked = attacked;
            this.damage = damage;
        }
    }
}
