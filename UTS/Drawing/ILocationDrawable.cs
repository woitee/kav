﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UTS.Drawing {
    /// <summary>
    /// An interface describing a class, that can be drawed to an externally suplied location.
    /// </summary>
    public interface ILocationDrawable {
        /// <summary>
        /// Draws this to an externally given location.
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch to draw on.</param>
        /// <param name="location">Location to draw to.</param>
        /// <param name="RequestRemove">Returns true if the graphics doesn't want to be drawed anymore ever.</param>
        void DrawTo(SpriteBatch spriteBatch, Vector2 location, out bool RequestRemove);
        /// <summary>
        /// Width of the drawn item.
        /// </summary>
        int Width { get; }
        /// <summary>
        /// Height of the drawn item.
        /// </summary>
        int Height { get; }
    }

    /// <summary>
    /// An implementation of the ILocationDrawable to draw a Texture2D.
    /// </summary>
    public class Texture2DILocationDrawable : ILocationDrawable {
        private Texture2D texture;
        public Texture2DILocationDrawable(Texture2D texture) {
            this.texture = texture;
        }

        public virtual void DrawTo(SpriteBatch spriteBatch, Vector2 location, out bool ShallRemove) {
            spriteBatch.Draw(texture, location, Color.White);
            ShallRemove = false;
        }

        public int Width { get { return texture.Width; } }
        public int Height { get { return texture.Height; } }
        public Texture2D Texture { get { return texture; } }
    }

    /// <summary>
    /// Static class to generate ILocationDrawable instances from Texture2D objects.
    /// </summary>
    public static class ILocationDrawableStatic {
        public static ILocationDrawable FromTexture2D(Texture2D texture) {
            return new Texture2DILocationDrawable(texture);
        }
    }
}
