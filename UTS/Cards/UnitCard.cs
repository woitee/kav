﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UTS.Units;
using UTS.Drawing;
using UTS.Buildings;
using UTS.Utils;

namespace UTS.Cards {
    /// <summary>
    /// A common ancestor of all unit cards.
    /// These cards can usually be played on any building with CanSpawnUnits = true.
    /// </summary>
    class UnitCard : HexCard {
        protected Unit unit;

        public UnitCard(ILocationDrawable sprite, Unit unit)
            : base(CardType.Unit, sprite, unit) {
            this.unit = unit;
        }

        protected List<Building> showingOverlays = new List<Building>();

        protected bool CanSpawnAt(Point hexLoc) {
            Match match = Match.Current;

            Unit unit = match.units[hexLoc];
            Building building = match.buildings[hexLoc];

            return unit == null &&
                   building != null &&
                   building.color == match.CurrentPlayer.color &&
                   building.CanSpawnUnits;
        }

        public override void Selected() {
            base.Selected();

            Match match = Match.Current;
            foreach (var keyVal in match.buildings) {
                Point hex = keyVal.Key;
                Building building = keyVal.Value;
                if (CanSpawnAt(hex)) {
                    showingOverlays.Add(building);
                    building.ShowingOverlay = true;
                }
            }
        }
        public override void Unselected() {
            base.Unselected();

            foreach (var building in showingOverlays) {
                building.ShowingOverlay = false;
            }
        }

        public override bool TryUseOnHex(Point hexLoc) {
            return TryUseOnHex(hexLoc,
                (hex) => { return CanSpawnAt(hex); },
                (hex) =>  { Match.Current.AddUnit(unit, hex); }
            );
        }

        public override void DrawTo(SpriteBatch spriteBatch, Vector2 location, out bool requestRemove) {
            base.DrawTo(spriteBatch, location, out requestRemove);
            
            TextUtils.DrawCenteredTextWithOutline(spriteBatch, location + new Vector2(132, 119), unit.MovementRange.ToString());
        }
    }
}
