﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UTS.Drawing;
using UTS.Hexes;
using UTS.Utils;

namespace UTS.Cards {
    /// <summary>
    /// A common ancestor of BuildingCards and UnitCards.
    /// </summary>
    class HexCard : Card{
        protected Hex hex;

        public HexCard(CardType cardType, ILocationDrawable sprite, Hex hex) 
            : base(cardType, sprite) {

                this.hex = hex;
        }

        public override void DrawTo(SpriteBatch spriteBatch, Vector2 location, out bool requestRemove) {
            base.DrawTo(spriteBatch, location, out requestRemove);
            TextUtils.DrawCenteredTextWithOutline(spriteBatch, location + new Vector2(16, 65), hex.Health.ToString());
            TextUtils.DrawCenteredTextWithOutline(spriteBatch, location + new Vector2(16, 119), hex.Defense.ToString());
            if (hex.attackType != Hex.AttackType.None) {
                if (hex.attackType == Hex.AttackType.Boulder || hex.attackType == Hex.AttackType.Ranged) {
                    TextUtils.DrawCenteredTextWithOutline(spriteBatch, location + new Vector2(130, 55), hex.AttackDamage.ToString());
                    TextUtils.DrawCenteredTextWithOutline(spriteBatch, location + new Vector2(136, 72), hex.AttackDamage.ToString());
                } else {
                    TextUtils.DrawCenteredTextWithOutline(spriteBatch, location + new Vector2(132, 65), hex.AttackDamage.ToString());
                }
            }

            TextUtils.DrawCenteredTextWithOutline(spriteBatch, location + new Vector2(64, 228), hex.FoodCost.ToString());
            TextUtils.DrawCenteredTextWithOutline(spriteBatch, location + new Vector2(94, 228), hex.PeopleCost.ToString());
            TextUtils.DrawCenteredTextWithOutline(spriteBatch, location + new Vector2(124, 228), hex.Upkeep.ToString());
        }

        protected bool TryUseOnHex(Point hexLoc, Func<Point, bool> canPlayAt, Action<Point> playCard) {
            Match match = Match.Current;
            if (canPlayAt(hexLoc)) {

                Player curPlayer = match.CurrentPlayer;
                if (curPlayer.food < hex.FoodCost || curPlayer.people < hex.PeopleCost) {
                    Hex.CreateFlowingText(hexLoc, "Insufficient resources", GameColors.ReallyBadColor);
                    return false;
                }
                curPlayer.food -= hex.FoodCost;
                curPlayer.people -= hex.PeopleCost;

                //can succesfully play this card on a field
                PlayerColor color = match.CurrentPlayer.color;
                hex.ChangeColor(color);
                playCard(hexLoc);
                return true;
            }
            return false;
        }
    }
}
