﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using UTS.Buildings;

namespace UTS.Cards {
    class WatchCard : BuildingCard {
        public WatchCard()
            : base(
                Textures.CardWatch,
                new Watch(PlayerColor.Blue)
            ) { }

        protected override List<Point> BuildableLocations() {
            Match match = Match.Current;
            return match.Navigator.AvailableBuildingSpots(match.CurrentPlayer, 3);
        }
    }
}
