﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UTS.Drawing;

namespace UTS.Units {
    class Pikeman : Unit {
        public Pikeman(PlayerColor color)
            : base( new SpriteOfEachColor(Textures.PikemanBlue, Textures.PikemanRed),
                    new SpriteOfEachColor(Textures.PikemanBlueSelected, Textures.PikemanRedSelected),
                    Textures.ScrollMidPikemanTexture,
                    color
            ) {
        }

        public override int AttackDamageTo(Hexes.Hex other, ref Hexes.AttackBonus bonus) {
            Unit unit = other as Unit;
            if (unit != null && unit.unitType == UnitType.Cavalry) {
                return base.AttackDamageTo(other, ref bonus) + 4;
            }
            return base.AttackDamageTo(other, ref bonus);
        }

        public override int DefenseFrom(Hexes.Hex other, ref Hexes.AttackBonus bonus) {
            Unit unit = other as Unit;
            if (unit != null && unit.unitType == UnitType.Cavalry) {
                return base.DefenseFrom(other, ref bonus) + 2;
            }
            return base.DefenseFrom(other, ref bonus);
        }
    }
}
