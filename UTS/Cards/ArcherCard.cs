﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UTS.Cards {
    class ArcherCard : UnitCard {
        public ArcherCard()
            : base(
                Textures.CardArcher,
                new Units.Archer(PlayerColor.Blue)
                ) { }

    }
}
