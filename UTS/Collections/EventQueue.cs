﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTS.Collections {
    /// <summary>
    /// A queue that provides methods to enqueue actions and time spacing between them,
    /// and then process them one by one.
    /// 
    /// Processing will always return as soon as possible, so the method should be called in
    /// a loop, and more actions will be processed as time goes forward.
    /// </summary>
    public class EventQueue {
        protected class Event {
            public bool isWait;
            public double waitSeconds;
            public Action action;

            protected Event() {}
            public static Event Wait(double waitSeconds) {
                Event ev = new Event();
                ev.isWait = true;
                ev.waitSeconds = waitSeconds;
                return ev;
            }
            public static Event Action(Action action) {
                Event ev = new Event();
                ev.isWait = false;
                ev.action = action;
                return ev;
            }
        }
        protected Deque<Event> deque = new Deque<Event>();

        public void Enqueue(Action action) {
            deque.AddToBack(Event.Action(action));
        }

        public void EnqueueWait(double seconds) {
            deque.AddToBack(Event.Wait(seconds));
        }

        public void Push(Action action) {
            deque.AddToFront(Event.Action(action));
        }

        public void PushWait(double seconds) {
            deque.AddToFront(Event.Wait(seconds));
        }

        protected DateTime lastWaitAt;
        protected double lastWaitSeconds;
        protected bool waiting = false;
        /// <summary>
        /// Processes stored events until it reaches a wait. Then it starts waiting and returns.
        /// If waiting, it only starts processing events again after the wait time has passed.
        /// Returns false if waiting, true otherwise.
        /// </summary>
        /// <returns>False if waiting, true otherwise.</returns>
        public bool Process() {
            if (waiting) {
                DateTime now = DateTime.Now;
                if ((now - lastWaitAt).TotalSeconds < lastWaitSeconds) {
                    // Still waiting
                    return false;
                } else {
                    // Not waiting anymore
                    waiting = false;
                }
            }


            while (deque.Count > 0) {
                Event ev = deque.RemoveFromFront();
                if (!ev.isWait) {
                    ev.action();
                } else {
                    // Start waiting
                    waiting = true;
                    lastWaitAt = DateTime.Now;
                    lastWaitSeconds = ev.waitSeconds;
                    return false;
                }
            }
            return true;
        }
    }
}
