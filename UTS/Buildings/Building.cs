﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UTS.Drawing;
using UTS.Hexes;
using UTS.Events;
using UTS.Units;

namespace UTS.Buildings {
    /// <summary>
    /// A common ancestor for all buildings.
    /// </summary>
    public abstract class Building : Hex {
        public Building(SpriteOfEachColor sprites, Texture2D innerScroll, PlayerColor color)
                : base(sprites, innerScroll, color) {

            primaryLayer = Match.Layer.PrimaryHex;
            defaultLayer = Match.Layer.Buildings;
            currentLayer = defaultLayer;
            SetBasicAttributes();
        }

        public void SetBasicAttributes() {
            BuildingBasicValue values = BasicAttributes.buildings[this.GetType()];
            Health = values.health;
            AttackDamage = values.attackDamage;
            AttackRange = values.attackRange;
            attackType = values.attackType;
            Defense = values.defense;
            FoodCost = values.foodCost;
            PeopleCost = values.peopleCost;
            Upkeep = values.upkeep;
        }

        public Unit DiedHere = null;
        public bool CanSpawnUnits = false;

        protected override bool CanShowInfo {
            get {
                Match match = Match.Current;
                if (!base.CanShowInfo)
                    return false;
                if (match.units[Location] != null && match.units[Location].IsTravelling)
                    return false;
                return true;
            }
        }

        public override void CreatedAt(Point hex) {
            base.CreatedAt(hex);
            Match.Current.BuildingCreated(new BuildingCreatedEventArgs(this));
        }

        protected override void DrawOverlays(SpriteBatch spriteBatch, Vector2 location, Color tint, double scale) {
            base.DrawOverlays(spriteBatch, location, tint, scale);
            Unit unit = Match.Current.units[this.Location];
            // units that just died here are drawn by buildings
            if (DiedHere != null) {
                bool shallRemove;
                DiedHere.DrawCalledByBuilding(spriteBatch, location, out shallRemove);
                if (shallRemove)
                    DiedHere = null;
            }
            // units on top of buildings are drawn by buildings
            if (unit != null && unit.shallBeDrawnByBuilding) {
                bool unused;
                unit.DrawCalledByBuilding(spriteBatch, location, out unused);
            }
        }
    }
}
