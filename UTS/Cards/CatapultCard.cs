﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UTS.Cards {
    class CatapultCard : UnitCard {
        public CatapultCard()
            : base(
                Textures.CardCatapult,
                new Units.Catapult(PlayerColor.Blue)
                ) { }
    }
}
