﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using UTS.Controllers;
using UTS.Cards;
using UTS.Units;
using UTS.Hexes;
using UTS.Events;
using UTS.Collections;
using UTS.Utils;
using UTS.Drawing;

namespace UTS {
    /// <summary>
    /// A class that represents a game player in a match, and contains all its info.
    /// </summary>
    public class Player {
        public PlayerColor color { get; set; }
        /// <summary>
        /// A controller this player is controled by.
        /// </summary>
        public Controller Controller { get; protected set; }
        /// <summary>
        /// Deck of cards of this player.
        /// </summary>
        public Deck deck;
        /// <summary>
        /// Cards currently in hand.
        /// </summary>
        public List<Card> hand { get; protected set; }

        public int index { get; protected internal set; }

        /// <summary>
        /// Scrolling offset the player ended the turn at.
        /// </summary>
        public Vector2 endTurnOffset = Vector2.Zero;

        //private variables, so that assigning to public properties triggers events
        private int _foodIncome;
        private int _peopleIncome;

        public int food = 5;
        public int foodCapacity;
        public int foodIncome {
            get { return _foodIncome; }
            set {
                Match.Current.FoodIncomeChanged(new FoodIncomeChangedEventArgs(this, value - _foodIncome));
                _foodIncome = value;
            }
        }
        /// <summary>
        /// Amount of food distributed to units each turn. Reduces food income.
        /// </summary>
        public int upkeep;

        public int people = 5;
        public int peopleCapacity;
        public int peopleIncome {
            get { return _peopleIncome; }
            set {
                Match.Current.PeopleIncomeChanged(new PeopleIncomeChangedEventArgs(this, value - _peopleIncome));
                _peopleIncome = value;
            }
        }

        public Player(PlayerColor color, Controller controller, Deck deck) {
            this.color = color;
            this.Controller = controller;
            this.deck = deck;
            this.deck.Shuffle();

            this.hand = new List<Card>();
            hand.Add(deck.DrawCard());
            hand.Add(deck.DrawCard());
            hand.Add(deck.DrawCard());
        }
        public Player(PlayerColor color, Controller controller)
            : this(color, controller, Deck.Default(color)) {
        }

        /// <summary>
        /// Adds default turn starting actions to the EventQueue.
        /// </summary>
        /// <param name="eventQueue">EventQueue for turn starting actions.</param>
        public void StartTurn(EventQueue eventQueue) {
            Match match = Match.Current;
            
            int killedUnitsUpkeep = 0;
            while (food + foodIncome - upkeep + killedUnitsUpkeep < 0) {
                int unitUpkeep;
                KillRandomUnit(eventQueue, out unitUpkeep);
                killedUnitsUpkeep += unitUpkeep;
            }
            eventQueue.Enqueue(() => {
                GainFood(foodIncome - upkeep);
                GainPeople(peopleIncome);
            });
        }

        protected int foodGainedRecently = 0;
        /// <summary>
        /// Gains or looses food, with a proper notification text showing, and limited to current capacity.
        /// </summary>
        /// <param name="amount">Amount of the food gained. Can be negative for food loss.</param>
        public void GainFood(int amount) {
            Match match = Match.Current;
            
            food += amount;
            food = food.LimitToRange(0, foodCapacity);

            // Dont show text for other players
            if (this != match.CurrentPlayer) {
                return;
            }

            // Show cummulative gain
            amount += foodGainedRecently;
            foodGainedRecently = amount;
            Color textColor = GameColors.BadColor;
            string amountString = amount.ToString();
            if (amount >= 0) {
                amountString = "+" + amountString;
                textColor = GameColors.GoodColor;
            }

            match.guiOverlay.ShowTextByFood(amountString, textColor, () => { foodGainedRecently = 0; });
        }

        protected int peopleGainedRecently = 0;
        /// <summary>
        /// Gains or looses people, with a proper notification text showing, and limited to current capacity.
        /// </summary>
        /// <param name="amount">Number of people gained. Can be negative for people loss.</param>
        public void GainPeople(int amount) {
            Match match = Match.Current;

            people += amount;
            people = people.LimitToRange(0, peopleCapacity);

            // Dont show text for other players
            if (this != match.CurrentPlayer) {
                return;
            }

            // Show cummulative gain
            amount += peopleGainedRecently;
            peopleGainedRecently = amount;
            Color textColor = GameColors.BadColor;
            string amountString = amount.ToString();
            if (amount >= 0) {
                amountString = "+" + amountString;
                textColor = GameColors.GoodColor;
            }

            match.guiOverlay.ShowTextByPeople(amountString, textColor, () => { peopleGainedRecently = 0; });
        }

        /// <summary>
        /// Kills a random unit of this player, and adds that to the EventQueue.
        /// </summary>
        /// <param name="eventQueue">The EventQueue to add the unit kill to.</param>
        /// <param name="reducedUpkeepBy">How much upkeep has the killed unit consumed.</param>
        protected void KillRandomUnit(EventQueue eventQueue, out int reducedUpkeepBy) {
            Match match = Match.Current;

            List<Unit> aliveUnits = match.units.Values.Where((unit) => { return unit.color == color; }).ToList();
            Unit chosen = aliveUnits.RandomElem();

            reducedUpkeepBy = chosen.Upkeep;

            eventQueue.EnqueueFocusOn(chosen);
            eventQueue.Enqueue(() => {
                chosen.Die();
                chosen.CreateFlowingText("Starved", GameColors.ReallyBadColor);
            });
            eventQueue.EnqueueWait(0.5);
        }
    }
}
