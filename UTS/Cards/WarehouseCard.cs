﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UTS.Buildings;

namespace UTS.Cards {
    class WarehouseCard : BuildingCard {
        public WarehouseCard()
            : base(
                Textures.CardWarehouse,
                new Warehouse(PlayerColor.Blue)
            ) { }
    }
}
