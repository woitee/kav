﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using UTS.Collections;
using UTS.Hexes;
using UTS.Units;
using UTS.Drawing;

namespace UTS.Buildings {
    class Field : Building {
        public Field(PlayerColor color)
            : base(
                new SpriteOfEachColor(Textures.FieldBlue, Textures.FieldRed),
                Textures.ScrollMidFieldTexture,
                color
            ) {

        }

        public override void CreatedAt(Point hex) {
            base.CreatedAt(hex);
            Player myPlayer = Match.Current.GetPlayer(color);
            myPlayer.foodIncome += 1;
            myPlayer.foodCapacity += 3;
        }

        public override void Die() {
            base.Die();
            Player myPlayer = Match.Current.GetPlayer(color);
            myPlayer.foodIncome -= 1;
            myPlayer.foodCapacity -= 3;
        }
    }
}
