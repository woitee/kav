﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using UTS.Drawing;
using UTS.Hexes;
using UTS.Units;
using UTS.Utils;

namespace UTS.Cards {
    /// <summary>
    /// A common ancestor of all event cards.
    /// These cards are defaultly played on any hex and cause something to happen, but that behaviour can be overriden.
    /// </summary>
    class EventCard : Card {
        public int FoodCost { get; protected set; }
        public int PeopleCost { get; protected set; }
        public int Upkeep { get; protected set; }

        public EventCard(ILocationDrawable Sprite) : base(CardType.Event, Sprite) {
            SetBasicAttributes();
        }

        public void SetBasicAttributes() {
            EventBasicValue values = BasicAttributes.events[this.GetType()];
            FoodCost = values.foodCost;
            PeopleCost = values.peopleCost;
            Upkeep = values.upkeep;
        }

        public override void DrawTo(SpriteBatch spriteBatch, Vector2 location, out bool requestRemove) {
            base.DrawTo(spriteBatch, location, out requestRemove);
            TextUtils.DrawCenteredTextWithOutline(spriteBatch, location + new Vector2(64, 228), FoodCost.ToString());
            TextUtils.DrawCenteredTextWithOutline(spriteBatch, location + new Vector2(94, 228), PeopleCost.ToString());
            TextUtils.DrawCenteredTextWithOutline(spriteBatch, location + new Vector2(124, 228), Upkeep.ToString());
        }

        protected bool TryPayCost(Point location) {
            Match match = Match.Current;
            Player curPlayer = match.CurrentPlayer;

            if (curPlayer.food < FoodCost || curPlayer.people < PeopleCost) {
                Hex.CreateFlowingText(location, "Insufficient resources", GameColors.ReallyBadColor);
                return false;
            }
            curPlayer.GainFood(-FoodCost);
            curPlayer.GainPeople(-PeopleCost);
            return true;
        }

        public override bool TryUseOnHex(Point point) {
            return true;
        }
    }
}
