﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using UTS.Hexes;
using UTS.Units;

namespace UTS.Controllers {
    /// <summary>
    /// Controller is an object that can control the actions of a player.
    /// It usually takes some kind of input from the user (mouse, keyboard) etc., 
    /// and translates it to in-game actions.
    /// </summary>
    public abstract class Controller {
        protected bool Enabled = false;

        public abstract void Update(GameTime gameTime);

        public virtual void Enable() {
            Enabled = true;
        }
        public virtual void Disable() {
            Enabled = false;
        }

        // Available actions
        // Here listed are the available actions, that the controller is permitted to make
        // They can be overriden to do something else, or to serve as a notificator
        protected virtual bool TryUseSelectedCardOnHex(Point hexLoc) {
            return Match.Current.TryUseSelectedCardOnHex(hexLoc);
        }
        protected virtual void AttackSelected(Hex hex) {
            Match.Current.AttackSelected(hex);
        }
        protected virtual void MoveSelectedUnit(Point hexLoc) {
            Match.Current.MoveSelectedUnit(hexLoc);
        }
        protected virtual void FinishMoveSelected() {
            Match.Current.FinishMoveSelected();
        }
        protected virtual void UnselectHex() {
            Match.Current.UnselectUnit();
        }
        protected virtual void SelectUnit(Unit unit) {
            Match.Current.SelectUnit(unit);
        }
        protected virtual void SetHoveredHex(Hex hex) {
            Match.Current.HoveredHex = hex;
        }
        protected virtual void UnselectCard() {
            Match.Current.UnselectCard();
        }
        protected virtual void SelectCard(int cardIndex) {
            Match.Current.SelectCard(cardIndex);
        }
        protected virtual void DiscardCard(int cardIndex) {
            Match match = Match.Current;
            match.DiscardCard(cardIndex);
            match.CurrentPlayer.GainFood(1);
        }
        protected virtual void EndTurn() {
            Match.Current.EndTurn();
        }
        protected virtual void StartScrolling(Direction direction) {
            Match.Current.StartScrolling(direction);
        }
        protected virtual void StopScrolling(Orientation orientation) {
            Match.Current.StopScrolling(orientation);
        }
        protected virtual void StopScrolling() {
            Match.Current.StopScrolling();
        }
        protected virtual void ScrollTo(Vector2 offset) {
            Match.Current.ScrollTo(offset);
        }
        protected virtual void GoToMenu(string message = "") {
            Match.Current.GoToMenu(message);
        }
    }
}
