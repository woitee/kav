﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UTS.Drawing;
using UTS.Utils;
using UTS.Units;
using UTS.Buildings;

namespace UTS.Hexes {
    /// <summary>
    /// A sliding info that shows details about a Hex when hovered.
    /// Also draws the scroll underneath.
    /// </summary>
    class HexDetails : ILocationDrawable {
        protected static readonly Vector2 topLeftTextOffset = new Vector2(17, 27);
        protected static readonly Vector2 bottomLeftTextOffset = new Vector2(17, 81);
        protected static readonly Vector2 topRightTextOffset = new Vector2(32, 27);
        protected static readonly Vector2 bottomRightTextOffset = new Vector2(32, 81);

        protected static readonly Vector2 topRightRangedAttackOffset = new Vector2(30, 17);
        protected static readonly Vector2 topRightRangedRangeOffset = new Vector2(36, 34);

        protected bool IsRetracting = false;
        
        protected readonly float minInfoOffset = -23;
        protected readonly float maxInfoOffset = 21;
        protected readonly float minLeftScrollOffset = 43;
        protected readonly float maxLeftScrollOffset = -12;
        protected readonly float minRightScrollOffset = 53;
        protected readonly float maxRightScrollOffset = 108;

        protected readonly float scrollY = 108;

        protected float progress;
        protected float progressChange = 0.02f;

        protected Action retractionCallback;
        protected Hex hex;

        // assigned in constructor
        protected Texture2D leftTexture;
        protected Texture2D rightTexture;

        public HexDetails(Hex hex, Action retractionCallback = null) {
            this.hex = hex;
            this.retractionCallback = retractionCallback;
            progress = 0;

            // Choose which textures to draw
            if (hex as Unit != null) {
                // Unit
                leftTexture = hex.color == PlayerColor.Blue ? Textures.UnitInfoLeftBlueTexture : Textures.UnitInfoLeftRedTexture;
                if (hex.attackType == Hex.AttackType.Melee) {
                    rightTexture = hex.color == PlayerColor.Blue ? Textures.UnitInfoRightBlueTexture : Textures.UnitInfoRightRedTexture;
                } else {
                    rightTexture = hex.color == PlayerColor.Blue ? Textures.UnitInfoRightRangedBlueTexture: Textures.UnitInfoRightRangedRedTexture;
                }
            } else {
                // Building
                if (hex.color == PlayerColor.Neutral) {
                    leftTexture = Textures.BuildingInfoLeftNeutralTexture;
                    rightTexture = Textures.BuildingInfoRightRangedAttackNeutralTexture;
                    return;
                }
                leftTexture = hex.color == PlayerColor.Blue ? Textures.BuildingInfoLeftBlueTexture : Textures.BuildingInfoLeftRedTexture;
                if (hex.AttackRange == 0) {
                    rightTexture = hex.color == PlayerColor.Blue ? Textures.BuildingInfoRightNonAttackBlueTexture : Textures.BuildingInfoRightNonAttackRedTexture;
                } else if (hex.attackType == Hex.AttackType.Melee) {
                    rightTexture = hex.color == PlayerColor.Blue ? Textures.BuildingInfoRightAttackBlueTexture : Textures.BuildingInfoRightAttackRedTexture;
                } else {
                    rightTexture = hex.color == PlayerColor.Blue ? Textures.BuildingInfoRightRangedAttackBlueTexture : Textures.BuildingInfoRightRangedAttackRedTexture;
                }
            }
        }

        public void StartRetracting() {
            IsRetracting = true;
        }
        public void StartExpandingAgain() {
            IsRetracting = false;
        }

        public void DrawTo(SpriteBatch spriteBatch, Vector2 location, out bool ShallRemove) {
            ShallRemove = false;

            // Update percentage
            if (IsRetracting) {
                progress -= progressChange;
                if (progress < 0) {
                    if (retractionCallback != null)
                        retractionCallback();
                    ShallRemove = true;
                }
            } else {
                progress += progressChange;
                progress = progress > 1 ? 1 : progress;
            }
            // Distribute percentage to info and scroll
            float progressInfo = progress * 2f;
            progressInfo = progressInfo.LimitToRange(0, 1);
            float progressScroll = (progress - 0.5f) * 2f;
            progressScroll = progressScroll.LimitToRange(0, 1);

            DrawInfo(spriteBatch, location, progressInfo);
            DrawScroll(spriteBatch, location, progressScroll);
        }
        /// <summary>
        /// Draws the sliding info around a hex.
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch to draw on.</param>
        /// <param name="location">Location of the hex to draw around.</param>
        /// <param name="progress">How much of the info is shown, on range from 0 to 1.</param>
        protected void DrawInfo(SpriteBatch spriteBatch, Vector2 location, float progress) {
            float offsetInfo = (maxInfoOffset - minInfoOffset) * progress + minInfoOffset;

            //====================
            // Draw to spriteBatch
            //====================
            // Calculate draw locations
            Vector2 leftDestination = location - new Vector2(offsetInfo, 0);
            float rightOffset = offsetInfo + Constants.InnerHexWidth - Textures.UnitInfoLeftBlue.Width;
            Vector2 rightDestination = location + new Vector2(rightOffset, 0);

            spriteBatch.Draw(leftTexture, leftDestination, Color.White);
            spriteBatch.Draw(rightTexture, rightDestination, Color.White);

            // Calculate which health to draw, if the first part of hit animation is still running, don't show actual health, but health before hit
            int showHealth = hex.Health;
            if (hex.ShowingHitAnimation && !hex.HitAnimation.isShowingBonus)
                showHealth = hex.HealthBeforeLastHit;

            // Draw text on the details
            TextUtils.DrawCenteredTextWithOutline(spriteBatch, leftDestination + topLeftTextOffset, showHealth.ToString());
            TextUtils.DrawCenteredTextWithOutline(spriteBatch, leftDestination + bottomLeftTextOffset, hex.Defense.ToString());

            if (hex.AttackRange > 0) {
                if (hex.attackType == Hex.AttackType.Melee) {
                    TextUtils.DrawCenteredTextWithOutline(spriteBatch, rightDestination + topRightTextOffset, hex.AttackDamage.ToString());
                } else if (hex.attackType != Hex.AttackType.None) {
                    TextUtils.DrawCenteredTextWithOutline(spriteBatch, rightDestination + topRightRangedAttackOffset, hex.AttackDamage.ToString());
                    TextUtils.DrawCenteredTextWithOutline(spriteBatch, rightDestination + topRightRangedRangeOffset, hex.AttackRange.ToString());
                }
            }

            Unit unit = hex as Unit;
            if (unit != null) {
                TextUtils.DrawCenteredTextWithOutline(spriteBatch, rightDestination + bottomRightTextOffset, unit.MovementRange.ToString());
            }
        }
        /// <summary>
        /// Draws the scroll underneath a hex.
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch to draw on.</param>
        /// <param name="location">Location of the hex to draw underneath.</param>
        /// <param name="progress">How much of the scroll is shown, on range from 0 to 1.</param>
        protected void DrawScroll(SpriteBatch spriteBatch, Vector2 location, float progress) {
            // Calculate outer scroll
            
            float leftScrollDiff = maxLeftScrollOffset - minLeftScrollOffset;
            float rightScrollDiff = maxRightScrollOffset - minRightScrollOffset;

            Vector2 leftDestination = location + new Vector2(minLeftScrollOffset + leftScrollDiff * progress, scrollY);
            Vector2 rightDestination = location + new Vector2(minRightScrollOffset + rightScrollDiff * progress, scrollY);


            // Calculate inner scroll

            int scrollBorderWidth = Textures.ScrollRightTexture.Width;
            int scrollHeight = Textures.ScrollRightTexture.Height;
            Rectangle destinationRectangle = new Rectangle(
                (int)leftDestination.X + scrollBorderWidth,
                (int)leftDestination.Y,
                (int)rightDestination.X - (int)leftDestination.X - scrollBorderWidth,
                scrollHeight
            );
            // Overlap textures a little if possible
            int overlap = hex.InnerScroll.Width - destinationRectangle.Width;
            overlap = overlap.LimitToRange(0, scrollBorderWidth);
            destinationRectangle.X -= overlap / 2;
            destinationRectangle.Width += overlap;

            Rectangle sourceRectangle = destinationRectangle;
            sourceRectangle.Y = 0;
            sourceRectangle.X = (Textures.ScrollMidPikeman.Width - sourceRectangle.Width) / 2;
            // Draw inner scroll
            spriteBatch.Draw(hex.InnerScroll, destinationRectangle, sourceRectangle, Color.White);

            // Draw outer scroll
            spriteBatch.Draw(Textures.ScrollLeftTexture, leftDestination, Color.White);
            spriteBatch.Draw(Textures.ScrollRightTexture, rightDestination, Color.White);

            // Draw upkeep numbers
            if (progress > 0.7)
                TextUtils.DrawCenteredTextWithOutline(spriteBatch, location + new Vector2(97, 179), hex.Upkeep.ToString());
        }

        public int Width { get { return Constants.HexWidth + 2 * (int)maxInfoOffset; } }
        public int Height { get { return Constants.HexHeight; } }
    }
}
