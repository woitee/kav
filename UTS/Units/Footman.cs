﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UTS.Drawing;
using UTS.Hexes;
using UTS.Units;
using UTS.Events;

namespace UTS.Units {
    class Footman : Unit {
        protected HexAttackedEventHandler hexAttackedEventHandler;
        
        public Footman(PlayerColor color)
            : base( new SpriteOfEachColor(Textures.FootmanBlue, Textures.FootmanRed),
                    new SpriteOfEachColor(Textures.FootmanBlueSelected, Textures.FootmanRedSelected),
                    Textures.ScrollMidFootmanTexture,
                    color
            ) {

            hexAttackedEventHandler = (args) => {
                if (args.attacked == this) {
                    GotAttacked(args.attacker);
                }
            };
        }

        public override void CreatedAt(Microsoft.Xna.Framework.Point hex) {
            base.CreatedAt(hex);
            Match.Current.HexAttacked += hexAttackedEventHandler;
        }

        public override void Die() {
            base.Die();
            Match.Current.HexAttacked -= hexAttackedEventHandler;
        }

        public void GotAttacked(Hex attacker) {
            Unit unit = attacker as Unit;
            if (!ItsMyTurn &&
                unit != null &&
                unit.unitType == UnitType.Infantry &&
                unit.attackType == AttackType.Melee) {
                Attack(attacker);
            }
        }
    }
}
