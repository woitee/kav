﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UTS.Buildings;

namespace UTS.Cards {
    class HouseCard : BuildingCard {
        public HouseCard()
            : base(
                Textures.CardHouse,
                new House(PlayerColor.Blue)
            ) { }
    }
}
