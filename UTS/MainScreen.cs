﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UTS.Drawing;
using UTS.Controllers;
using UTS.Utils;
using UTS.Components;

namespace UTS {
    /// <summary>
    /// A visual class representing the Main Menu.
    /// </summary>
    class MainScreen : IDrawer {
        public class Layout {
            public Rectangle IPAddressRectangle;
            public Rectangle StartClientRectangle;
            public Rectangle StartHotSeatRectangle;
            public Rectangle StartServerRectangle;

            public Layout() {
                IPAddressRectangle = new Rectangle(625, 276, 206, 37);
                StartClientRectangle = new Rectangle(563, 219, 217, 43);
                StartHotSeatRectangle = new Rectangle(563, 347, 267, 45);
                StartServerRectangle = new Rectangle(563, 437, 225, 49);
            }
        }
        protected Layout layout = new Layout();
        protected MainGame game;
        protected MouseAssistant mouseAssist;
        protected ClickableAreaManager clickables;
        protected TextField IPtextField;
        /// <summary>
        /// String that is shown on the screen as a notification message.
        /// </summary>
        protected string showString;

        /// <summary>
        /// Initializes a new instance of the MainScreen class.
        /// </summary>
        /// <param name="game">The Game this screen belongs to.</param>
        /// <param name="message">Message that is shown at the start as a notification.</param>
        public MainScreen(MainGame game, string message = "") {
            this.game = game;
            this.showString = message;
            mouseAssist = new MouseAssistant();
            clickables = new ClickableAreaManager(mouseAssist);

            clickables.AddClickableArea(layout.StartClientRectangle, StartClient);
            clickables.AddClickableArea(layout.StartHotSeatRectangle, StartHotSeat);
            clickables.AddClickableArea(layout.StartServerRectangle, StartServer);
            IPtextField = new TextField(Fonts.IPAddress, layout.IPAddressRectangle);
            IPtextField.Text = "127.0.0.1";
            IPtextField.ActionCallback = StartClient;
        }

        /// <summary>
        /// Starts a Hot Seat game.
        /// </summary>
        protected void StartHotSeat() {
            game.updateCallbacks.Enqueue(() => {
                game.drawers.Remove(this);

                Player[] players = new Player[] {
                    new Player(PlayerColor.Blue, new MouseController()),
                    new Player(PlayerColor.Red, new MouseController())
                };

                Match match = new Match(game, new Maps.LakeMap(), Match.Mode.Hotseat, players);
                game.drawers.Add(match);
            });
        }

        /// <summary>
        /// Starts a Server that listens for a connection and when that is made, starts the game.
        /// </summary>
        protected void StartServer() {
            clickables.Enabled = false;

            showString = "Waiting for connection...";
            TcpListener tcpListener = new TcpListener(IPAddress.Any, Constants.Port);
            Task.Factory.StartNew(() => {
                try {
                    tcpListener.Start(10);
                    TcpClient tcpClient = tcpListener.AcceptTcpClient();
                    game.gameExitingCallbacks.Enqueue(tcpClient.Close);
                    StartServer(tcpClient);
                } catch (Exception) {
                    showString = "Connection error.";
                    clickables.Enabled = true;
                }
            }, CancellationToken.None, TaskCreationOptions.LongRunning, TaskScheduler.Default);
        }

        /// <summary>
        /// Starts the game from the server-side when a connection is already initiated.
        /// </summary>
        /// <param name="tcpClient">Connected client.</param>
        protected void StartServer(TcpClient tcpClient) {
            game.updateCallbacks.Enqueue(() => {
                game.drawers.Remove(this);

                Player[] players = new Player[] {
                    new Player(PlayerColor.Blue, new OutgoingConnectionMouseCtrl(tcpClient)),
                    new Player(PlayerColor.Red, new IncomingConnectionController(tcpClient))
                };

                Match match = new Match(game, new Maps.LakeMap(), Match.Mode.Network, players);
                game.drawers.Add(match);
            });
        }

        /// <summary>
        /// Starts a Client that tries to connect to a server and when a connection is made, starts the game.
        /// It gains the target IPAddress from the IPtextField.
        /// </summary>
        protected void StartClient() {
            // Try parse IPAddress
            IPAddress targetIP;
            if (!IPAddress.TryParse(IPtextField.Text, out targetIP)) {
                showString = "Invalid IP Address.";
                return;
            }

            // IP Address parsed, try connect
            clickables.Enabled = false;

            TcpClient tcpClient = new TcpClient();
            try {
                tcpClient.Connect(new IPEndPoint(targetIP, Constants.Port));
                game.gameExitingCallbacks.Enqueue(tcpClient.Close);

                StartClient(tcpClient);
            } catch (Exception) {
                showString = "Connection error.";
                clickables.Enabled = true;
            }
        }

        /// <summary>
        /// Starts the game from the client-side when a connection is already initiated.
        /// </summary>
        /// <param name="tcpClient"></param>
        protected void StartClient(TcpClient tcpClient) {
            game.updateCallbacks.Enqueue(() => {
                game.drawers.Remove(this);

                Player[] players = new Player[] {
                    new Player(PlayerColor.Red, new IncomingConnectionController(tcpClient)),
                    new Player(PlayerColor.Blue, new OutgoingConnectionMouseCtrl(tcpClient))
                };

                Match match = new Match(game, new Maps.LakeMap(), Match.Mode.Network, players);
                game.drawers.Add(match);
            });
        }

        public void Update(GameTime gameTime) {
            mouseAssist.Update(gameTime);
            clickables.Update(gameTime);
            IPtextField.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch) {
            bool unused;
            Textures.MainScreen.DrawTo(spriteBatch, Vector2.Zero, out unused);
            TextUtils.DrawCenteredTextWithOutline(spriteBatch, new Vector2(682, 668), showString);
            IPtextField.Draw(spriteBatch);
        }
    }
}
