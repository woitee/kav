﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using UTS.Units;

namespace UTS.Events {
    public delegate void UnitMovedEventHandler(UnitMovedEventArgs args);

    public class UnitMovedEventArgs {
        public Unit unit;
        public Point fromHex;
        public Point toHex;
        public UnitMovedEventArgs(Unit unit, Point fromHex, Point toHex) {
            this.unit = unit;
            this.fromHex = fromHex;
            this.toHex = toHex;
        }
    }
}
