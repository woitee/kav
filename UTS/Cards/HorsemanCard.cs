﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UTS.Cards {
    class HorsemanCard : UnitCard {
        public HorsemanCard()
            : base(
                Textures.CardHorseman,
                new Units.Horseman(PlayerColor.Blue)
                ) { }

    }
}
