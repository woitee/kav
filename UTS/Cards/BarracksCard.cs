﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UTS.Buildings;

namespace UTS.Cards {
    class BarracksCard : BuildingCard {
        public BarracksCard()
            : base(
                Textures.CardBarracks,
                new Barracks(PlayerColor.Blue)
            ) { }
    }
}
