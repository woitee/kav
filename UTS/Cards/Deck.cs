﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UTS.Utils;

namespace UTS.Cards {
    public class Deck {
        private List<Card> deck;
        
        public Deck(List<Card> deckList) {
            deck = deckList;
        }

        public void Add(Card card) {
            deck.Add(card);
        }

        public void Shuffle() {
            deck.Shuffle();
        }

        public Card DrawCard() {
            int last = deck.Count - 1;
            Card drawn = deck[last];
            deck.RemoveAt(last);
            return drawn;
        }

        public int Count {
            get { return deck.Count; }
        }

        public static Deck Default(PlayerColor color) {
            return new Deck(new List<Card>(new Card[] {
                new PeasantCard(),
                new PeasantCard(),
                new PeasantCard(),
                new PeasantCard(),

                new FootmanCard(),
                new FootmanCard(),
                new FootmanCard(),

                new KnightCard(),
                new KnightCard(),
                new KnightCard(),

                new HorsemanCard(),
                new HorsemanCard(),
                new HorsemanCard(),

                new ArcherCard(),
                new ArcherCard(),
                new ArcherCard(),

                new CatapultCard(),
                new CatapultCard(),
                new CatapultCard(),

                new PikemanCard(),
                new PikemanCard(),

                new MarshalCard(),

                // buildings

                new FieldCard(),
                new FieldCard(),
                new FieldCard(),
                new FieldCard(),
                new FieldCard(),
                new FieldCard(),
                new FieldCard(),
                new FieldCard(),
                new FieldCard(),

                new ChapelCard(),
                new ChapelCard(),

                new HouseCard(),
                new HouseCard(),
                new HouseCard(),
                new HouseCard(),

                new BarracksCard(),
                new BarracksCard(),

                new TavernCard(),
                new TavernCard(),

                new WarehouseCard(),
                new WarehouseCard(),
                new WarehouseCard(),

                new WatchCard(),
                new WatchCard(),

                // events

                new WeaponsCard(),
                new WeaponsCard(),

                new ArmoredCard(),
                new ArmoredCard(),

                new FeastCard(),
                new FeastCard(),

                new HarvestCard(),
                new HarvestCard(),

                new AssaultCard(),
                new AssaultCard(),
                new AssaultCard(),

                new KnowledgeCard(),
                new KnowledgeCard(),

                new PlagueCard()
            }));
        }

        public static Deck Pikemen(PlayerColor color) {
            return new Deck(new List<Card>(new Card[] {
                new PikemanCard(),
                new PikemanCard(),
                new PikemanCard(),
                new PikemanCard()
            }));
        }

        public static Deck Footmen(PlayerColor color) {
            return new Deck(new List<Card>(new Card[] {
                new FootmanCard(),
                new FootmanCard(),
                new FootmanCard(),
                new FootmanCard()
            }));
        }
    }
}
