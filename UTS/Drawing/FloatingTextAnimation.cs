﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UTS.Utils;

namespace UTS.Drawing {
    /// <summary>
    /// A graphical element, floating text.
    /// It is a floating text with a specific color, floating in some direction, fading after a given time. 
    /// </summary>
    class FloatingTextAnimation : ILocationDrawable{
        protected readonly float moveSpeed = 0.15f;

        protected string text;
        protected Color textColor;
        protected Direction direction;
        protected double fadeoutTime;
        protected Action fadeCallback;

        protected DateTime createdAt;
        protected float offset = 0;

        public int Height { get; protected set; }
        public int Width { get; protected set; }

        /// <summary>
        /// Creates a custom FloatingText.
        /// </summary>
        /// <param name="text">The text to display.</param>
        /// <param name="textColor">Color of the text to display.</param>
        /// <param name="direction">Direction in which the text will slightly move.</param>
        /// <param name="fadeoutTime">Time in seconds, after which the text should fade out.</param>
        public FloatingTextAnimation(string text, Color textColor, Direction direction, double fadeoutTime, Action fadeCallback = null) {
            this.text = text;
            this.textColor = textColor;
            this.direction = direction;
            this.fadeoutTime = fadeoutTime;
            this.fadeCallback = fadeCallback;
            createdAt = DateTime.Now;

            Vector2 size = Fonts.Arial14.MeasureString(text);
            Height = (int)size.X;
            Width = (int)size.Y;
        }

        public void DrawTo(SpriteBatch spriteBatch, Vector2 location, out bool RequestRemove) {
            DateTime now = DateTime.Now;
            if ((now - createdAt).TotalSeconds > fadeoutTime) {
                RequestRemove = true;
                if (fadeCallback != null)
                    fadeCallback();
                return;
            }
            RequestRemove = false;

            offset += moveSpeed;
            switch (direction) {
                case Direction.Left:
                    location.X -= offset;
                    break;
                case Direction.Right:
                    location.X += offset;
                    break;
                case Direction.Up:
                    location.Y -= offset;
                    break;
                case Direction.Down:
                    location.Y += offset;
                    break;
                default:
                    break;
            }
            
            TextUtils.DrawCenteredTextWithOutline(spriteBatch, location, text, textColor);
        }
    }
}
