﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using UTS.Collections;
using UTS.Hexes;
using UTS.Units;
using UTS.Drawing;

namespace UTS.Buildings {
    class Tavern : Building {
        public Tavern(PlayerColor color)
            : base(
                new SpriteOfEachColor(Textures.TavernBlue, Textures.TavernRed),
                Textures.ScrollMidTavernTexture,
                color
            ) {

        }

        public override void CreatedAt(Point hex) {
            base.CreatedAt(hex);
            Player myPlayer = Match.Current.GetPlayer(color);
            myPlayer.peopleIncome += 3;
            myPlayer.peopleCapacity += 2;
        }

        public override void Die() {
            base.Die();
            Player myPlayer = Match.Current.GetPlayer(color);
            myPlayer.peopleIncome -= 3;
            myPlayer.peopleCapacity -= 2;
        }
    }
}
