﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UTS.Hexes;
using UTS.Drawing;

namespace UTS.Units {
    class Knight : Unit {
        public Knight(PlayerColor color)
            : base( new SpriteOfEachColor(Textures.KnightBlue, Textures.KnightRed),
                    new SpriteOfEachColor(Textures.KnightBlueSelected, Textures.KnightRedSelected),
                    Textures.ScrollMidKnightTexture,
                    color
            ) {
        }

        public override int DefenseFrom(Hexes.Hex other, ref AttackBonus bonus) {
            if (other.attackType == AttackType.Ranged &&
                MainGame.random.NextDouble() < 0.66) {
                bonus.text = "Blocked";
                bonus.textColor = GameColors.BadColor;
                bonus.bonus = -9999;
            }
            return base.DefenseFrom(other, ref bonus);
        }
    }
}
