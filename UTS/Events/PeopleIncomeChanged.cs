﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UTS.Events {
    public delegate void PeopleIncomeChangedEventHandler(PeopleIncomeChangedEventArgs args);

    public class PeopleIncomeChangedEventArgs {
        public Player player;
        public int difference;
        public PeopleIncomeChangedEventArgs(Player player, int difference) {
            this.player = player;
            this.difference = difference;
        }
    }
}
