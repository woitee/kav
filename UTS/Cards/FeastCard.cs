﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using UTS.Hexes;

namespace UTS.Cards {
    class FeastCard : EventCard {
        public FeastCard() : base(Textures.CardFeast) { }

        public override bool TryUseOnHex(Point location) {
            Match match = Match.Current;
            Player curPlayer = match.CurrentPlayer;

            if (TryPayCost(location)) {
                curPlayer.GainPeople(curPlayer.peopleIncome);
                return true;
            }

            return false;
        }
    }
}
