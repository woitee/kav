﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using UTS.Drawing;

namespace UTS.Buildings {
    class Barracks : Building {
        public Barracks(PlayerColor color)
            : base(
                new SpriteOfEachColor(Textures.BarracksBlue, Textures.BarracksRed),
                Textures.ScrollMidBarracksTexture,
                color
            ) {

            CanSpawnUnits = true;
        }
    }
}
