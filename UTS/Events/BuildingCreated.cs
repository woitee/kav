﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UTS.Buildings;

namespace UTS.Events {
    public delegate void BuildingCreatedEventHandler(BuildingCreatedEventArgs unit);

    public class BuildingCreatedEventArgs {
        public Building building;
        public BuildingCreatedEventArgs(Building building) {
            this.building = building;
        }
    }
}
