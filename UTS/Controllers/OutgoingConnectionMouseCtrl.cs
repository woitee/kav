﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;

using Microsoft.Xna.Framework;

using UTS.Hexes;
using UTS.Units;
using UTS.Controllers.Messages;


namespace UTS.Controllers {
    /// <summary>
    /// A class that controls the game based on mouse input, and also sends messages of the actions
    /// through network.
    /// </summary>
    class OutgoingConnectionMouseCtrl : MouseController {
        TcpClient tcpClient;
        StreamWriter writer;

        public OutgoingConnectionMouseCtrl(TcpClient tcpClient) {
            this.tcpClient = tcpClient;
            writer = new StreamWriter(tcpClient.GetStream(), Encoding.ASCII, 128);
            writer.AutoFlush = true;
            writer.NewLine = "\r\n";

            int seed = MainGame.random.Next();
            SendMessage(new SetRandomSeedMessage(seed));
            MainGame.random = new Random(seed);
        }

        protected void SendMessage(Message msg) {
            try {
                writer.WriteLine(msg.msgString);
            } catch (Exception) {
                Match.Current.updateCallbacks.Enqueue(() => {
                    GoToMenu("Connection error.");
                });
            }
        }

        protected override bool TryUseSelectedCardOnHex(Point hexLoc) {
            SendMessage(new TryUseSelectedCardOnHexMessage(hexLoc));
            return base.TryUseSelectedCardOnHex(hexLoc);
        }
        protected override void AttackSelected(Hex hex) {
            base.AttackSelected(hex);
            SendMessage(new AttackSelectedMessage(hex));
        }
        protected override void MoveSelectedUnit(Point hexLoc) {
            base.MoveSelectedUnit(hexLoc);
            SendMessage(new MoveSelectedUnitMessage(hexLoc));
        }
        protected override void FinishMoveSelected() {
            base.FinishMoveSelected();
            SendMessage(new FinishMoveSelectedMessage());
        }
        protected override void UnselectHex() {
            base.UnselectHex();
            SendMessage(new UnselectHexMessage());
        }
        protected override void SelectUnit(Unit unit) {
            base.SelectUnit(unit);
            SendMessage(new SelectUnitMessage(unit));
        }
        protected override void UnselectCard() {
            base.UnselectCard();
            SendMessage(new UnselectCardMessage());
        }
        protected override void SelectCard(int cardIndex) {
            base.SelectCard(cardIndex);
            SendMessage(new SelectCardMessage(cardIndex));
        }
        protected override void EndTurn() {
            base.EndTurn();
            SendMessage(new EndTurnMessage());
        }

        protected Vector2 lastSentOffset = Vector2.Zero;
        protected DateTime lastSentOffsetTime = DateTime.Now;
        public override void Update(GameTime gameTime) {
            base.Update(gameTime);

            //Update scrolling
            Vector2 matchOffset = Match.Current.ScrollOffset;
            double secondsSinceLast = (DateTime.Now - lastSentOffsetTime).TotalSeconds;
            if (matchOffset != lastSentOffset &&
                secondsSinceLast > 0.1) {
                lastSentOffset = matchOffset;
                lastSentOffsetTime = DateTime.Now;
                SendMessage(new ScrollToMessage(matchOffset));
            }
        }
    }
}
