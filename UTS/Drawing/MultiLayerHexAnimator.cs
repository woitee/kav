﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UTS.Drawing {
    /// <summary>
    /// An animator, that holds sprites in different levers and can access them by either pixel or hex coordinates.
    /// </summary>
    public class MultiLayerHexAnimator : MultiLayerAnimator {
        /// <summary>
        /// Creates a new MultiLayerHexAnimator, without scrolling capabilities.
        /// </summary>
        /// <param name="positionOnScreen">Location on screen.</param>
        public MultiLayerHexAnimator(Rectangle positionOnScreen) : base(positionOnScreen) { }
        /// <summary>
        /// Creates a new MultiLayerAnimator, with scrolling capabilities.
        /// </summary>
        /// <param name="positionOnScreen">Location on screen.</param>
        /// <param name="size">Size of the Canvas. Can be larger than position on screen,
        ///     extranneous space can be scrolled to.</param>
        public MultiLayerHexAnimator(Rectangle positionOnScreen, Point size) : base(positionOnScreen, size) { }

        /// <summary>
        /// Adds a sprite to the animator.
        /// </summary>
        /// <param name="sprite">Sprite to add.</param>
        /// <param name="location">Hex-coordinates of the sprite.</param>
        /// <param name="layer">Layer to add the sprite to. The layer must be added to the animator before adding to it.</param>
        public void AddHex(ILocationDrawable sprite, Point hex, int layer) {
            AddSprite(sprite, HexAnimator.GetPixelCoordinates(hex), layer);
        }

        /// <summary>
        /// Returns whether a sprite exists at a given layer.
        /// </summary>
        /// <param name="hex">Hex-location where the sprite should be.</param>
        /// <param name="layer">Layer of the potential sprite.</param>
        /// <returns></returns>
        public bool IsHexAt(Point hex, int layer) {
            return SpriteExists(HexAnimator.GetPixelCoordinates(hex), layer);
        }

        /// <summary>
        /// Moves an existing sprite in the animator.
        /// </summary>
        /// <param name="from">Starting hex-location of the sprite.</param>
        /// <param name="to">Hex-destination of the move.</param>
        /// <param name="layer">Layer at which the sprite resides.</param>
        /// <param name="time">How long the move should take, in seconds.</param>
        /// <param name="callback">Action to be called after the move completes.</param>
        public void MoveHex(Point from, Point to, int layer, double time, Action callback = null) {
            MoveSprite(HexAnimator.GetPixelCoordinates(from), HexAnimator.GetPixelCoordinates(to), layer, time, callback);
        }

        /// <summary>
        /// Moves an existing sprite in the animator along a given path.
        /// </summary>
        /// <param name="from">Starting hex-location of the sprite.</param>
        /// <param name="path">Path of hex-locations the sprite will travel through.</param>
        /// <param name="layer">Layer at which the sprite resides.</param>
        /// <param name="time">How long the move should take, in seconds.</param>
        /// <param name="callback">Action to be called after the move completes.
        public void MoveHex(Point from, List<Point> path, int layer, double time, Action callback = null) {
            List<Point> translatedToPixels = new List<Point>();
            foreach (Point pathVector2 in path) {
                translatedToPixels.Add(HexAnimator.GetPixelCoordinates(pathVector2));
            }
            MoveSprite(HexAnimator.GetPixelCoordinates(from), translatedToPixels, layer, time, callback);
        }

        /// <summary>
        /// Returns whether a sprite exists at a given layer.
        /// </summary>
        /// <param name="hex">Hex-location where the sprite should be.</param>
        /// <param name="layer">Layer of the potential sprite.</param>
        /// <returns></returns>
        public bool HexExists(Point hex, int layer) {
            return SpriteExists(HexAnimator.GetPixelCoordinates(hex), layer);
        }

        /// <summary>
        /// Returns a sprite at a given location.
        /// </summary>
        /// <param name="hex">Hex-location of the sprite.</param>
        /// <param name="layer">Layer of the sprite.</param>
        /// <returns></returns>
        public ILocationDrawable HexAt(Point hex, int layer) {
            return SpriteAt(HexAnimator.GetPixelCoordinates(hex), layer);
        }

        /// <summary>
        /// Removes a sprite from the animator.
        /// </summary>
        /// <param name="location">Hex-location of the sprite.</param>
        /// <param name="layer">Layer of the sprite.</param>
        public void RemoveHex(Point hex, int layer) {
            RemoveSprite(HexAnimator.GetPixelCoordinates(hex), layer);
        }

        /// <summary>
        /// Moves a sprite from one layer to another.
        /// </summary>
        /// <param name="spriteLocation">Location of the sprite.</param>
        /// <param name="layerFrom">Layer the sprite resides at.</param>
        /// <param name="layerTo">Layer to move the sprite to.</param>
        public void ChangeHexLayer(Point hex, int layerFrom, int layerTo) {
            ChangeSpriteLayer(HexAnimator.GetPixelCoordinates(hex), layerFrom, layerTo);
        }
    }
}
