﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UTS.Drawing;
using UTS.Utils;

namespace UTS.GUI {
    /// <summary>
    /// A graphic component of the top bar above game, showing player resources and other stats.
    /// </summary>
    public class GUIOverlay : Texture2DILocationDrawable {
        /// <summary>
        /// Creates a default GUI overlay.
        /// </summary>
        public GUIOverlay() : base(Textures.GUIOverlayTexture) {
        }

        public override void DrawTo(SpriteBatch spriteBatch, Vector2 location, out bool requestRemove) {
            base.DrawTo(spriteBatch, location, out requestRemove);

            Player curPlayer = Match.Current.CurrentPlayer;

            int foodCapacity = curPlayer.foodCapacity;
            int food = curPlayer.food;
            int foodIncome = curPlayer.foodIncome - curPlayer.upkeep;

            int peopleCapacity = curPlayer.peopleCapacity;
            int people = curPlayer.people;
            int peopleIncome = curPlayer.peopleIncome;

            TextUtils.DrawCenteredTextWithOutline(spriteBatch, location + new Vector2(1165, 17), "cap. " + foodCapacity, Color.White, Color.Black, Fonts.Arial14);
            TextUtils.DrawCenteredTextWithOutline(spriteBatch, location + new Vector2(1165, 47), food.ToString(), Color.White, Color.Black, Fonts.BigArial);
            string incomeString = foodIncome >= 0 ? "+"+foodIncome : foodIncome.ToString();
            TextUtils.DrawCenteredTextWithOutline(spriteBatch, location + new Vector2(1165, 72), incomeString, Color.White, Color.Black, Fonts.Arial14);

            TextUtils.DrawCenteredTextWithOutline(spriteBatch, location + new Vector2(1275, 17), "cap. " + peopleCapacity, Color.White, Color.Black, Fonts.Arial14);
            TextUtils.DrawCenteredTextWithOutline(spriteBatch, location + new Vector2(1275, 47), people.ToString(), Color.White, Color.Black, Fonts.BigArial);
            incomeString = peopleIncome >= 0 ? "+" + peopleIncome: peopleIncome.ToString();
            TextUtils.DrawCenteredTextWithOutline(spriteBatch, location + new Vector2(1275, 72), incomeString, Color.White, Color.Black, Fonts.Arial14);
        }

        /// <summary>
        /// Shows a floating text next to the food amount.
        /// </summary>
        /// <param name="text">Text to display. Should be short.</param>
        /// <param name="textColor">Color of the displayed text.</param>
        public void ShowTextByFood(string text, Color textColor, Action fadeCallback = null) {
            Match match = Match.Current;
            match.layoutAnimator.AddSprite(
                new FloatingTextAnimation(text, textColor, Direction.Right, 2, fadeCallback),
                new Point(1185, 47),
                (int) Match.LayoutLayer.Gui
            );
        }

        /// <summary>
        /// Shows a floating text next to the people amount.
        /// </summary>
        /// <param name="text">Text to display. Should be short.</param>
        /// <param name="textColor">Color of the displayed text.</param>
        public void ShowTextByPeople(string text, Color textColor, Action fadeCallback = null) {
            Match match = Match.Current;
            match.layoutAnimator.AddSprite(
                new FloatingTextAnimation(text, textColor, Direction.Right, 2, fadeCallback),
                new Point(1295, 47),
                (int) Match.LayoutLayer.Gui
            );
        }
    }
}
