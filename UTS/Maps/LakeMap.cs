﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UTS;

namespace UTS.Maps {
    public class LakeMap : Map {
        // generated
        static MapField[][] mapGrid = new MapField[][] {
	        new MapField[] { 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Mountains, 	 MapField.Mountains, 	 MapField.Mountains, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Water, 	 MapField.Water, 	 MapField.Village, 	 MapField.Grass },
	        new MapField[] { 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Mountains, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Water, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass },
	        new MapField[] { 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Start, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Water, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Forest },
	        new MapField[] { 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Village, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Grass },
	        new MapField[] { 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Water, 	 MapField.Water, 	 MapField.Water, 	 MapField.Grass, 	 MapField.Water, 	 MapField.Water, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Grass },
	        new MapField[] { 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Water, 	 MapField.Water, 	 MapField.Water, 	 MapField.Water, 	 MapField.Water, 	 MapField.Water, 	 MapField.Water, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass },
	        new MapField[] { 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Water, 	 MapField.Grass, 	 MapField.Village, 	 MapField.Grass, 	 MapField.Water, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass },
	        new MapField[] { 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Forest },
	        new MapField[] { 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Water, 	 MapField.Water, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Mountains, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Start, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest },
	        new MapField[] { 	 MapField.Grass, 	 MapField.Village, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Water, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Mountains, 	 MapField.Mountains, 	 MapField.Mountains, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest },
	        new MapField[] { 	 MapField.Grass, 	 MapField.Nothing, 	 MapField.Grass, 	 MapField.Nothing, 	 MapField.Water, 	 MapField.Nothing, 	 MapField.Grass, 	 MapField.Nothing, 	 MapField.Grass, 	 MapField.Nothing, 	 MapField.Mountains, 	 MapField.Nothing, 	 MapField.Forest, 	 MapField.Nothing, 	 MapField.Grass, 	 MapField.Nothing, 	 MapField.Forest, 	 MapField.Nothing, 	 MapField.Forest, 	 MapField.Nothing, 	 MapField.Grass }
        };


        public LakeMap()
            : base(mapGrid, Textures.MapLake) {
        }
    }
}
