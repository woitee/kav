﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using UTS.Drawing;

namespace UTS.Buildings {
    class House : Building {
        public House(PlayerColor color)
            : base(
                new SpriteOfEachColor(Textures.HouseBlue, Textures.HouseRed),
                Textures.ScrollMidHouseTexture,
                color
            ) {

        }

        public override void CreatedAt(Point hex) {
            base.CreatedAt(hex);
            Player myPlayer = Match.Current.GetPlayer(color);

            myPlayer.peopleIncome += 1;
            myPlayer.peopleCapacity += 1;
        }

        public override void Die() {
            base.Die();
            Player myPlayer = Match.Current.GetPlayer(color);

            myPlayer.peopleIncome -= 1;
            myPlayer.peopleCapacity -= 1;
        }
    }
}
