﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace UTS {
    /// <summary>
    /// A static class for all drawn colors used in game, mainly for texts.
    /// </summary>
    public static class GameColors {
        public static Color GreatColor {
            get {
                return new Color(214, 237, 85);
            }
        }
        public static Color GoodColor {
            get {
                return new Color(195, 250, 185);
            }
        }
        public static Color NeutralColor {
            get {
                return Color.White;
            }
        }
        public static Color BelowAverageColor {
            get {
                return new Color(250, 156, 62);
            }
        }
        public static Color BadColor {
            get {
                return new Color(250, 128, 52);
            }
        }
        public static Color ReallyBadColor {
            get {
                return new Color(227, 125, 125);
            }
        }
    }
}
