﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UTS.Cards {
    class KnightCard : UnitCard {
        public KnightCard()
            : base(
                Textures.CardKnight,
                new Units.Knight(PlayerColor.Blue)
                ) { }

    }
}
