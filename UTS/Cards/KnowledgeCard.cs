﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using UTS.Hexes;

namespace UTS.Cards {
    class KnowledgeCard : EventCard {
        public KnowledgeCard() : base(Textures.CardKnowledge) { }

        public override bool TryUseOnHex(Point location) {
            Match match = Match.Current;
            Player curPlayer = match.CurrentPlayer;

            if (TryPayCost(location)) {
                match.DrawCard();
                match.DrawCard();
                match.DrawCard();
                return true;
            }

            return false;
        }
    }
}
