﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UTS.Cards;
using UTS.Utils;

namespace UTS.Drawing {
    /// <summary>
    /// An animator specifically designed to draw and manipulate cards.
    /// </summary>
    public class CardAnimator : IDrawer {
        /// <summary>
        /// Contains cards in a players hand.
        /// </summary>
        protected Animator handAnimator;
        /// <summary>
        /// Contains everything except the players hand, to draw various effects.
        /// </summary>
        protected Animator fullAnimator;
        protected Rectangle positionOnScreen;
        protected List<Card> cards = new List<Card>();
        /// <summary>
        /// Gets/sets whether this animator draws anything.
        /// </summary>
        public bool Enabled = true;

        /// <summary>
        /// The number of cards this animator currently shows.
        /// </summary>
        public int CardCount { get { return cards.Count; } }

        /// <summary>
        /// A default constructor of the card animator.
        /// </summary>
        /// <param name="totalScreen">The total drawable screen dimensions.</param>
        /// <param name="positionOnScreen">The position of the player's hand.</param>
        public CardAnimator(Rectangle totalScreen, Rectangle positionOnScreen) {
            handAnimator = new Animator(positionOnScreen, positionOnScreen.Size());
            fullAnimator = new Animator(totalScreen);
            this.positionOnScreen = positionOnScreen;
        }

        List<Card> cardsTravelling = new List<Card>();
        /// <summary>
        /// Draws a card to hand, with animation.
        /// </summary>
        /// <param name="card">The card to draw.</param>
        public void DrawCard(Card card) {
            MoveAllTravellingCardsToHand();
            fullAnimator.AddSprite(card, 0, 0);
            Point targetInHand = GetPixelCoordinates(cards.Count).Add(positionOnScreen.Origin());
            cardsTravelling.Add(card);

            fullAnimator.MoveSprite(new Point(0, 0), targetInHand, 0.75,
                // callback
                () => {
                    Add(card);
                    cardsTravelling.Remove(card);
                    fullAnimator.RemoveSprite(targetInHand);
                }
            );
        }

        /// <summary>
        /// Adds a card to hand, without animation.
        /// </summary>
        /// <param name="card">The card to add.</param>
        public void Add(Card card) {
            handAnimator.AddSprite(card, GetPixelCoordinates(cards.Count));
            cards.Add(card);
        }

        /// <summary>
        /// Adds an overlay to be displayed over a card.
        /// </summary>
        /// <param name="index">Index of the card</param>
        /// <param name="overlay">The overlay to display</param>
        public void AddOverlay(int index, ILocationDrawable overlay) {
            CardAt(index).AddOverlay(Vector2.Zero, overlay);
        }

        public void RemoveOverlays(int index) {
            CardAt(index).RemoveAllOverlays();
        }

        /// <summary>
        /// Returns the card at a specified index.
        /// </summary>
        /// <param name="index">Zero-based index of the card.</param>
        /// <returns></returns>
        public Card CardAt(int index) {
            return cards[index];
        }

        protected void MoveAllTravellingCardsToHand() {
            foreach (Card card in cardsTravelling) {
                Add(card);
            }
            cardsTravelling = new List<Card>();
            fullAnimator.RemoveAllSprites();
        }

        /// <summary>
        /// Gets actual pixel coordinates of a card index.
        /// </summary>
        /// <param name="index">Zero-based index of the card.</param>
        /// <returns></returns>
        protected Point GetPixelCoordinates(int index) {
            return new Point(Constants.CardWidth * index, 0);
        }

        /// <summary>
        /// Removes a card from hand at the specified index.
        /// </summary>
        /// <param name="index">Index of the card removed.</param>
        public void RemoveCard(int index) {
            //Move cards to the left
            for (int i = index; i < cards.Count; ++i) {
                handAnimator.RemoveSprite(GetPixelCoordinates(i));
            }
            cards.RemoveAt(index);
            for (int i = index; i < cards.Count; ++i) {
                handAnimator.AddSprite(cards[i], GetPixelCoordinates(i));
            }
        }

        /// <summary>
        /// Removes all cards.
        /// </summary>
        public void RemoveAllCards() {
            MoveAllTravellingCardsToHand();
            for (int i = cards.Count - 1; i >= 0; --i) {
                RemoveCard(i);
            }
        }


        public void Draw(SpriteBatch spriteBatch) {
            if (!Enabled)
                return;
            handAnimator.Draw(spriteBatch);
            fullAnimator.Draw(spriteBatch);
        }
        public void Update(GameTime gameTime) {
            handAnimator.Update(gameTime);
            fullAnimator.Update(gameTime);
        }

    }
}
