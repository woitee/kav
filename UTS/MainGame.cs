using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using UTS.Controllers;
using UTS.Drawing;

namespace UTS {
    /// <summary>
    /// Main game type, loads all content and then distributes Update and Draw calls to current shown screen objects.
    /// </summary>
    public class MainGame : Microsoft.Xna.Framework.Game {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        /// <summary>
        /// Used for all RNG in the game, which is used deterministically.
        /// Synchronizes network play, so neither player can make up "random" values.
        /// </summary>
        public static Random random = new Random();
        /// <summary>
        /// A list of objects that should be Drawn and/or Updated at a time.
        /// </summary>
        public List<IDrawer> drawers;
        /// <summary>
        /// A queue of Actions that should be processed after the Update loop finishes.
        /// Mostly used when the collection of Updaters should be modified,
        /// which cannot be done from inside the Update loop.
        /// </summary>
        public Queue<Action> updateCallbacks = new Queue<Action>();
        /// <summary>
        /// A list of Actions to be processed when the Game ends.
        /// </summary>
        public Queue<Action> gameExitingCallbacks = new Queue<Action>();

        public MainGame() {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferHeight = 768;
            graphics.PreferredBackBufferWidth = 1366;

            Content.RootDirectory = "Content";
        }

        protected override void Initialize() {
            base.Initialize();

            graphics.IsFullScreen = true;
            this.IsMouseVisible = true;
        }

        protected override void LoadContent() {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            Textures.LoadAll(Content);
            Fonts.LoadAll(Content);
            AfterLoading();
        }

        protected void AfterLoading() {
            GoToMenu();
        }

        protected override void UnloadContent() {
        }

        protected override void Update(GameTime gameTime) {
            // Allows the game to exit
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            foreach (var drawer in drawers) {
                drawer.Update(gameTime);
            }
            while (updateCallbacks.Count > 0) {
                updateCallbacks.Dequeue()();
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.Clear(Color.BlanchedAlmond);

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
            foreach (var drawer in drawers) {
                drawer.Draw(spriteBatch);
            }
            spriteBatch.End();

            base.Draw(gameTime);
        }

        protected override void OnExiting(object sender, EventArgs args) {
            base.OnExiting(sender, args);

            while (gameExitingCallbacks.Count > 0) {
                gameExitingCallbacks.Dequeue()();
            }

            Logger.Close();
        }

        /// <summary>
        /// Goes to menu.
        /// </summary>
        /// <param name="message">Message that will be displayed in menu after the transition.</param>
        public void GoToMenu(string message = "") {
            Logger.Info("Went to menu");

            MainScreen mainScreen = new MainScreen(this, message);
            drawers = new List<IDrawer>();
            drawers.Add(mainScreen);
        }
    }
}
