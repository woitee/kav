﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace UTS {   
    /// <summary>
    /// A class that preloads all the game's fonts, and then has them ready for any class to use.
    /// </summary>
    public static class Fonts {
        public static SpriteFont Arial14;
        public static SpriteFont IPAddress;
        public static SpriteFont BigArial;

        /// <summary>
        /// Load all fonts. Must be called before any fonts are used.
        /// </summary>
        /// <param name="content">Content manager to load all fonts into.</param>
        public static void LoadAll(ContentManager content) {
            Arial14 = content.Load<SpriteFont>("Arial14");
            BigArial = content.Load<SpriteFont>("BigArial");
            IPAddress = content.Load<SpriteFont>("IPAddressFont");
        }
    }
}
