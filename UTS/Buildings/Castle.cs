﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using UTS.Collections;
using UTS.Hexes;
using UTS.Units;
using UTS.Drawing;

namespace UTS.Buildings {
    class Castle : Building {
        public Castle(PlayerColor color)
            : base(
                new SpriteOfEachColor(Textures.CastleBlue, Textures.CastleRed),
                Textures.ScrollMidCastleTexture,
                color
            ) {

            CanSpawnUnits = true;
        }

        public override void CreatedAt(Point hex) {
            base.CreatedAt(hex);
            Player myPlayer = Match.Current.GetPlayer(color);

            myPlayer.foodIncome += 2;
            myPlayer.peopleIncome += 2;
            myPlayer.foodCapacity += 10;
            myPlayer.peopleCapacity += 10;
        }

        public override void StartTurn(EventQueue eventQueue) {
            base.StartTurn(eventQueue);
            if (ItsMyTurn) {
                CommonAbilities.AttackRandomEnemyUnit(this, eventQueue);
                CommonAbilities.AttackRandomEnemyUnit(this, eventQueue);
            }
        }

        public override void Die() {
            base.Die();
            Player myPlayer = Match.Current.GetPlayer(color);
            myPlayer.foodIncome -= 2;
            myPlayer.peopleIncome -= 2;
            myPlayer.foodCapacity -= 10;
            myPlayer.peopleCapacity -= 10;
        }
    }
}
