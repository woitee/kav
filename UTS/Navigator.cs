﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

using UTS.Maps;
using UTS.Units;
using UTS.Buildings;
using UTS.Drawing;
using UTS.Collections;
using UTS.Utils;

namespace UTS {
    /// <summary>
    /// A class that solves all navigation and travellability of a map.
    /// Also contains some practical helper methods of a map.
    /// </summary>
    public class Navigator {
        protected Match match;
        protected Map map;
        protected Unit[][] units;
        /// <summary>
        /// Contains predecessor points needed for backtracking after a search is done.
        /// </summary>
        protected Point[][] predecessors;
        /// <summary>
        /// Contains the last point, from which navigation was called.
        /// </summary>
        protected Point lastSource;

        /// <summary>
        /// Creates default Navigator.
        /// </summary>
        public Navigator() {
            this.match = Match.Current;
            this.map = match.Map;
            this.units = match.units.array;
            predecessors = MyUtils.CreateArray<Point>(map.Width, map.Height);
        }

        /// <summary>
        /// Returns whether a hex location is travellable by units of a specific Color.
        /// </summary>
        /// <param name="hex">The hex to be travelled.</param>
        /// <param name="color">The color of the unit that wants to travel.</param>
        /// <returns>True if the hex can be travelled, false otherwise.</returns>
        protected bool IsHexTravellable(Point hex, PlayerColor color) {
            return map.FieldAt(hex).IsPassable() && 
                (Match.Current.units[hex] == null || Match.Current.units[hex].color == color);
        }
        /// <summary>
        /// Returns whether Units of the specified color can finish their movement at a destination.
        /// </summary>
        /// <param name="hex">The potential destination of the move.</param>
        /// <param name="color">The color of the unit.</param>
        /// <returns>True if the units is allowed to move to the destination, false otherwise.</returns>
        protected bool CanFinishMoveAt(Point hex, PlayerColor color) {
            return IsHexTravellable(hex, color) && units.At(hex) == null;
        }
        /// <summary>
        /// The cost of travelling from a hex to another.
        /// </summary>
        /// <param name="hexLoc">The destination of the travel.</param>
        /// <returns>How much of their movement range units spend by travelling to the hex.</returns>
        protected double TravelCost(Point hexLoc) {
            Match match = Match.Current;
            if (match.buildings[hexLoc] != null)
                return 1;
            else
                return map.FieldAt(hexLoc).TravelCost();
        }

        /// <summary>
        /// Returns all hexes in movement range from a location.
        /// </summary>
        /// <param name="location">The start of the move.</param>
        /// <param name="range">The possible range of the move.</param>
        /// <returns>List of all hexes reachable by the movement.</returns>
        public List<Point> AvailableMoves(Point location, int range) {
            lastSource = location;
            PlayerColor color = Match.Current.CurrentPlayer.color;

            Func<Point, Point, double> neighborTravelCost = (from, to) => {
                if (!IsHexTravellable(to, color))
                    return double.PositiveInfinity;
                return TravelCost(to);
            };

            return Dijkstra(location, range, neighborTravelCost)
                   .Where((point) => CanFinishMoveAt(point, color))
                   .ToList();
        }
        /// <summary>
        /// Returns all hexes that are closer than the given range (air-distance).
        /// Useful i.e. for determining range of ranged attacks.
        /// </summary>
        /// <param name="location">The source location.</param>
        /// <param name="range">Range.</param>
        /// <returns></returns>
        public List<Point> HexesInDistance(Point location, int range) {
            return Dijkstra(location, range, (from, to) => { return 1; });
        }
        /// <summary>
        /// Returns a list of building spots for a given player.
        /// Players can generally only build buildings adjacent to their city, but the Watchtower for example
        /// can be build upto 3 fields away from the city.
        /// </summary>
        /// <param name="player">The player that wants to build.</param>
        /// <param name="rangeFromCity">Permitted range from the city.</param>
        /// <returns>List of all building spots available.</returns>
        public List<Point> AvailableBuildingSpots(Player player, int rangeFromCity = 1) {           
            // Find player's castle
            Building castle = null;
            foreach (var building in match.buildings.Values) {
                if (building.color == player.color && building is Castle) {
                    castle = building;
                    break;
                }
            }
            // Compute player's city
            List<Point> city = Dijkstra(castle.Location, 1000,
              (from, to) => {
                  return match.buildings[to] == null ? double.PositiveInfinity : 1;
              }  
            );
            city.Add(castle.Location);
            // Calculate buildings in range
            List<Point> buildSpots = Dijkstra(city, rangeFromCity,
              (from, to) => {
                  return 1;
              }
            )
            // Filter unbuildable locations
            .Where((location) => {
                return match.Map.FieldAt(location).IsPassable() &&
                       match.buildings[location] == null;
            }).ToList();

            return buildSpots;
        }
        
        /// <summary>
        /// Core navigation - Dijsktra's algorithm. It returns all hexes in given movement range.
        /// Movement costs of different hexes are supplied as a parameter.
        /// </summary>
        /// <param name="startLocations">Locations we start the search from.</param>
        /// <param name="range">Permitted range from the starting locations.</param>
        /// <param name="neighborTravelCost">Function that returns travel costs of hex transitions.</param>
        /// <returns>All reachable hexes from the given locations.</returns>
        protected List<Point> Dijkstra(IEnumerable<Point> startLocations, int range, Func<Point, Point, double> neighborTravelCost) {
            // Dijkstra's algorithm

            bool[][] visited = MyUtils.CreateArray<bool>(map.Width, map.Height);
            List<Point> ret = new List<Point>();

            var heap = new SimpleHeap<double, Point>();
            foreach (Point location in startLocations) {
                heap.Add(0, location);
                ret.Add(location);
                visited.Set(location, true);
            }

            while (!heap.IsEmpty) {
                var heapMin = heap.ExtractMin();
                Point current = heapMin.Value;
                double curDist = heapMin.Key;

                var neighbors = GetNeighbors(current);

                foreach (Point neighbor in neighbors) {
                    if (!visited.At(neighbor)) {
                        double distance = curDist + neighborTravelCost(current, neighbor);
                        double epsilon = 0.1;
                        if (distance > range + epsilon) {
                            continue;
                        }
                        heap.Add(distance, neighbor);
                        visited[neighbor.X][neighbor.Y] = true;
                        predecessors[neighbor.X][neighbor.Y] = current;
                        ret.Add(neighbor);
                    }
                }
            }

            return ret;
        }
        /// <summary>
        /// Core navigation - Dijsktra's algorithm. It returns all hexes in given movement range.
        /// Movement costs of different hexes are supplied as a parameter.
        /// </summary>
        /// <param name="startLocations">Location to start the search from.</param>
        /// <param name="range">Permitted range from the starting locations.</param>
        /// <param name="neighborTravelCost">Function that returns travel costs of hex transitions.</param>
        /// <returns>All reachable hexes from the given location.</returns>
        protected List<Point> Dijkstra(Point location, int range, Func<Point, Point, double> neighborTravelCost) {
            return Dijkstra(new Point[] { location }, range, neighborTravelCost);
        }

        #region Neighbor hexes calculating
        protected static Point[] evenNeighborOffsets = new Point[] {
            new Point(0, -1),
            new Point(0, +1),
            new Point(-1, -1),
            new Point(-1, 0),
            new Point(+1, -1),
            new Point(+1, 0)
        };
        protected static Point[] oddNeighborOffsets = new Point[] {
            new Point(0, -1),
            new Point(0, +1),
            new Point(-1, +1),
            new Point(-1, 0),
            new Point(+1, +1),
            new Point(+1, 0)
        };
        /// <summary>
        /// Returns all neighbors of a given hex, limited to the Map size.
        /// </summary>
        /// <param name="from">Hex, whose neighbors we want to calculate.</param>
        /// <returns>List of all neighbors of the hex.</returns>
        public List<Point> GetNeighbors(Point from) {
            List<Point> ret = new List<Point>();
            Point[] offsets = from.X % 2 == 0 ? evenNeighborOffsets : oddNeighborOffsets;

            foreach (Point offset in offsets) {
                Point point = from.Add(offset);

                if (point.X.IsLimitedToRange(0, map.Width - 1) &&
                    point.Y.IsLimitedToRange(0, map.Height - 1)) {
                    ret.Add(point);
                }
            }

            return ret;
        }

        /// <summary>
        /// Returns all travellable neighbors of a given hex, limited to the Map size.
        /// </summary>
        /// <param name="from">Hex, whose neighbors we want to calculate.</param>
        /// <returns>List of all neighbors of the hex.</returns>
        protected List<Point> GetTravellableNeighbors(Point from, PlayerColor color) {
            List<Point> neighbors = GetNeighbors(from);
            List<Point> accessibles = new List<Point>();

            foreach (Point neighbor in neighbors) {
                if (IsHexTravellable(neighbor, color)) {
                    accessibles.Add(neighbor);
                }
            }
            return accessibles;
        }
        #endregion

        /// <summary>
        /// Returns one of the shortest path a unit can take to a location.
        /// </summary>
        /// <param name="unit">Unit to move.</param>
        /// <param name="target">Target of the move.</param>
        /// <returns>A list of hexes on the path, one by one.</returns>
        public List<Point> GetPathTo(Unit unit, Point target) {
            AvailableMoves(unit.Location, unit.MovementRange);
            var ret = new List<Point>();

            while (target != lastSource) {
                ret.Add(target);
                target = predecessors.At(target);
            }

            ret.Reverse();
            return ret;
        }

        /// <summary>
        /// Lists all hexes in the map of a given Map Type.
        /// </summary>
        /// <param name="type">Type.</param>
        /// <returns></returns>
        public List<Point> GetHexesOfType(MapField type) {
            var ret = new List<Point>();

            for (int x = 0; x < map.Width; ++x) {
                for (int y = 0; y < map.Height; ++y) {
                    if (map.FieldAt(x, y) == type) {
                        ret.Add(new Point(x, y));
                    }
                }
            }

            return ret;
        }
    }
}
