﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UTS.Buildings;

namespace UTS.Cards {
    class ChapelCard : BuildingCard {
        public ChapelCard()
            : base(
                Textures.CardChapel,
                new Chapel(PlayerColor.Blue)
            ) { }
    }
}
