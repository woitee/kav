﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UTS.Drawing {
    public interface IDrawer {
        /// <summary>
        /// Main update loop, should be called repeatedly to adjust locations of moving sprites and scrolling.
        /// </summary>
        /// <param name="gameTime">Current GameTime.</param>
        void Update(GameTime gameTime);
        /// <summary>
        /// Main draw loop, draws all the sprites.
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch to draw to.</param>
        void Draw(SpriteBatch spriteBatch);
    }
}
