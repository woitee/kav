﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using UTS.Drawing;
using UTS.Events;
using UTS.Units;

namespace UTS.Buildings {
    class Smithy : Building {
        protected UnitCreatedEventHandler unitCreatedEventHandler;

        public Smithy(PlayerColor color)
            : base(
                new SpriteOfEachColor(Textures.SmithyBlue, Textures.SmithyRed),
                Textures.ScrollMidSmithyTexture,
                color
            ) {

                unitCreatedEventHandler = (args) => {
                    Unit unit = args.unit;
                    if (unit.color == this.color) {
                        unit.AttackDamage += 1;
                    }
                };
        }

        public override void CreatedAt(Point hex) {
            base.CreatedAt(hex);
            Match match = Match.Current;

            // Boost All Units
            foreach (Unit unit in match.units.Values) {
                if (unit.color == this.color)
                    unit.AttackDamage += 1;
            }
            // Boost Units that will be newly created
            match.UnitCreated += unitCreatedEventHandler;
        }

        public override void Die() {
            base.Die();
            Match match = Match.Current;

            // Unboost All Units
            foreach (Unit unit in match.units.Values) {
                if (unit.color == this.color)
                    unit.AttackDamage -= 1;
            }
            // Stop boosting newly created
            match.UnitCreated -= unitCreatedEventHandler;
        }
    }
}
