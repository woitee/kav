﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using UTS.Buildings;
using UTS.Drawing;
using UTS.Maps;
using UTS.Units;
using UTS.Hexes;

namespace UTS.Cards {
    /// <summary>
    /// A common ancestor of all Building cards.
    /// These cards can usually be used on any hex touching the border of a player's city.
    /// </summary>
    class BuildingCard : HexCard{
        protected Building building;

        public BuildingCard(ILocationDrawable sprite, Building building)
            : base(CardType.Building, sprite, building) {
            this.building = building;
        }

        protected virtual List<Point> BuildableLocations() {
            Match match = Match.Current;
            return match.Navigator.AvailableBuildingSpots(match.CurrentPlayer);
        }

        protected static HashSet<Point> showingOverlays;
        public override void Selected() {
            base.Selected();

            Match match = Match.Current;
            // Get valid building locations
            showingOverlays = new HashSet<Point>();
            foreach (var location in BuildableLocations()) {
                showingOverlays.Add(location);
                Unit unit = match.units[location];
                if (unit != null) {
                    unit.overrideTargetScale = 0.5;
                }
            }

            // Show overlays
            foreach (var point in showingOverlays) {
                match.hexAnimator.AddHex(Textures.HexOverlay, point, (int)Match.Layer.Buildings);
            }
        }

        public override void Unselected() {
            base.Unselected();
            Match match = Match.Current;

            foreach (var point in showingOverlays) {
                match.hexAnimator.RemoveHex(point, (int)Match.Layer.Buildings);
                Unit unit = match.units[point];
                if (unit != null) {
                    unit.overrideTargetScale = null;
                }
            }
        }

        public override bool TryUseOnHex(Point hexLoc) {
            Match match = Match.Current;
            return TryUseOnHex(hexLoc,
                (hex) => { return BuildableLocations().Contains(hexLoc); },
                (hex) => { match.AddBuilding(new ConstructionSite(match.CurrentPlayer.color, building), hex); }
            );
        }
    }
}
