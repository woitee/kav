﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UTS.Cards {
    class PeasantCard : UnitCard {
        public PeasantCard()
            : base(
                Textures.CardPeasant,
                new Units.Peasant(PlayerColor.Blue)
                ) { }
    }
}
