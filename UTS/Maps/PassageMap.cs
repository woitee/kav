﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UTS;

namespace UTS.Maps {
    public class PassageMap : Map {
        // generated
        static MapField[][] mapGrid = new MapField[][] {
	        new MapField[] { 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Start, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Forest },
	        new MapField[] { 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Start, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Mountains, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Start, 	 MapField.Forest, 	 MapField.Forest },
	        new MapField[] { 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Mountains, 	 MapField.Mountains, 	 MapField.Forest, 	 MapField.Village, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass },
	        new MapField[] { 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Mountains, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass },
	        new MapField[] { 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Village, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Village, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Village, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass },
	        new MapField[] { 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Village, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Start, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest },
	        new MapField[] { 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Village, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Village, 	 MapField.Forest, 	 MapField.Forest },
	        new MapField[] { 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Start, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Mountains, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Start, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Mountains, 	 MapField.Mountains, 	 MapField.Mountains },
	        new MapField[] { 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Mountains, 	 MapField.Mountains, 	 MapField.Village, 	 MapField.Forest, 	 MapField.Village, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Mountains, 	 MapField.Mountains, 	 MapField.Mountains, 	 MapField.Forest, 	 MapField.Start, 	 MapField.Forest },
	        new MapField[] { 	 MapField.Mountains, 	 MapField.Mountains, 	 MapField.Mountains, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Start, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest },
	        new MapField[] { 	 MapField.Mountains, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Village, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Village, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Forest },
	        new MapField[] { 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Village, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass },
	        new MapField[] { 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Mountains, 	 MapField.Mountains, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass },
	        new MapField[] { 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Start, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Start, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Mountains, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass },
	        new MapField[] { 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Grass, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Forest, 	 MapField.Grass, 	 MapField.Start, 	 MapField.Grass, 	 MapField.Grass },
	        new MapField[] { 	 MapField.Grass, 	 MapField.Nothing, 	 MapField.Grass, 	 MapField.Nothing, 	 MapField.Forest, 	 MapField.Nothing, 	 MapField.Forest, 	 MapField.Nothing, 	 MapField.Forest, 	 MapField.Nothing, 	 MapField.Grass, 	 MapField.Nothing, 	 MapField.Forest, 	 MapField.Nothing, 	 MapField.Grass, 	 MapField.Nothing, 	 MapField.Grass, 	 MapField.Nothing }
        };

        public PassageMap()
            : base(mapGrid, Textures.MapPassage) {
        }
    }
}
