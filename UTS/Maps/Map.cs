﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UTS.Drawing;

namespace UTS.Maps {
    /// <summary>
    /// Enumeration of all the possible map field types.
    /// </summary>
    public enum MapField {
        Nothing, Grass, Forest, Mountains, Water, Village, Start, Swamp
    }

    public static class MapFieldMethods {
        public static Boolean IsPassable(this MapField field) {
            return  field != MapField.Mountains &&
                    field != MapField.Water &&
                    field != MapField.Nothing;
        }

        public static double TravelCost(this MapField field) {
            if (field == MapField.Forest) {
                return 1.5;
            } else if (!field.IsPassable()) {
                return double.PositiveInfinity;
            } else {
                return 1;
            }
        }
    }
    
    /// <summary>
    /// Class representing a map in the game. Holds both visual and functional information.
    /// </summary>
    public abstract class Map : IEnumerable<MapField>{
        protected MapField[][] grid;
        public ILocationDrawable Sprite { get; protected set; }
        protected Map(MapField[][] grid, ILocationDrawable image) {
            this.grid = grid;
            this.Sprite = image;
        }
        public MapField FieldAt(int x, int y) {
            return grid[y][x];
        }
        public MapField FieldAt(Point point) {
            return FieldAt((int) point.X, (int) point.Y);
        }
        public Point Size {
            get {
                return new Point(grid[0].Length, grid.Length);
            }
        }
        public int Width {
            get {
                return grid[0].Length;
            }
        }
        public int Height {
            get {
                return grid.Length;
            }
        }
        /// <summary>
        /// Enumerates fields in the map.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<MapField> GetEnumerator() {
            foreach (MapField[] row in grid) {
                foreach (MapField elem in row) {
                    yield return elem;
                }
            }
        }

        /// <summary>
        /// Enumerates fields in the map.
        /// </summary>
        /// <returns></returns>
        IEnumerator System.Collections.IEnumerable.GetEnumerator() {
            return this.GetEnumerator();
        }
    }
}
