﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UTS.Buildings;

namespace UTS.Cards {
    class TavernCard : BuildingCard {
        public TavernCard()
            : base(
                Textures.CardTavern,
                new Tavern(PlayerColor.Blue)
            ) { }
    }
}
