﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using UTS.Hexes;
using UTS.Events;

namespace UTS.Cards {
    class PlagueCard : EventCard {
        protected int hidingPeopleIncome;
        protected Player affectedPlayer;
        protected PeopleIncomeChangedEventHandler peopleIncomeChanged;
        protected TurnEndEventHandler turnEnd;
        protected int lastingTurn;
        protected int willLastTurns = MainGame.random.Next(4) + 1;

        public PlagueCard() : base(Textures.CardPlague) {
            peopleIncomeChanged = (args) => {
                if (args.player == affectedPlayer) {
                    MyPeopleIncomeChange(args.difference);
                }
            };

            turnEnd = (args) => {
                if (args.player == affectedPlayer) {
                    MyTurnEnd();
                }
            };
        }

        public override bool TryUseOnHex(Point location) {
            Match match = Match.Current;
            Player curPlayer = match.CurrentPlayer;
            affectedPlayer = match.Players[(match.CurrentPlayerIndex + 1) % 2];

            if (TryPayCost(location)) {
                // Start plague
                hidingPeopleIncome = 2 + affectedPlayer.peopleIncome;
                affectedPlayer.peopleIncome = -2;
                match.TurnEnd += turnEnd;
                match.PeopleIncomeChanged += peopleIncomeChanged;
                lastingTurn = 0;
                return true;
            }

            return false;
        }

        bool ignoringIncomeChange = false;
        // Affected player only
        protected void MyPeopleIncomeChange(int difference) {
            if (ignoringIncomeChange)
                return;

            hidingPeopleIncome += difference;
            ignoringIncomeChange = true;
            affectedPlayer.peopleIncome -= difference;
            ignoringIncomeChange = false;
        }

        protected void MyTurnEnd() {
            Match match = Match.Current;

            ++lastingTurn;
            if (lastingTurn == willLastTurns) {
                // End plague
                match.TurnEnd -= turnEnd;
                match.PeopleIncomeChanged -= peopleIncomeChanged;
                affectedPlayer.peopleIncome += hidingPeopleIncome;
            }
        }
    }
}
