﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using UTS.Hexes;
using UTS.Units;

namespace UTS.Cards {
    class WeaponsCard : EventCard {
        public WeaponsCard() : base(Textures.CardWeapons) { }

        public override void Selected() {
            base.Selected();
            Match match = Match.Current;
            Player curPlayer = match.CurrentPlayer;

            foreach (Unit unit in match.units.Values) {
                unit.ShowingOverlay = true;
            }
        }

        public override void Unselected() {
            base.Unselected();
            base.Selected();
            Match match = Match.Current;
            Player curPlayer = match.CurrentPlayer;

            foreach (Unit unit in match.units.Values) {
                unit.ShowingOverlay = false;
            }
        }

        public override bool TryUseOnHex(Point location) {
            if (!base.TryUseOnHex(location))
                return false;

            Match match = Match.Current;
            Player curPlayer = match.CurrentPlayer;

            Unit unit = match.units[location];
            if (unit != null &&
                TryPayCost(location) &&
                !unit.Buffs.Contains(Buff.Armored) &&
                unit.color == match.CurrentPlayer.color)
            {
                unit.AttackDamage += 2;
                unit.Buffs.Add(Buff.Weapons);
                return true;
            }

            return false;
        }
    }
}
