﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UTS.Drawing;

namespace UTS.Cards {
    public enum CardType {
        Unit, Building, Event
    }

    public class NonExistentObjectException : Exception {
    }

    /// <summary>
    /// A common ancestor for all cards.
    /// Contains all info about them.
    /// </summary>
    public abstract class Card : ExtensibleILocationDrawable {
        public CardType type { get; protected set; }
        public ILocationDrawable sprite { get; protected set; }

        public Card(CardType type, ILocationDrawable Sprite) {
            this.type = type;
            this.sprite = Sprite;
        }

        protected bool IsSelected = false;
        public virtual void Selected() { IsSelected = true; }
        public virtual void Unselected() { IsSelected = false; }

        /// <summary>
        /// Tries to use this card on a hex. Returns true if the use was successful.
        /// </summary>
        /// <param name="hexLoc">Location for the card to be used to.</param>
        /// <returns>True if the card could've been and was used on a hex. False otherwise.</returns>
        public virtual bool TryUseOnHex(Point hexLoc) { return false; }
        public override int Height { get { return Textures.CardPikemanTexture.Height; } }
        public override int Width { get { return Textures.CardPikemanTexture.Width; } }
        public override void DrawTo(SpriteBatch spriteBatch, Vector2 location, out bool requestRemove) {
            requestRemove = false;
            bool unused;
            sprite.DrawTo(spriteBatch, location, out unused);
            // overlays
            base.DrawTo(spriteBatch, location, out unused);
        }
    }
}
