﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using UTS.Buildings;
using UTS.Hexes;

namespace UTS.Cards {
    class AssaultCard : EventCard {
        public AssaultCard() : base(Textures.CardAssault) { }

        public override bool TryUseOnHex(Point location) {
            Match match = Match.Current;
            Player curPlayer = match.CurrentPlayer;
            Player otherPlayer = match.Players[(match.CurrentPlayerIndex + 1) % match.Players.Length];

            if (TryPayCost(location)) {
                var foodToSteal = otherPlayer.food;

                foreach (var building in match.buildings.Values) {
                    if (building.color != curPlayer.color && building is Warehouse) {
                        foodToSteal -= 4;
                    }
                }

                foodToSteal = foodToSteal < 0 ? 0 : foodToSteal;
                curPlayer.GainFood(foodToSteal);
                otherPlayer.GainFood(-foodToSteal);
                return true;
            }

            return false;
        }
    }
}
