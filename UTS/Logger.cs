﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace UTS {
    enum LogLevel {
        Fine,
        Info,
        Warning,
        Error
    }

    static class Logger {
        static TextWriter writer; 

        static Logger() {
#if DEBUG
            Directory.CreateDirectory("log");
            writer = new StreamWriter("log/" + DateTimeFileFormat + ".txt");
#endif
        }

        private static string DateTimeFileFormat {
            get {
                return DateTime.Now.ToString("yy-MM-dd_HH-mm-ss");
            }
        }

        private static string DateTimeLogFormat {
            get {
                return DateTime.Now.ToString("HH:mm:ss");
            }
        }

        public static void Log(LogLevel level, string line) {
#if DEBUG
            writer.WriteLine(String.Format("{0} [{1}]: {2}",
                             DateTimeLogFormat, level, line));
            writer.Flush();
#endif
        }

        public static void Fine(string line) {
            Log(LogLevel.Fine, line);
        }
        public static void Info(string line) {
            Log(LogLevel.Info, line);
        }
        public static void Warning(string line) {
            Log(LogLevel.Warning, line);
        }
        public static void Error(string line) {
            Log(LogLevel.Error, line);
        }

        public static void Fine(string format, params object[] args) {
            Fine(String.Format(format, args));
        }
        public static void Info(string format, params object[] args) {
            Info(String.Format(format, args));
        }
        public static void Warning(string format, params object[] args) {
            Warning(String.Format(format, args));
        }
        public static void Error(string format, params object[] args) {
            Error(String.Format(format, args));
        }

        public static void Close() {
#if DEBUG
            writer.Close();
#endif
        }
    }
}
