﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;

using Microsoft.Xna.Framework;

using UTS.Controllers.Messages;
using UTS.Utils;

namespace UTS.Controllers {
    /// <summary>
    /// A controller that controls the player based on incoming TCP messages from the network.
    /// </summary>
    class IncomingConnectionController : Controller {
        TcpClient tcpClient;
        StreamReader reader;

        public IncomingConnectionController(TcpClient tcpClient) {
            this.tcpClient = tcpClient;
            reader = new StreamReader(tcpClient.GetStream(), Encoding.ASCII, false, 128);

            //expect and handle SetRandomSeed message
            string s = null;
            while (s == null)
                s = GetMessage();
            HandleMessage(MessageStatic.Parse(s));
        }

        public override void Update(GameTime gameTime) {
            string message = GetMessage();
            while (message != null) {
                Console.WriteLine("Got message: {0}", message);
                HandleMessage(MessageStatic.Parse(message));
                message = GetMessage();
            }
        }

        StringBuilder stringBuilder = new StringBuilder();
        protected string GetMessage() {
            try {
                int availableChars = tcpClient.Available;
                if (availableChars > 0) {
                    return reader.ReadLine();
                } else if (!tcpClient.IsConnected()) {
                    throw new IOException("Connection has been lost");
                }
            } catch (Exception) {
                Match.Current.updateCallbacks.Enqueue(() => {
                    GoToMenu("Connection error.");
                });
            }
            return null;
        }

        public void HandleMessage(Message message) {
            if (message.GetType() == typeof(SetRandomSeedMessage)) {
                SetRandomSeedMessage msg = (SetRandomSeedMessage)message;
                MainGame.random = new Random(msg.seed); 
            } else if (message.GetType() == typeof(TryUseSelectedCardOnHexMessage)) {
                TryUseSelectedCardOnHexMessage msg = (TryUseSelectedCardOnHexMessage)message;
                TryUseSelectedCardOnHex(msg.hexLoc);
            } else if (message.GetType() == typeof(AttackSelectedMessage)) {
                AttackSelectedMessage msg = (AttackSelectedMessage)message;
                AttackSelected(msg.hex);
            } else if (message.GetType() == typeof(MoveSelectedUnitMessage)) {
                MoveSelectedUnitMessage msg = (MoveSelectedUnitMessage)message;
                MoveSelectedUnit(msg.hexLoc);
            } else if (message.GetType() == typeof(FinishMoveSelectedMessage)) {
                FinishMoveSelected();
            } else if (message.GetType() == typeof(UnselectHexMessage)) {
                UnselectHexMessage msg = (UnselectHexMessage)message;
                UnselectHex();
            } else if (message.GetType() == typeof(SelectUnitMessage)) {
                SelectUnitMessage msg = (SelectUnitMessage)message;
                SelectUnit(msg.unit);
            } else if (message.GetType() == typeof(UnselectCardMessage)) {
                UnselectCard();
            } else if (message.GetType() == typeof(SelectCardMessage)) {
                SelectCardMessage msg = (SelectCardMessage)message;
                SelectCard(msg.cardIndex);
            } else if (message.GetType() == typeof(EndTurnMessage)) {
                EndTurn();
            } else if (message.GetType() == typeof(ScrollToMessage)) {
                ScrollToMessage msg = (ScrollToMessage)message;
                ScrollTo(msg.offset);
            }
        }
    }
}
