﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

using UTS;
using UTS.Utils;
using UTS.Drawing;
using UTS.Hexes;
using UTS.Units;
using UTS.Buildings;

namespace UTS.Controllers {
    /// <summary>
    /// Describes a clickable region on screen.
    /// </summary>
    public class ClickableArea {
        public Rectangle rectangle;
        public Action clickCallback;

        public ClickableArea(Rectangle rectangle, Action clickCallback) {
            this.rectangle = rectangle;
            this.clickCallback = clickCallback;
        }
    }

    /// <summary>
    /// Manages a set of clickable areas and can notify about clicks over any of them.
    /// Warning: The areas cannot overlap.
    /// </summary>
    public class ClickableAreaManager {
        protected MouseAssistant mouseHelper;
        protected int lastAreaIndex = -1;
        protected List<ClickableArea> clickables = new List<ClickableArea>();
        public bool Enabled = true;

        public ClickableAreaManager(MouseAssistant mouseAssistant) {
            this.mouseHelper = mouseAssistant;
        }

        public void AddClickableArea(Rectangle rectangle, Action clickCallback) {
            clickables.Add(new ClickableArea(rectangle, clickCallback));
        }

        public void Update(GameTime gameTime) {
            if (!Enabled)
                return;

            HasClickedSomething = false;
            IsOverSomething = false;
            int i;
            for (i = 0; i < clickables.Count; ++i) {
                if (mouseHelper.Location.IsIn(clickables[i].rectangle)) {
                    IsOverSomething = true;
                    if (mouseHelper.JustPressedLeft) {
                        lastAreaIndex = i;
                    }
                    if (mouseHelper.JustReleasedLeft) {
                        if (lastAreaIndex != -1 && mouseHelper.Location.IsIn(clickables[lastAreaIndex].rectangle)) {
                            HasClickedSomething = true;
                            clickables[lastAreaIndex].clickCallback();
                        }
                    }
                    break;
                }
            }
            if (i == clickables.Count) {
                // The previous loop didn't break, no clickable rectangle was found
                lastAreaIndex = -1;
            }
        }

        /// <summary>
        /// Reports whether an area has been clicked in last tick.
        /// </summary>
        public bool HasClickedSomething { get; protected set; }
        /// <summary>
        /// Returns whether the mouse is over any of the areas.
        /// </summary>
        public bool IsOverSomething { get; protected set; }
    }

    /// <summary>
    /// A controller that does user actions based on mouse input, which is the default or primary way
    /// to control the game.
    /// </summary>
    public class MouseController : Controller {
        MouseAssistant mouseHelper = new MouseAssistant();
        // for dealing with buttons and other stationary stuff
        ClickableAreaManager manager;
        MouseState lastMouseState = new MouseState();
        Point lastMouseHex = new Point();
        bool wasLastMouseInner = false;

        public MouseController() {
        }

        // This function checks all the mouse controlled areas in a match
        // And distributes mouse action to the more specific functions later on
        public override void Update(GameTime gameTime) {
            if (!Enabled)
                return;

            mouseHelper.Update(gameTime);

            Match match = Match.Current;
            MouseState mouseState = Mouse.GetState();
            

            #region Handle CardsRectangle
            bool cardAffected = false;
            if (mouseState.Location().IsIn(match.layout.cardsRectangle)) {
                Point relative = mouseState.Location().Sub(match.layout.hexesRectangle.Origin());

                if (discardState == false) {
                    if (mouseHelper.JustPressedLeft) {
                        CardsPress(relative, true, out cardAffected);
                    }
                    if (mouseHelper.JustReleasedLeft) {
                        CardsRelease(relative, true, out cardAffected);
                    }
                }
                if (mouseHelper.JustReleasedRight) {
                    CardsPress(relative, false, out cardAffected);
                }
                if (mouseHelper.JustReleasedRight) {
                    CardsRelease(relative, false, out cardAffected);
                }
            }
            #endregion
            if (discardState == false) {
                #region Handle Other (buttons...)
                if (manager == null) {
                    //Lazy instantiation, needed for match to be instantiated by this time
                    manager = new ClickableAreaManager(mouseHelper);
                    manager.AddClickableArea(match.layout.nextTurnButton, NextTurnClick);
                }

                manager.Update(gameTime);

                if (manager.HasClickedSomething)
                    return;
                #endregion
                #region Handle HexesRectangle
                if (!cardAffected && mouseState.Location().IsIn(match.layout.hexesRectangle)) {
                    Point relative = mouseState.Location().Sub(match.layout.hexesRectangle.Origin());
                    Point lastRelative = lastMouseState.Location().Sub(match.layout.hexesRectangle.Origin());

                    bool isInner;
                    Point mouseHex = GetHexPosition(relative, out isInner);

                    HexesHover(mouseHex, isInner);

                    // Button Presses
                    if (mouseHelper.JustPressedLeft) {
                        HexesLeftPress(mouseHex, isInner);
                    }
                    if (mouseHelper.JustReleasedLeft) {
                        HexesLeftRelease(mouseHex, isInner);
                    }
                    if (mouseHelper.JustReleasedRight) {
                        HexesRightRelease(mouseHex, isInner);
                    }

                    if (mouseHelper.JustMoved()) {
                        // Mouse move -- Scrolling
                        if (relative.X <= Constants.ScrollBorderWidth) {
                            StartScrolling(Direction.Left);
                        } else if (relative.X >= match.layout.hexesRectangle.Width - Constants.ScrollBorderWidth) {
                            StartScrolling(Direction.Right);
                        } else {
                            StopScrolling(Orientation.Horizontal);
                        }

                        if (relative.Y <= Constants.ScrollBorderWidth) {
                            StartScrolling(Direction.Up);
                        } else if (relative.Y >= match.layout.hexesRectangle.Height - Constants.ScrollBorderWidth) {
                            StartScrolling(Direction.Down);
                        } else {
                            StopScrolling(Orientation.Vertical);
                        }
                        if (manager.IsOverSomething) {
                            StopScrolling();
                        }
                    }

                    lastMouseHex = mouseHex;
                    wasLastMouseInner = isInner;
                } else { //mouseState not in hexesRectangle
                    match.StopScrolling();
                }
                #endregion 
            } else {
                #region Handle Discard Buttons
                if (mouseHelper.JustReleasedLeft) {
                    if (mouseState.Location().IsIn(match.layout.discardConfirmButton)) {
                        DiscardSelected();
                        discardState = false;
                    } else if (mouseState.Location().IsIn(match.layout.discardCancelButton)) {
                        discardState = false;
                    }
                }
                #endregion
            }

            if (mouseHelper.JustReleasedLeft) {
                lastCardPressIndex = -1;
                lastHexPressed = new Point(-1, -1);
            }

            lastMouseState = mouseState;
        }

        #region Handling Hexes

        // Registering clicks

        protected static Point GetHexPosition(Point location, out bool isInnerClick) {
            Match match = Match.Current;
            Point actualLocation = location.Add(match.hexAnimator.Offset.ToPoint());
            
            //First, we compute closest points from even and odd columns (because that is simple)

            Point oddLoc = new Point();
            Point evenLoc = new Point();

            int blockWidth = 3 * Constants.HexWidth / 2;
            int blockHeight = Constants.HexHeight;

            evenLoc.X = 2 * (actualLocation.X / blockWidth);
            evenLoc.Y = actualLocation.Y / blockHeight;

            Point shifted = actualLocation;
            shifted.X -= (3 * Constants.HexWidth) / 4;
            shifted.Y -= Constants.HexHeight / 2;
            oddLoc.X = 2 * (shifted.X / blockWidth) + 1;
            oddLoc.Y = shifted.Y / blockHeight;

            //Computed candidate even-column and odd-column locations
            //Now we check which is closer to the click origin

            Point actualOdd = HexAnimator.GetPixelCoordinates(oddLoc.X, oddLoc.Y);
            Point actualEven = HexAnimator.GetPixelCoordinates(evenLoc.X, evenLoc.Y);
            //Move to center
            actualOdd.X += Constants.HexWidth / 2;
            actualOdd.Y += Constants.HexHeight / 2;
            actualEven.X += Constants.HexWidth / 2;
            actualEven.Y += Constants.HexHeight / 2;

            // Compare distances from hex center
            Point resultLoc; Point pixelHexCenter;
            if (actualOdd.DistanceFrom(actualLocation) < actualEven.DistanceFrom(actualLocation)) {
                resultLoc = oddLoc;
                pixelHexCenter = actualOdd;
            } else {
                resultLoc = evenLoc;
                pixelHexCenter = actualEven;
            }

            // Resulting location is in resultLoc, computing inner click
            double innerCircleRad = 27;
            if (pixelHexCenter.Add(13, 27).DistanceFrom(actualLocation) < innerCircleRad) {
                isInnerClick = true;
            } else {
                isInnerClick = false;
            }

            return resultLoc;
        }

        protected Point lastHexPressed = new Point(-1, -1);
        protected bool lastHexPressedInner = false;
        protected bool InnerPosMakesDifference(Point hexLoc) {
            return Match.Current.buildings[hexLoc] != null && Match.Current.units[hexLoc] != null;
        }
        protected static Hex ProperHex(Point hexLoc, bool isInner) {
            Match match = Match.Current;
            Unit unit = match.units[hexLoc];
            Building building = match.buildings[hexLoc];
            if (unit != null && building == null)
                return unit;
            else if (building != null && unit == null)
                return building;
            else if (building != null && unit != null)
                return isInner ? (Hex)unit : (Hex)building;
            return null;
        }
        protected void HexesLeftPress(Point hexLoc, bool isInner) {
            Match match = Match.Current;
            //Click on non-hex
            if (hexLoc.X < 0 || hexLoc.X >= match.Map.Width || hexLoc.Y < 0 || hexLoc.Y >= match.Map.Height) {
                return;
            }

            lastHexPressed = hexLoc;
            lastHexPressedInner = isInner;
        }
        protected void HexesLeftRelease(Point hexLoc, bool isInner) {
            Match match = Match.Current;
            //Click on non-hex
            if (hexLoc.X < 0 || hexLoc.X >= match.Map.Width || hexLoc.Y < 0 || hexLoc.Y >= match.Map.Height) {
                return;
            }

            if (lastHexPressed == hexLoc &&
               (!InnerPosMakesDifference(hexLoc) || isInner == lastHexPressedInner)) {
                    HexesClick(lastHexPressed, isInner);
            }
            lastHexPressed = new Point(-1, -1);
        }

        // Handling clicks

        protected internal virtual void HexesClick(Point hexLoc, bool isInner) {
            Match match = Match.Current;

            if (match.IsCardSelected && TryUseSelectedCardOnHex(hexLoc)) {
                // Card was played, all done
                return;
            }
            Unit unit = match.units[hexLoc];
            Building building = match.buildings[hexLoc];
            Hex hex = ProperHex(hexLoc, isInner);


            if (match.IsUnitSelected) {
                Hex selected = match.SelectedUnit;
                bool canAttack = hex != null && selected.CanAttack(hex, true);
                bool canMove = selected as Unit != null && ((Unit)selected).CanMoveTo(hexLoc);
                if (canMove && canAttack) {
                    // If can do both, inner hex click means attack
                    if (isInner)
                        canMove = false;
                    else
                        canAttack = false;
                }

                // Try attack
                if (canAttack) {
                    AttackSelected(hex);
                    return;
                // Try move
                } else if (canMove) {
                    MoveSelectedUnit(hexLoc);
                    return;
                // Try finish move
                } else if (unit != null && unit == selected && unit.IsMidMove) {
                    FinishMoveSelected();
                    return;
                // Unselect if other fails
                } else {
                    UnselectHex();
                    // no return, selecting another hex is still possible
                }
            }
            if (unit != null && unit.CanBeSelectedBy(match.CurrentPlayer)) {
                SelectUnit(unit);
            }
        }
        /// <summary>
        /// Processes that the Mouse is over a Hex. This function is called every tick to keep the proper hex hovered.
        /// </summary>
        /// <param name="hexLoc"></param>
        /// <param name="isInner"></param>
        protected internal virtual void HexesHover(Point hexLoc, bool isInner) {
            Match match = Match.Current;

            //Click on non-hex
            if (hexLoc.X < 0 || hexLoc.X >= match.Map.Width || hexLoc.Y < 0 || hexLoc.Y >= match.Map.Height) {
                return;
            }

            SetHoveredHex(ProperHex(hexLoc, isInner));
            if (match.HoveredHex != null) {
                if (Mouse.GetState().MiddleButton == ButtonState.Pressed) {
                    ShowPossibleMovesAndTargets(match.HoveredHex);
                }
            }
        }
        protected void ShowPossibleMovesAndTargets(Hex hex) {
            Match match = Match.Current;
            
            // Fake exhaustion to get valid moves even from exhausted hexes
            bool wasExhausted = hex.IsExhausted;
            hex.IsExhausted = false;
            List<Point> validMoves = hex.GetValidMoves();
            List<Hex> validTargets = hex.GetValidTargets();
            hex.IsExhausted = wasExhausted;

            validMoves = validMoves.Where((move) => { return !Hex.IsDisplayedAsValidMove(move); }).ToList();
            validTargets = validTargets.Where((target) => { return !target.IsDisplayedAsValidTarget(); }).ToList();
            foreach (var move in validMoves) {
                Hex.DisplayAsValidMove(move);
            }
            foreach (var target in validTargets) {
                target.DisplayAsValidTarget();
            }
            match.hexAnimator.GetAnimator((int)Match.Layer.FloatingTexts).AddDrawFinishCallback(() => {
                foreach (var move in validMoves) {
                    Hex.HideAsValidMove(move);
                }
                foreach (var target in validTargets) {
                    target.HideAsValidTarget();
                }
            });
        }

        protected void HexesRightRelease(Point hex, bool isInner) {
            // unselect unit
            Match match = Match.Current;
            if (match.IsUnitSelected) {
                UnselectHex();
            }
            if (match.IsCardSelected) {
                UnselectCard();
            }
        }

        #endregion

        #region Handling Cards
        protected int GetCardIndex(Point location) {
            return location.X / Constants.CardWidth;
        }

        protected int lastCardPressIndex = -1;
        protected bool cardPressedButton = false;
        protected void CardsPress(Point location, bool isLeft, out bool cardAffected) {
            lastCardPressIndex = GetCardIndex(location);
            cardPressedButton = isLeft;

            int handSize = Match.Current.CurrentPlayer.hand.Count;
            cardAffected = lastCardPressIndex.IsLimitedToRange(0, handSize - 1);
        }
        protected void CardsRelease(Point location, bool isLeft, out bool cardAffected) {
            int cardIndex = GetCardIndex(location);
            if (cardIndex == lastCardPressIndex && isLeft == cardPressedButton) {
                if (isLeft)
                    CardClick(lastCardPressIndex);
                else
                    CardRightClick(lastCardPressIndex);
            }

            int handSize = Match.Current.CurrentPlayer.hand.Count;
            cardAffected = cardIndex.IsLimitedToRange(0, handSize - 1);
        }

        protected void CardClick(int cardIndex) {
            Match match = Match.Current;
            if (match.cardAnimator.CardCount > cardIndex) {
                SelectCard(cardIndex);
            }
        }

        bool _discardState = false;
        bool[] selectedForDiscard = new bool[9];
        bool discardState {
            get { return _discardState; }
            set {
                Match match = Match.Current;
                if (_discardState == value)
                    return;

                _discardState = value;

                if (value) {
                    match.layoutAnimator.AddSprite(Textures.GUIDiscardOverlay, Point.Zero, (int)Match.LayoutLayer.Overlay);
                } else {
                    for (int i = 0; i < match.cardAnimator.CardCount; ++i) {
                        selectedForDiscard[i] = false;
                        match.cardAnimator.RemoveOverlays(i);
                    }
                    for (int i = match.cardAnimator.CardCount; i < selectedForDiscard.Length; ++i) {
                        selectedForDiscard[i] = false;
                    }
                    match.layoutAnimator.RemoveSprite(Point.Zero, (int)Match.LayoutLayer.Overlay);
                }
            }
        }
        protected void CardRightClick(int cardIndex) {
            UnselectCard();
            UnselectHex();

            Match match = Match.Current;
            if (!discardState) {
                discardState = true;
            }

            if (selectedForDiscard[cardIndex]) {
                selectedForDiscard[cardIndex] = false;
                match.cardAnimator.RemoveOverlays(cardIndex);
            } else {
                selectedForDiscard[cardIndex] = true;
                match.cardAnimator.AddOverlay(cardIndex, Textures.CardDiscardOverlay);
            }
            

            if (!selectedForDiscard.Contains(true)) {
                discardState = false;
            }
        }
        
        protected void DiscardSelected() {
            Match match = Match.Current;
            
            int discarded = 0;
            for (int i = 0; i < selectedForDiscard.Length; ++i) {
                if (selectedForDiscard[i]) {
                    DiscardCard(i - discarded);
                    ++discarded;
                }
            }
            
        }
        #endregion

        #region Handling other - buttons, etc.

        public void NextTurnClick() {
            Match match = Match.Current;

            if (match.IsUnitSelected && match.SelectedUnit != null && match.SelectedUnit.IsMidMove)
                return;
            EndTurn();
        }

        #endregion
    }
}
