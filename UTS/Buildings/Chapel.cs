﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using UTS.Drawing;

namespace UTS.Buildings {
    class Chapel : Building {
        public Chapel(PlayerColor color)
            : base(
                new SpriteOfEachColor(Textures.ChapelBlue, Textures.ChapelRed),
                Textures.ScrollMidChapelTexture,
                color
            ) {
        }

        public override void StartTurn(Collections.EventQueue eventQueue) {
            base.StartTurn(eventQueue);
            if (ItsMyTurn) {
                if (MainGame.random.NextDouble() < 0.5) {
                    eventQueue.PushWait(0.5);
                    eventQueue.Push(() => {
                        Match.Current.DrawCard();
                    });
                }
            }
        }
    }
}
