﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

using UTS.Utils;

namespace UTS.Controllers {
    /// <summary>
    /// Records the mouse state over time and provides methods to check whether the mouse has moved, or a button was just pressed / released.
    /// </summary>
    public class MouseAssistant {
        protected MouseState currentState = new MouseState();
        protected MouseState lastState = new MouseState();

        public void Update(GameTime gameTime) {
            lastState = currentState;
            currentState = Mouse.GetState();
        }

        public bool JustPressedLeft {
            get {
                return lastState.LeftButton == ButtonState.Released && currentState.LeftButton == ButtonState.Pressed;
            }
        }

        public bool JustReleasedLeft {
            get {
                return lastState.LeftButton == ButtonState.Pressed && currentState.LeftButton == ButtonState.Released;
            }
        }

        public bool JustPressedRight {
            get {
                return lastState.RightButton == ButtonState.Released && currentState.RightButton == ButtonState.Pressed;
            }
        }

        public bool JustReleasedRight {
            get {
                return lastState.RightButton == ButtonState.Pressed && currentState.RightButton == ButtonState.Released;
            }
        }

        public Point Location {
            get {
                return new Point(currentState.X, currentState.Y);
            }
        }

        public bool JustMoved() {
            return currentState.Location() != lastState.Location();
        }
    }
    /// <summary>
    /// Oversees the keyboard state over time and can provide info when keys have been pressed/released.
    /// Also provides helper methods to directly get characters of keys pressed.
    /// </summary>
    public class KeyboardAssistant {
        protected KeyboardState currentState = new KeyboardState();
        protected KeyboardState lastState = new KeyboardState();
        protected static Dictionary<Keys, char> keyToChars;

        public KeyboardAssistant() {
        }
        static KeyboardAssistant() {
            keyToChars = new Dictionary<Keys, char>();
            keyToChars.Add(Keys.A, 'a');
            keyToChars.Add(Keys.B, 'b');
            keyToChars.Add(Keys.C, 'c');
            keyToChars.Add(Keys.D, 'd');
            keyToChars.Add(Keys.E, 'e');
            keyToChars.Add(Keys.F, 'f');
            keyToChars.Add(Keys.G, 'g');
            keyToChars.Add(Keys.H, 'h');
            keyToChars.Add(Keys.I, 'i');
            keyToChars.Add(Keys.J, 'j');
            keyToChars.Add(Keys.K, 'k');
            keyToChars.Add(Keys.L, 'l');
            keyToChars.Add(Keys.M, 'm');
            keyToChars.Add(Keys.N, 'n');
            keyToChars.Add(Keys.O, 'o');
            keyToChars.Add(Keys.P, 'p');
            keyToChars.Add(Keys.Q, 'q');
            keyToChars.Add(Keys.R, 'r');
            keyToChars.Add(Keys.S, 's');
            keyToChars.Add(Keys.T, 't');
            keyToChars.Add(Keys.U, 'u');
            keyToChars.Add(Keys.V, 'v');
            keyToChars.Add(Keys.W, 'w');
            keyToChars.Add(Keys.X, 'x');
            keyToChars.Add(Keys.Y, 'y');
            keyToChars.Add(Keys.Z, 'z');

            keyToChars.Add(Keys.D0, '0');
            keyToChars.Add(Keys.D1, '1');
            keyToChars.Add(Keys.D2, '2');
            keyToChars.Add(Keys.D3, '3');
            keyToChars.Add(Keys.D4, '4');
            keyToChars.Add(Keys.D5, '5');
            keyToChars.Add(Keys.D6, '6');
            keyToChars.Add(Keys.D7, '7');
            keyToChars.Add(Keys.D8, '8');
            keyToChars.Add(Keys.D9, '9');

            keyToChars.Add(Keys.NumPad0, '0');
            keyToChars.Add(Keys.NumPad1, '1');
            keyToChars.Add(Keys.NumPad2, '2');
            keyToChars.Add(Keys.NumPad3, '3');
            keyToChars.Add(Keys.NumPad4, '4');
            keyToChars.Add(Keys.NumPad5, '5');
            keyToChars.Add(Keys.NumPad6, '6');
            keyToChars.Add(Keys.NumPad7, '7');
            keyToChars.Add(Keys.NumPad8, '8');
            keyToChars.Add(Keys.NumPad9, '9');

            keyToChars.Add(Keys.OemPeriod, '.');
            keyToChars.Add(Keys.OemComma, ',');
        }

        public void Update(GameTime gameTime) {
            lastState = currentState;
            currentState = Keyboard.GetState();
        }

        public List<Keys> JustPressedKeys() {
            return currentState.GetPressedKeys().Except(lastState.GetPressedKeys()).ToList();
        }
        public List<Keys> JustReleasedKeys() {
            return lastState.GetPressedKeys().Except(currentState.GetPressedKeys()).ToList();
        }

        public List<char> JustPressedChars() {
            List<Keys> keys = JustPressedKeys();
            List<char> ret = new List<char>(keys.Count);
            foreach (Keys key in keys) {
                char c;
                if (keyToChars.TryGetValue(key, out c)) {
                    ret.Add(c);
                }
            }
            return ret;
        }
    }
}
