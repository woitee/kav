﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using UTS.Hexes;
using UTS.Units;
using UTS.Events;

namespace UTS.Cards {
    class ChargeCard : EventCard {

        TurnEndEventHandler turnEndEventHandler;

        public ChargeCard() : base(Textures.CardCharge) {
            turnEndEventHandler = (args) => {
                Deactivate(args.player);
                Match.Current.TurnEnd -= turnEndEventHandler;
            };
        }

        public override bool TryUseOnHex(Point location) {
            Match match = Match.Current;
            Player curPlayer = match.CurrentPlayer;

            if (TryPayCost(location)) {
                Activate(match.CurrentPlayer);
                match.TurnEnd += turnEndEventHandler;
                return true;
            }

            return false;
        }

        public List<Unit> chargedUnits;
        public void Activate(Player player) {
            Match match = Match.Current;

            chargedUnits = new List<Unit>();
            foreach (Unit unit in match.units.Values) {
                if (unit.color == player.color) {
                    chargedUnits.Add(unit);
                    unit.MovementRange += 1;
                }
            }
        }

        public void Deactivate(Player player) {
            foreach (Unit unit in chargedUnits) {
                unit.MovementRange -= 1;
            }
        }
    }
}
