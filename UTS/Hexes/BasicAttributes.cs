﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UTS.Hexes;
using UTS.Cards;
using UTS.Units;
using UTS.Buildings;

namespace UTS.Hexes {
    /// <summary>
    /// Common ancestor, basic info about a unit, building or an event.
    /// </summary>
    public class HexBasicValue {
        public int health;
        public int attackDamage;
        public int attackRange;
        public int defense;
        public int foodCost;
        public int peopleCost;
        public int upkeep;
        public Hex.AttackType attackType;

        public HexBasicValue(int health, int attackDamage, int attackRange, Hex.AttackType attackType, int defense, int foodCost, int peopleCost, int upkeep) {
            this.health = health;
            this.attackDamage = attackDamage;
            this.attackRange = attackRange;
            this.attackType = attackType;
            this.defense = defense;
            this.foodCost = foodCost;
            this.peopleCost = peopleCost;
            this.upkeep = upkeep;
        }
    }
    /// <summary>
    /// Basic info about a unit.
    /// </summary>
    public class UnitBasicValue : HexBasicValue {
        public int movementRange;
        public UnitType unitType;
        public UnitBasicValue(int health, int attackDamage, int attackRange, Hex.AttackType attackType, int defense, int movementRange,
                              int foodCost, int peopleCost, int upkeep, UnitType unitType)
            : base (health, attackDamage, attackRange, attackType, defense, foodCost, peopleCost, upkeep) {

            this.unitType = unitType;
            this.movementRange = movementRange;
        }
    }
    /// <summary>
    /// Basic info about a building.
    /// </summary>
    public class BuildingBasicValue : HexBasicValue {
        public BuildingBasicValue(int health, int attackDamage, int attackRange, Hex.AttackType attackType, int defense, int foodCost, int peopleCost, int upkeep)
            : base (health, attackDamage, attackRange, attackType, defense, foodCost, peopleCost, upkeep) {

        }
    }
    /// <summary>
    /// Basic info about an event.
    /// </summary>
    public class EventBasicValue {
        public int foodCost;
        public int peopleCost;
        public int upkeep;

        public EventBasicValue(int foodCost, int peopleCost, int upkeep) {
            this.foodCost = foodCost;
            this.peopleCost = peopleCost;
            this.upkeep = upkeep;
        }
    }

    /// <summary>
    /// Contains the most basic information about all units, buildings and events.
    /// </summary>
    public static class BasicAttributes {
        /// <summary>
        /// Contains the most basic information about all units.
        /// </summary>
        public static Dictionary<Type, UnitBasicValue> units = new Dictionary<Type, UnitBasicValue>();
        /// <summary>
        /// Contains the most basic information about all buildings.
        /// </summary>
        public static Dictionary<Type, BuildingBasicValue> buildings = new Dictionary<Type, BuildingBasicValue>();
        /// <summary>
        /// Contains the most basic information about all events.
        /// </summary>
        public static Dictionary<Type, EventBasicValue> events = new Dictionary<Type, EventBasicValue>();

        static BasicAttributes() {
            //                                           HP, AD, AR, AttackType        DEF, MOV,FC,PC,UP, UnitType
            units.Add(typeof(Archer), new UnitBasicValue(4, 2, 3, Hex.AttackType.Ranged, 0, 4, 1, 1, 1, UnitType.Infantry));
            units.Add(typeof(Catapult), new UnitBasicValue(10, 4, 4, Hex.AttackType.Boulder, 0, 2, 2, 7, 3, UnitType.Siege));
            units.Add(typeof(Footman), new UnitBasicValue(4, 3, 1, Hex.AttackType.Melee, 0, 4, 2, 1, 1, UnitType.Infantry));
            units.Add(typeof(Horseman), new UnitBasicValue(7, 3, 1, Hex.AttackType.Melee, 1, 7, 6, 2, 3, UnitType.Cavalry));
            units.Add(typeof(Knight), new UnitBasicValue(8, 4, 1, Hex.AttackType.Melee, 2, 3, 5, 4, 2, UnitType.Cavalry));
            units.Add(typeof(Marshal), new UnitBasicValue(8, 5, 1, Hex.AttackType.Melee, 2, 4, 7, 7, 3, UnitType.Cavalry));
            units.Add(typeof(Peasant), new UnitBasicValue(3, 2, 1, Hex.AttackType.Melee, 0, 3, 0, 1, 0, UnitType.Infantry));
            units.Add(typeof(Pikeman), new UnitBasicValue(5, 3, 1, Hex.AttackType.Melee, 0, 3, 2, 1, 1, UnitType.Infantry));

            //                                                    HP, AD, AR,   AttackType      DEF,FC,PC,Upkeep
            buildings.Add(typeof(Barracks), new BuildingBasicValue(9, 0, 0, Hex.AttackType.None, 2, 2, 4, 0));
            buildings.Add(typeof(Castle), new BuildingBasicValue(15, 3, 3, Hex.AttackType.Ranged, 3, 0, 0, 0));
            buildings.Add(typeof(Chapel), new BuildingBasicValue(8, 0, 0, Hex.AttackType.None, 2, 2, 4, 0));
            buildings.Add(typeof(ConstructionSite), new BuildingBasicValue(1, 0, 0, Hex.AttackType.None, 0, 0, 0, 0));
            buildings.Add(typeof(Field), new BuildingBasicValue(3, 0, 0, Hex.AttackType.None, 1, 0, 1, 0));
            buildings.Add(typeof(House), new BuildingBasicValue(5, 0, 0, Hex.AttackType.None, 1, 2, 2, 0));
            buildings.Add(typeof(Smithy), new BuildingBasicValue(6, 0, 0, Hex.AttackType.None, 1, 5, 8, 2));
            buildings.Add(typeof(Tavern), new BuildingBasicValue(6, 0, 0, Hex.AttackType.None, 1, 6, 4, 1));
            buildings.Add(typeof(Village), new BuildingBasicValue(7, 3, 4, Hex.AttackType.Ranged, 1, 8, 8, 0));
            buildings.Add(typeof(Warehouse), new BuildingBasicValue(3, 0, 0, Hex.AttackType.None, 1, 0, 3, 0));
            buildings.Add(typeof(Watch), new BuildingBasicValue(8, 3, 3, Hex.AttackType.Ranged, 1, 3, 5, 0));
            
            //                                                  FC, PC, Upkeep
            events.Add(typeof(ArmoredCard), new EventBasicValue(1, 2, 0));
            events.Add(typeof(AssaultCard), new EventBasicValue(1, 2, 0));
            events.Add(typeof(ChargeCard), new EventBasicValue(4, 0, 0));
            events.Add(typeof(FeastCard), new EventBasicValue(3, 0, 0));
            events.Add(typeof(HarvestCard), new EventBasicValue(0, 2, 0));
            events.Add(typeof(KnowledgeCard), new EventBasicValue(3, 0, 0));
            events.Add(typeof(PlagueCard), new EventBasicValue(7, 4, 2));
            events.Add(typeof(WeaponsCard), new EventBasicValue(1, 2, 0));
        }
    }
}
