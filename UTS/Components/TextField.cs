﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using UTS.Drawing;
using UTS.Controllers;
using UTS.Utils;

namespace UTS.Components {
    /// <summary>
    /// A graphical textfield, self-managed class for the user to enter text in a GUI.
    /// </summary>
    class TextField : IDrawer {
        protected class Blip {
            /// <summary>
            /// A period of blicking cursor, in seconds.
            /// </summary>
            protected static readonly double blickPeriod = 1.5;
            protected TextField parent;
            public int charIndex = 0;
            protected DateTime lastPeriodStart = DateTime.Now;

            public Blip(TextField parent) {
                this.parent = parent;
            }

            public void DrawTo(SpriteBatch spriteBatch) {
                // Figure top left corner of text
                Vector2 drawLocation = parent.rectangle.Origin().ToVector();
                // Move after apropriate amount of characters
                drawLocation.X += parent.font.MeasureString(parent.Text.Substring(0, charIndex)).X;
                // Draw
                double s = (DateTime.Now - lastPeriodStart).TotalSeconds;
                if (s <= blickPeriod / 2) {
                    spriteBatch.DrawString(parent.font, "|", drawLocation, parent.color);
                } else if (s > blickPeriod) {
                    lastPeriodStart = lastPeriodStart.AddSeconds(blickPeriod);
                }
            }
        }

        protected MouseAssistant mouseAssist = new MouseAssistant();
        protected KeyboardAssistant kbAssist = new KeyboardAssistant();
        protected Rectangle rectangle;
        protected SpriteFont font;
        
        protected float fontHeight;
        protected string _Text;
        
        protected Blip blip;

        /// <summary>
        /// Called when the enter key is pressed.
        /// </summary>
        public Action ActionCallback = null;
        public string Text {
            get { return _Text; }
            set { _Text = value; blip.charIndex = value.Length; }
        }
        public bool IsSelected { get; protected set; }
        public Color color { get; set; }

        public TextField(SpriteFont font, Rectangle rectangle) {
            this.rectangle = rectangle;
            this.font = font;
            fontHeight = font.MeasureString("|").Y;
            _Text = "";
            color = Color.Black;
            blip = new Blip(this);
        }
        public TextField(SpriteFont font, int x, int y, int width, int height) : this(font, new Rectangle(x, y, width, height)) { }

        public void Draw(SpriteBatch spriteBatch) {
            // Center vertically
            float yOffset = (rectangle.Height - fontHeight) / 2;
            Vector2 drawLocation = new Vector2(rectangle.X, rectangle.Y + (int)yOffset);

            // Draw
            spriteBatch.DrawString(font, Text, drawLocation, Color.Black);
            if (IsSelected)
                blip.DrawTo(spriteBatch);
        }
        public void Update(GameTime gameTime) {
            mouseAssist.Update(gameTime);
            kbAssist.Update(gameTime);
            // Handle mouse clicks -- capable of selecting the TextField
            if (mouseAssist.JustReleasedLeft) {
                if (Mouse.GetState().Location().IsIn(rectangle)) {
                    IsSelected = true;
                } else {
                    IsSelected = false;
                }
            }
            // Handle keyboard presses -- to enter text if the TextField is selected 
            if (IsSelected) {
                // Delete text on BackSpace presses
                if (kbAssist.JustPressedKeys().Contains(Keys.Back)) {
                    if (Text.Length > 0) {
                        Text = Text.Substring(0, Text.Length - 1);
                    }
                // Fire event on enter key
                } else if (kbAssist.JustPressedKeys().Contains(Keys.Enter)) {
                    if (ActionCallback != null)
                        ActionCallback();
                // Try to add text on any other press
                } else {
                    string pressed = new String(kbAssist.JustPressedChars().ToArray());
                    string newText = Text + pressed;
                    if (font.MeasureString(newText).X <= rectangle.Width) {
                        Text = newText;
                    }
                }
            }
        }
    }
}
