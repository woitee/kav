﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UTS.Cards {
    class MarshalCard : UnitCard {
        public MarshalCard()
            : base(
                Textures.CardMarshal,
                new Units.Marshal(PlayerColor.Blue)
                ) { }
    }
}
