﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UTS.Buildings;

namespace UTS.Cards {
    class SmithyCard : BuildingCard {
        public SmithyCard()
            : base(
                Textures.CardSmithy,
                new Smithy(PlayerColor.Blue)
            ) { }
    }
}
