﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UTS.Utils;
using UTS.Hexes;
using UTS.Drawing;

namespace UTS.Hexes.Animations {
    /// <summary>
    /// A class drawing everything visual that happens when one Hex attacks another.
    /// </summary>
    public class HexHitAnimation : ILocationDrawable{
        public int Width { get { return Textures.MeleeHitIcon.Width; } }
        public int Height { get { return Textures.MeleeHitIcon.Height; } }
        /// <summary>
        /// Seconds after start, when the text with damage amount appears.
        /// </summary>
        public readonly float textBegin;
        /// <summary>
        /// Seconds after start, when the hit starts to fade out.
        /// </summary>
        public readonly float fadeoutStart;
        /// <summary>
        /// Seconds after start, when the hit finishes fading out, and the whole animation finishes completely.
        /// </summary>
        public readonly float fadeoutEnd;

        protected DateTime createdAt;
        protected int thisAttackDamage;
        protected Hex.AttackType attackType;
        protected AttackBonus bonus;
        protected Hex source, target;
        /// <summary>
        /// Return if this animation is already showing text with damage amount.
        /// </summary>
        public bool isShowingBonus { get; protected set; }

        /// <summary>
        /// Creates a new animation for a specific attack.
        /// </summary>
        /// <param name="source">The attacker, source of the attack.</param>
        /// <param name="target">The target of the attack.</param>
        /// <param name="thisAttackDamage">Calculated damage of the attack, bonus included.</param>
        /// <param name="bonus">Bonus that has been chosen for this attack.</param>
        public HexHitAnimation(Hex source, Hex target, int thisAttackDamage, AttackBonus bonus) {
            createdAt = DateTime.Now;
            this.source = source;
            this.target = target;
            this.thisAttackDamage = thisAttackDamage;
            this.attackType = source.attackType;
            if (attackType == Hex.AttackType.Melee) {
                textBegin = 0.3f;
                fadeoutStart = 1.3f;
                fadeoutEnd = 2.3f;
            } else {
                double distance = target.PixelLocation.DistanceFrom(source.PixelLocation);
                textBegin = 0.2f * (float)distance / Constants.HexHeight;
                fadeoutStart = textBegin + 1f;
                fadeoutEnd = textBegin + 2f;
            }
            this.bonus = bonus;
        }

        public void DrawTo(SpriteBatch spriteBatch, Vector2 location, out bool RequestRemove) {
            DateTime now = DateTime.Now;
            float deadTime = (float)(now - createdAt).TotalSeconds;
            if (deadTime > fadeoutEnd) {
                RequestRemove = true;
                return;
            }
            RequestRemove = false;

            float hitProgress = deadTime / textBegin;
            if (hitProgress <= 1) {
                // Draw hit animation
                if (attackType == Hex.AttackType.Melee)
                    DrawMeleeStrike(spriteBatch, location, hitProgress);
                else if (attackType == Hex.AttackType.Ranged)
                    DrawRangedStrike(spriteBatch, location, hitProgress);
                else if (attackType == Hex.AttackType.Boulder)
                    DrawBoulderStrike(spriteBatch, location, hitProgress);
            } else {
                // Draw hit details (damage, flowing text)
                if (!isShowingBonus) {
                    bonus.CreateFloatingText(target.Location);
                    isShowingBonus = true;
                }
                float fadePercent = ((deadTime - fadeoutStart) / (fadeoutEnd - fadeoutStart));
                fadePercent = fadePercent.LimitToRange(0f, 1f);
                DrawHit(spriteBatch, location, fadePercent);
            }
        }

        /// <summary>
        /// Draw a ranged strike, an arrow flying through the air.
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch to draw on.</param>
        /// <param name="targetLocation">Hex location of the target Hex to attack.</param>
        /// <param name="percent">
        ///     Percentage of the strike to draw.
        ///     i.e. percent = 0.5 would mean to draw the arrow half way.
        /// </param>
        public void DrawRangedStrike(SpriteBatch spriteBatch, Vector2 targetLocation, float percent) {
            // Calculate location
            Vector2 sourceFromTarget = 
                HexAnimator.GetPixelCoordinates(source.Location)
                .Sub(HexAnimator.GetPixelCoordinates(target.Location))
                .ToVector();
            Vector2 src = targetLocation + sourceFromTarget;
            Vector2 dst = targetLocation;

            Vector2 vector = dst - src;
            Vector2 destination = src +vector * percent;
            destination += new Vector2(Constants.HexWidth / 2, Constants.HexHeight / 2);

            Vector2 halfSize = new Vector2(Textures.ArrowSmall.Width / 2, Textures.ArrowSmall.Height / 2);

            // Calculate rotation
            float radians = (float)Math.Atan2(dst.Y - src.Y, dst.X - src.X);
            radians += MathHelper.Pi;
            // Draw on top of everything
            Animator animator = Match.Current.hexAnimator.GetAnimator((int)Match.Layer.FloatingTexts);
            animator.AddDrawFinishCallback(() => {
                spriteBatch.Draw(Textures.ArrowSmallTexture, destination, null, Color.White, radians, halfSize, 1, SpriteEffects.None, 0);
            });
        }
        private float boulderRotation = 0; //radians
        /// <summary>
        /// Draw a boulder strike, a boulder flying through the air.
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch to draw on.</param>
        /// <param name="targetLocation">Hex location of the target Hex to attack.</param>
        /// <param name="percent">
        ///     Percentage of the strike to draw.
        ///     i.e. percent = 0.5 would mean to draw the boulder half way.
        /// </param>
        public void DrawBoulderStrike(SpriteBatch spriteBatch, Vector2 targetLocation, float percent) {
            // Calculate location
            Vector2 sourceFromTarget =
                HexAnimator.GetPixelCoordinates(source.Location)
                .Sub(HexAnimator.GetPixelCoordinates(target.Location))
                .ToVector();
            Vector2 src = targetLocation + sourceFromTarget;
            Vector2 dst = targetLocation;

            Vector2 vector = dst - src;
            Vector2 destination = src + vector * percent;
            destination += new Vector2(Constants.HexWidth / 2, Constants.HexHeight / 2);

            Vector2 halfSize = new Vector2(Textures.BoulderFlying.Width / 2, Textures.BoulderFlying.Height / 2);

            boulderRotation += 0.1f;
            // Draw on top of everything
            Animator animator = Match.Current.hexAnimator.GetAnimator((int)Match.Layer.FloatingTexts);
            animator.AddDrawFinishCallback(() => {
                spriteBatch.Draw(Textures.BoulderFlyingTexture, destination, null, Color.White, boulderRotation, halfSize, 1, SpriteEffects.None, 0);
            });
        }
        /// <summary>
        /// Draw a melee strike, an swinging sword.
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch to draw on.</param>
        /// <param name="targetLocation">Hex location of the target Hex to attack.</param>
        /// <param name="percent">
        ///     Percentage of the strike to draw.
        ///     i.e. percent = 0.5 would mean to draw the sword in the middle of it's swing.
        /// </param>
        public void DrawMeleeStrike(SpriteBatch spriteBatch, Vector2 targetLocation, float percent) {
            // Calculate location
            Vector2 sourceFromTarget =
                HexAnimator.GetPixelCoordinates(source.Location)
                .Sub(HexAnimator.GetPixelCoordinates(target.Location))
                .ToVector();
            Vector2 src = targetLocation + sourceFromTarget;
            Vector2 dst = targetLocation;
            Vector2 halfHex = Constants.HexSize.Scale(0.5).ToVector();

            // Calculate rotation
            Vector2 rotOrigin = new Vector2(21, 99);
            float radians = (float)Math.Atan2(dst.Y - src.Y, dst.X - src.X);
            radians += 3 * MathHelper.Pi / 12;
            radians += MathHelper.PiOver2 * percent;
            Animator animator = Match.Current.hexAnimator.GetAnimator((int)Match.Layer.FloatingTexts);
            animator.AddDrawFinishCallback(() => {
                spriteBatch.Draw(Textures.SwordSmallTexture, src + halfHex, null, Color.White, radians, rotOrigin, 1, SpriteEffects.None, 0);
            });
        }

        /// <summary>
        /// Draw the hit texture with text of damage dealt on top of it.
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch to draw on.</param>
        /// <param name="location">Hex location of the target that was hit.</param>
        /// <param name="fadePercent"></param>
        public void DrawHit(SpriteBatch spriteBatch, Vector2 location, float fadePercent) {
            Texture2D texture = null;
            switch (attackType) {
                case Hex.AttackType.Ranged:
                    texture = Textures.RangedHitIconTexture;
                    break;
                case Hex.AttackType.Melee:
                    texture = Textures.MeleeHitIconTexture;
                    break;
                case Hex.AttackType.Boulder:
                    texture = Textures.BoulderHitIconTexture;
                    break;
                default:
                    break;
            }
            spriteBatch.Draw(texture, location, Color.White * (1 - fadePercent));
            TextUtils.DrawCenteredTextWithOutline(
                spriteBatch,
                location + Constants.HexSize.ToVector() * 0.5f,
                "-" + thisAttackDamage.ToString(),
                Color.White * (1 - fadePercent),
                Color.Black * (1 - fadePercent),
                Fonts.BigArial
            );
        }
    }
}
